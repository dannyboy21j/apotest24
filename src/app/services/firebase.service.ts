import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import firebase from "firebase";

@Injectable({
	providedIn: 'root'
})
export class FirebaseService {

	constructor(private _fire: AngularFirestore) {}

	addCollection(col, obj) {
		return this._fire
			.collection(col)
			.add(obj);
	}
	
	addSubCollection(col, doc, subcol, obj) {
		return this._fire
			.collection(col)
			.doc(doc)
			.collection(subcol)
			.add(obj);
	}

	addDataInDBAutoKey(col, doc, subcol, obj) {
		return this._fire
			.collection(col)
			.doc(doc.toString())
			.collection(subcol)
			.add(obj);
	}

	setCollection(col, doc, obj) {
		return this._fire
			.collection(col)
			.doc(doc.toString())
			.set(obj);
	}

	setImageToDataBase(falohoId, allImages: Array<{nr:number, url:string}>) {
		return this._fire
			.collection('bildergalerie')
			.doc(falohoId)
			.set({bilder: allImages});
	}

	setAndMerge(feedId, newCommentsArray) {
		return this._fire
			.collection('feeds')
			.doc(feedId)
			.set({'comments': newCommentsArray},
				{merge: true}
			);
	}

	// setServerTimestamp() {
	// 	let timestamp = firebase.firestore.FieldValue.serverTimestamp();
	// 	return timestamp;
	// }

	deleteDocument(col, doc) {
		return this._fire
			.collection(col)
			.doc(doc)
			.delete();
	}

	deleteSubDocument(col, doc, subcol, subdoc) {
		return this._fire
			.collection(col)
			.doc(doc)
			.collection(subcol)
			.doc(subdoc)
			.delete();
	}

	getBeforeDocumentId(col): string {
		const db = firebase.firestore();
		const doc = db.collection(col).doc();
		return doc.id;
	}

	getSpecificDocument(col, doc) {
		return this._fire
			.collection(col)
			.doc(doc)
			.ref.get();
	}

	getCollectionSortedByDate(col) {
		return this._fire
			.collection(col, ref => {
				return ref.orderBy('date', 'desc')
			}).valueChanges();
		// return ref.orderBy('date','desc').limit(3)
	}

	getDataFromDatabaseWithDocId(col) {
		return this._fire
			.collection(col)
			.snapshotChanges().pipe(map(actions => {
				return actions.map((a: any) => {
					const data = a.payload.doc.data();
					const id = a.payload.doc.id;
					return {id, data};
				})
			}));
	}

	getDataFromDatabaseWithDocIdSortedByParam(col, searchTyp: string, sortTyp) {
		return this._fire
			.collection(col, (ref) => {
				return ref.orderBy(searchTyp, sortTyp)
			})
			.snapshotChanges().pipe(map(actions => {
				return actions.map((a: any) => {
					const data = a.payload.doc.data();
					const id = a.payload.doc.id;
					return {id, data};
				})
			}));
	}

	getDataWithDocIdFilteredByProperty(col, propertyKey: string, propertyValue: string) {
		return this._fire
			.collection(col, (ref) => {
				return ref.where(propertyKey, '==', propertyValue).orderBy('date','desc')
			})
			.snapshotChanges().pipe(map(actions => {
				return actions.map((a: any) => {
					const data = a.payload.doc.data();
					const id = a.payload.doc.id;
					return {id, data};
				})
			}));
	}

	getCollection(col) {
		return this._fire
			.collection(col)
			.valueChanges();
	}

	getCollectionWithQuery(col, param1, param2) {
		return this._fire
			.collection(col, ref => {
				return ref.where(param1.trim(), '==', param2.trim())
			}).valueChanges();
	}

	getCollectionWithTwoQuery(col,paramA1,paramA2,paramB1,paramB2) {
		return this._fire
			.collection(col, ref => {
				return ref.where(paramA1, '==', paramA2).where(paramB1, '==', paramB2)
			}).valueChanges();
	}
	
	getCollectionWithThreeQuery(col,paramA1,paramA2,paramB1,paramB2,paramC1,paramC2) {
		return this._fire
			.collection(col, ref => {
				return ref.where(paramA1, '==', paramA2)
					.where(paramB1, '==', paramB2)
					.where(paramC1, '==', paramC2)
			}).valueChanges();
	}
	
	getCollectionWith1QueryWithDocId(col, param1, param2) {
		return this._fire
			.collection(col, ref => ref
				.where(param1, '==' , param2))
			.snapshotChanges()
			.pipe(map(actions => {
				return actions.map((a: any) => {
					const data = a.payload.doc.data();
					const id = a.payload.doc.id;
					return {id, data};
				})
			}));
	}

	getCollectionWith2QueryWithDocId(col, paramA1, paramA2, paramB1, paramB2) {
		return this._fire
			.collection(col, ref => ref
				.where(paramA1, '==' , paramA2).where(paramB1, '==',paramB2))
			.snapshotChanges()
			.pipe(map(actions => {
				return actions.map((a: any) => {
					const data = a.payload.doc.data();
					const id = a.payload.doc.id;
					return {id, data};
				})
			}));
	}
	
	getCollectionWith3QueryWithDocId(col, paramA1, paramA2, paramB1, paramB2, paramC1, paramC2) {
		return this._fire
			.collection(col, ref => ref
				.where(paramA1, '==' , paramA2).where(paramB1, '==',paramB2).where(paramC1, '==',paramC2)
			).snapshotChanges()
			.pipe(map(actions => {
				return actions.map((a: any) => {
					const data = a.payload.doc.data();
					const id = a.payload.doc.id;
					return {id, data};
				})
			}));
	}
	
	getCollectionWith4QueryWithDocId(col, paramA1, paramA2, paramB1, paramB2, paramC1, paramC2, paramD1, paramD2) {
		return this._fire
			.collection(col, ref => ref
				.where(paramA1, '==' , paramA2).where(paramB1, '==',paramB2).where(paramC1, '==',paramC2).where(paramD1, '==',paramD2)
			).snapshotChanges()
			.pipe(map(actions => {
				return actions.map((a: any) => {
					const data = a.payload.doc.data();
					const id = a.payload.doc.id;
					return {id, data};
				})
			}));
	}

	getCollectionWithDocId(col) {
		return this._fire
			.collection(col)
			.snapshotChanges()
			.pipe(map(actions => {
				return actions.map((a: any) => {
					const data = a.payload.doc.data();
					const id = a.payload.doc.id;
					return {id, data};
				})
			}));
	}

	getDocId(col, doc, subcol, key: string, value: string) {
		return this._fire
			.collection(col)
			.doc(doc)
			.collection(subcol, ref => ref.where(key, '==', value))
			.get();
	}

	getDocumentById(col, doc) {
		return this._fire
			.collection(col)
			.doc(doc)
			.valueChanges();
	}

	getSubCollection(col, doc, subcol) {
		return this._fire
			.collection(col)
			.doc(doc)
			.collection(subcol)
			.valueChanges();
	}
	
	getSubCollectionDoc(col, doc, subcol, doc2) {
		return this._fire
			.collection(col)
			.doc(doc)
			.collection(subcol)
			.doc(doc2)
			.valueChanges();
	}

	getSubCollectionReference(col, doc, subcol) {
		return this._fire
			.collection(col)
			.doc(doc)
			.collection(subcol)
			.get();
	}

	getSubCollectionOrdyBy(col, doc, subcol) {
		return this._fire
			.collection(col)
			.doc(doc)
			.collection(subcol, (ref) => {
				return ref.orderBy('date', 'desc')
			})
			.snapshotChanges()
			.pipe(map(actions => {
				return actions.map((a: any) => {
					const data = a.payload.doc.data();
					const id = a.payload.doc.id;
					return {id, data};
				})
			}));
	}

	getSubCollectionWithDocIdOrderByDate(col, doc, subcol) {
		return this._fire
			.collection(col)
			.doc(doc)
			.collection(subcol, ref => {
				return ref.orderBy('datum','desc')
			})
			.snapshotChanges()
			.pipe(map(actions => {
				return actions.map((a: any) => {
					const data = a.payload.doc.data();
					const id = a.payload.doc.id;
					return {id, data};
				})
			}));
	}

	getSubCollectionOrderByDate(col, doc, subcol) {
		return this._fire
			.collection(col)
			.doc(doc)
			.collection(subcol, ref => {
				return ref.orderBy('datum','desc')
			})
			.valueChanges();
	}

	getSubCollectionWithDocId(col, doc, subcol) {
		return this._fire
			.collection(col)
			.doc(doc)
			.collection(subcol)
			.snapshotChanges()
			.pipe(map(actions => {
				return actions.map((a: any) => {
					const data = a.payload.doc.data();
					const id = a.payload.doc.id;
					return {id, data};
				})
			}));
	}

	workAroundToPreventItalicFlagInDB(col, falohoId: string) {
		return this._fire
			.collection(col)
			.doc(falohoId)
			.set({'workaroud': ''});
	}

	updateSubcollectionComplete(col, doc, subcol, subDocId: string, obj: {}) {
		return this._fire
			.collection(col)
			.doc(doc)
			.collection(subcol)
			.doc(subDocId)
			.update(obj);
	}

	updateCollectionProperty(col, doc, property: string, value: any) {
		return this._fire
			.collection(col)
			.doc(doc)
			.update({
				[property]: value
			})
	}
	
	updateCollectionTwoProperty(col, doc, property: string, value: any, property2: string, value2: any) {
		return this._fire
			.collection(col)
			.doc(doc)
			.update({
				[property]: value,
				[property2]: value2
			})
	}

	updateObj(col, doc, obj: {}) {
		return this._fire
			.collection(col)
			.doc(doc)
			.update(obj);
	}

	updateSubcollection(col, doc, subcol, subDocId: string, property: string, value: boolean | string | number) {
		return this._fire
			.collection(col)
			.doc(doc)
			.collection(subcol)
			.doc(subDocId)
			.update({
				[property]: value
			})
	}
}

// addWithFalohoId() {
// 	this.falohe.forEach((faloho, id) => {
// 		id++;
// 		let falohoId = id.toString();
// 		this._fire.collection('falohe')
// 			.doc(falohoId)
// 			.set(faloho);
//
// 		faloho['filialen'].forEach((filiale, filialeId) => {
// 			filialeId++;
// 			this._fire.collection('falohe')
// 				.doc(falohoId)
// 				.collection('filialen')
// 				.doc('filialeId_'+filialeId)
// 				.set(filiale);
// 		});
// 	});
// }

// addInDatabaseGeneratedAutoID() {
// 	this.falohe.forEach((userObjuserObj) => {
// 		this._fire
// 			.collection('falohe')
// 			.add(userObjuserObj);
// 	});
// }

// getLengthOfDocuments() {
// 	return this._fire.collection('feeds')
// 		.doc('1534581625684_2332')
// 		.collection('comments')
// 		.snapshotChanges()
// 		.subscribe((doc) => console.log(doc.length));
// }

// getAllCommentsFromDatabase() {
// 	return this._fire.collection('feeds')
// 		.snapshotChanges()
// 		.pipe(map(actions => {
// 			return actions.map(a => {
// 				const data = a.payload.doc.data();
// 				const id = a.payload.doc.id;
// 				return {id, data};
// 			})
// 		});
//
// }

// return this._fire.collection('feeds').doc().ref.firestore valueChanges();

// 	addComment(feedId) {
// 		return this._fire
// 			.collection('feeds').doc(feedId)
// 			.collection('comments').add({'text': 'commentText'});
// 	}
