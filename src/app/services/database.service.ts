import {Injectable} from '@angular/core';
import {FirebaseService} from "./firebase.service";
import {Collection, GLOBAL, USER} from "../global-data/global";
import {AuthService} from "../modules/authentification/auth.service";
import {LOCALSTORAGE} from "../global-data/localstorage-names";
import {ROUTES} from "../global-data/routes";
import {Storage} from "@ionic/storage";
import * as _ from 'lodash';
import {EventsService} from "./events.service";

@Injectable({
	providedIn: 'root'
})
export class DatabaseService {

	constructor(
		private auth: AuthService,
		private eventService: EventsService,
		private storage: Storage,
		private fire: FirebaseService,
	) {
	}

	getData(col: any) {
		return new Promise((resolve) => {
			this.fire.getCollection(col).subscribe((res) => {
				resolve(res);
			});
		});
	}

	getDataWithDocId(col) {
		return new Promise((resolve) => {
			this.fire.getDataFromDatabaseWithDocId(col).subscribe((res) => {
				resolve(res);
			});
		});
	}

	getAlleGemachtenAuftraegeDesTesters() {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWithQuery(Collection.AlleGemachtenAuftraege, 'email', await this.getEmail())
			.subscribe((res) => {
				resolve(res)
			});
		});
	}

	getAlleGemachtenAuftraegeDesTestersWithDocId() {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWith1QueryWithDocId(Collection.AlleGemachtenAuftraege, 'email', await this.getEmail())
			.subscribe((res) => {
				resolve(res)
			});
		});
	}

	getAlleGemachtenAuftraegeZuEinemApotheker(email) {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWithQuery(Collection.AlleGemachtenAuftraege, 'apothekerMail', email)
			.subscribe((res) => {
				resolve(res)
			});
		});
	}

	getAlleGemachtenAuftraegeZuEinemApothekerInEinerBestimmtenApotheke(apothekermail, apothekerId) {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWith2QueryWithDocId(Collection.AlleGemachtenAuftraege,
				'apothekerMail', apothekermail,
				'apothekenId', apothekerId)
				.subscribe((res) => {
					res.length ? resolve(res) : resolve([]);
				});
		});
	}
	
	getAlleGemachtenAuftraegeZuEinemApothekerInEinerBestimmtenApothekeOhne(apothekermail, apothekerId) {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWithTwoQuery(Collection.AlleGemachtenAuftraege,
				'apothekerMail', apothekermail,
				'apothekenId', apothekerId)
				.subscribe((res) => {
					res.length ? resolve(res) : resolve([]);
				});
		});
	}

	getAlleGemachtenAuftraegeEinerBestimmtenKrankheitZuEinemApothekerInEinerBestimmtenApotheke(apothekermail, apothekerId, krankheitsId) {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWith3QueryWithDocId(Collection.AlleGemachtenAuftraege,
				'apothekerMail', apothekermail,
				'krankheitsId', krankheitsId,
				'apothekenId', apothekerId)
				.subscribe((res) => {
					res.length ? resolve(res) : resolve([]);
				});
		});
	}
	
	getAlleGemachtenAuftraegeEinerBestimmtenKrankheitZuEinemApothekerInEinerBestimmtenApothekeOhne(apothekermail, apothekerId, krankheitsId) {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWithThreeQuery(Collection.AlleGemachtenAuftraege,
				'apothekerMail', apothekermail,
				'krankheitsId', krankheitsId,
				'apothekenId', apothekerId)
				.subscribe((res) => {
					res.length ? resolve(res) : resolve([]);
				});
		});
	}

	getAuszahlungenEinesBestimmtenTesters(email) {
		return new Promise(async (resolve) => {
			const $sub = this.fire.getCollectionWithQuery(Collection.Auszahlungen, 'email', email)
				.subscribe((res) => {
					resolve(res);
					$sub.unsubscribe();
				});
		});
	}
	
	getCurrentTester(email) {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWithQuery(Collection.Tester, 'email', email)
				.subscribe((res) => {
					resolve(res);
				});
		});
	}

	getCurrentApotheker() {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWithQuery(Collection.Apotheker, 'email', await this.getEmail()).subscribe((res) => {
				resolve(res);
			});
		});
	}

	getCurrentApotheken(apothekerMail) {
		return new Promise(async (resolve) => {
			if (GLOBAL.currentApotheken.length) {
				resolve(GLOBAL.currentApotheken)
			} else {
				this.fire.getCollectionWithQuery(Collection.Apotheken, 'apotheker', apothekerMail).subscribe((res) => {
					GLOBAL.currentApotheken = res;
					resolve(res)
				});
			}
		});
	}

	getAlleApotheken() {
		return new Promise(async (resolve) => {
			if (GLOBAL.Apotheken.length) {
				resolve(GLOBAL.Apotheken)
			} else {
				this.fire.getCollection(Collection.Apotheken).subscribe((res) => {
					GLOBAL.Apotheken = res;
					resolve(res)
				});
			}
		});
	}

	async getAuftraegeEinerBestimmtenKrankheitEinesApothekers(apothekermail, krankheitsId) {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWithTwoQuery(Collection.AlleGemachtenAuftraege,
				'apothekerMail', apothekermail,
				'krankheitsId', krankheitsId)
				.subscribe((res) => {
					res.length ? resolve(res) : resolve([]);
				});
		});
	}
	
	async getAuftraegeEinerBestimmtenKrankheitEinesApothekersWithDocId(apothekermail, krankheitsId) {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWith2QueryWithDocId(Collection.AlleGemachtenAuftraege,
				'apothekerMail', apothekermail,
				'krankheitsId', krankheitsId)
				.subscribe((res) => {
					res.length ? resolve(res) : resolve([]);
				});
		});
	}

	async getAuftraegeEinerBestimmtenApothekeEinesApothekers(apothekermail, apothekenId) {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWithTwoQuery(Collection.AlleGemachtenAuftraege,
				'apothekerMail', apothekermail,
				'apothekenId', apothekenId)
				.subscribe((res) => {
					res.length ? resolve(res) : resolve([]);
				});
		});
	}

	getAlleAuftraegeEinesMitarbeiters(mitarbeiter: string) {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWith2QueryWithDocId(Collection.AlleGemachtenAuftraege,
				'apothekerMail', await this.getEmail(),
				'select_mitarbeiter', mitarbeiter)
				.subscribe((res) => {
					resolve(res)
				});
		});
	}
	
	getAlleAuftraegeEinesMitarbeitersOhne(mitarbeiter: string) {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWithTwoQuery(Collection.AlleGemachtenAuftraege,
				'apothekerMail', await this.getEmail(),
				'select_mitarbeiter', mitarbeiter)
				.subscribe((res) => {
					resolve(res)
				});
		});
	}

	getAlleAuftraegeEinesMitarbeitersZurEinerBestimmtenKrankheit(mitarbeiter: string, krankheitsId: string) {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWith3QueryWithDocId(Collection.AlleGemachtenAuftraege,
				'apothekerMail', await this.getEmail(),
				'krankheitsId', krankheitsId,
				'select_mitarbeiter', mitarbeiter)
				.subscribe((res) => {
					resolve(res)
				});
		});
	}
	
	getAlleAuftraegeEinesMitarbeitersZurEinerBestimmtenKrankheitOhne(mitarbeiter: string, krankheitsId: string) {
		return new Promise(async (resolve) => {
			this.fire.getCollectionWithThreeQuery(Collection.AlleGemachtenAuftraege,
				'apothekerMail', await this.getEmail(),
				'krankheitsId', krankheitsId,
				'select_mitarbeiter', mitarbeiter)
				.subscribe((res) => {
					resolve(res)
				});
		});
	}

	initGlobaleTexte() {
		this.fire.getDocumentById(Collection.GlobaleTexte, Collection.doc.menupages).subscribe(res => {
			GLOBAL.MenuPages = res;
		});
		this.fire.getDocumentById(Collection.GlobaleTexte, Collection.doc.texte).subscribe((res) => {
			GLOBAL.GlobaleTexte = res;
		});
		this.fire.getDocumentById(Collection.GlobaleTexte, Collection.doc.formularueberschriften).subscribe((res) => {
			GLOBAL.Formularueberschriften = res;
		});
		this.fire.getDocumentById(Collection.GlobaleTexte, Collection.doc.anleitungstexte).subscribe((res) => {
			GLOBAL.Anleitungstexte = res;
		});
		this.fire.getDocumentById(Collection.GlobaleTexte, Collection.doc.rechnung).subscribe((res) => {
			GLOBAL.Rechnung = res;
		});
	}

	getLandingPageRoute() {
		return new Promise((resolve) => {
			if (USER.typ) {
				if(USER.typ === 'VIP') {
					resolve (ROUTES.vipTab1);
				} else if (USER.typ === 'APOTHEKER') {
					resolve (ROUTES.apoDashboard);
				} else if (USER.typ === 'TESTER') {
					this.storage.get(LOCALSTORAGE.introSlider).then((isIntroSliderChecked) => {
						resolve(isIntroSliderChecked ? ROUTES.customerTab1 : ROUTES.intro);
					});
				}
			} else {
				this.storage.get(LOCALSTORAGE.userTyp).then((userTyp: string) => {
					if(userTyp === 'VIP') {
						resolve (ROUTES.vipTab1);
					} else if (userTyp === 'APOTHEKER') {
						resolve (ROUTES.apoDashboard);
					} else if (userTyp === 'TESTER') {
						this.storage.get(LOCALSTORAGE.introSlider).then((isIntroSliderChecked) => {
							resolve(isIntroSliderChecked ? ROUTES.customerTab1 : ROUTES.intro);
						});
					}
				});
			}

			// function getUserTyp(userTyp) {
			// 	if(userTyp === 'VIP') {
			// 		return ROUTES.vipTab1;
			// 	} else if (userTyp === 'APOTHEKER') {
			// 		return ROUTES.apoDashboard
			// 	} else if (userTyp === 'TESTER') {
			// 		return ROUTES.customerTab1;
			// 		self.storage.get(LOCALSTORAGE.introSlider).then((isIntroSliderChecked) => {
			// 			return isIntroSliderChecked ? ROUTES.intro : ROUTES.customerTab1;
			// 		});
			// 	}
			// }
		});
	}

	getUserTyp(): any {
		return new Promise((resolve) => {
			console.log("BSHEYNO: " +LOCALSTORAGE.userTyp)
			this.storage.get(LOCALSTORAGE.userTyp).then((userTyp: string) => {
				resolve(userTyp);
			});
		});
	}

	categorizeTesterOrApotheker(email, userTyp = '') {
		return new Promise((resolve) => {
			if(userTyp === 'VIP') {
				USER.typ = 'VIP';
				this.storage.set(LOCALSTORAGE.userTyp, USER.typ).then(()=> {
					this.eventService.publish('pub:userTyp', {userTyp: USER.typ});
					resolve('egal');
				});
			} else {
				const $sub = this.fire.getCollectionWithQuery(Collection.Apotheker, 'email', email).subscribe((apotheker) => {
					USER.typ = (_.find(apotheker, {'email': email})) ? 'APOTHEKER' : 'TESTER';
					this.storage.set(LOCALSTORAGE.userTyp, USER.typ).then(()=> {
						this.eventService.publish('pub:userTyp', {userTyp: USER.typ});
						$sub.unsubscribe();
						resolve('egal');
					});
				});
			}
		});
	}

	getEmail(): Promise<string> {
		return new Promise((resolve) => {
			if (USER.email) {
				resolve(USER.email);
			} else if (this.auth.userDetails()) {
				this.auth.userDetails().then((user) => {
					if(user && user.email) {
						USER.email = user.email;
						resolve(USER.email);
					} else {
						this.storage.get(LOCALSTORAGE.userEmail).then(async (userEmail) => {
							USER.email = userEmail;
							userEmail ? resolve(USER.email) : resolve('');
						});
					}
				});
			} else {
				this.storage.get(LOCALSTORAGE.userEmail).then(async (userEmail) => {
					USER.email = userEmail;
					userEmail ? resolve(USER.email) : resolve('');
				});
			}
		});
	}

	setEmail(email: string) {
		USER.email = email;
		this.storage.set(LOCALSTORAGE.userEmail, USER.email);
	}

	extendResultByApothekenNamesForCheckboxen(res, apotheken) {
		let indexApotheke = _.findIndex(res, {subtyp: 'apotheke'});

		if (indexApotheke !== -1) {
			let extendWithApotheken = [];

			if (apotheken.length) {
				apotheken.forEach((apotheke) => {
					extendWithApotheken.push({
						label: apotheke.name,
						value: apotheke.formvalue
					});
				});
			}
			res[indexApotheke].apotheken = extendWithApotheken;
		}
		return res;
	}

	updateDataCurrentTester(email, dataObj) {
		return new Promise((resolve) => {
			this.fire.updateObj(Collection.Tester, email, dataObj)
			.then(() => resolve('success'))
			.catch(() => resolve('error'));
		});
	}

	updateCollection(col, doc, obj) {
		return new Promise((resolve) => {
			this.fire.updateObj(col, doc, obj)
			.then(() => resolve('success'))
			.catch(() => resolve('error'));
		});
	}
}
