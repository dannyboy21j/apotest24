import {Injectable} from '@angular/core';
import {FirebaseService} from "./firebase.service";
import {Collection} from "../global-data/global";

@Injectable({
	providedIn: 'root'
})
export class AdddataindatabaseService {
	ALLEKRANKHEITEN = [
		'abfuehrmittel',
		'blasenentzuendung',
		'durchfall',
		'fusspilz',
		'haemorrhoiden',
		'halsschmerzen',
		'heuschnupfen',
		'husten',
		'hustenreiz',
		'kopfschmerzen',
		'lippenherpes',
		'nasenneben',
		'nasenspray',
		'sodbrennen',
		'zahnschmerzen'
	];
	
	KRANKHEITEN = [
		{
			docId: '',
			id: 'hustenreiz',
			subtitle: '',
			title: 'Hustenreiz'
		}
	];
	
	PRODUKTE = [
		{
			id: 'blasenentzuendung',
			checkboxen: [
				'Cystinol akut Tabletten',
				'Aqua libra Tabletten',
				'Sidroga Blasen und Nierentee',
				'Granufink Cranberry Kürbis Kapseln',
				'Ein anderes Produkt',
			]
		}
	];
	
	APOTHEKER = {
		angelegtAm: new Date(),
		vorname: '',
		nachname: '',
		email: '',
		preise: [
			['abfuehrmittel', 2],
			['blasenentzuendung', 2],
			['durchfall', 2],
			['fusspilz', 2],
			['haemorrhoiden', 2],
			['halsschmerzen', 2],
			['heuschnupfen', 2],
			['husten', 2],
			['hustenreiz', 2],
			['kopfschmerzen', 2],
			['lippenherpes', 2],
			['nasenneben', 2],
			['nasenspray', 2],
			['sodbrennen', 2],
			['zahnschmerzen', 2],
		]
	};
	
	constructor(
		private fire: FirebaseService,
	) {
	}
	
	addKrankheit() {
		for(let thema of this.KRANKHEITEN) {
			let docId = this.fire.getBeforeDocumentId(Collection.Krankheiten);
			thema.docId = docId;
			this.fire.addCollection(Collection.Krankheiten, thema);
		}
	}
	
	addProdukteForKrankheit() {
		let fixObj = {
			typ: 'checkbox',
			checkbox_produktempfehlung: false,
			formarray: 'checkbox_produktempfehlung',
			title: 'checkbox_produktempfehlung',
			order: 5,
			required: true
		};
		
		for(let produkt of this.PRODUKTE) {
			const mergedObj = Object.assign(produkt, fixObj)
			this.fire.addCollection(Collection.FormElementsGenerellProdukte, mergedObj);
		}
	}
	
	deleteAuftraege() {
		this.fire.getCollectionWithDocId(Collection.AlleGemachtenAuftraege).subscribe((res)=>{
			for(let item of res) {
				// this.fire.deleteDocument(Collection.AlleGemachtenAuftraege,item.id);
			}
		});
	}
}
