import {Injectable} from '@angular/core';
import {AlertController, NavController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class AlertService {

    constructor(
        private alertCtrl: AlertController,
        private navCtrl: NavController
    ) {
    }

    async presentAlert(header, subHeader, message, buttonText) {
        const alert = await this.alertCtrl.create({
            header: header,
            subHeader: subHeader,
            message: message,
            buttons: [{
                text: buttonText,
                handler: () => {
                    alert.dismiss().then(() => {
                        this.navCtrl.pop();
                    });
                    return false;
                }
            }]
        });
        await alert.present();
    }

    async presentAlertConfirm(title: string, msg: string) {
        const alert = await this.alertCtrl.create({
            header: title,
            message: msg,
            buttons: [
                {
                    text: 'Nein',
                    role: 'nein',
                    cssClass: 'secondary',
                    handler: () => alert.dismiss(true)
                },
                {
                    text: 'Ja',
                    role: 'ja',
                    handler: () => alert.dismiss(true)
                }
            ]
        });

        await alert.present();
        return await alert.onDidDismiss();
    }
}
