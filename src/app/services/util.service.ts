import {Injectable} from '@angular/core';
import * as _ from 'lodash';

@Injectable({
	providedIn: 'root'
})
export class UtilService {
	capitalizeFirstLetter(string) {
		if (typeof string !== 'string') return '';
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
	}

	convertSetToArray(set: Set<any>) {
		return Array.from(set);
	}
	
	convertStringNumberToNumber(obj) {
		let newObj = {};
		
		for(var key in obj) {
			if (/^\d+$/.test(obj[key]) || /^\d+\.\d+$/.test(obj[key])) {
				newObj[key] = Number(obj[key].trim());
			} else {
				newObj[key] = obj[key].trim()
			}
		}
		return newObj;
	}

	getslicedMap(start: number, end: number, map: Map<any,any>) {
		return Array
			.from(map.keys()).slice(start, end)
			.reduce((m, k) => m.set(k, map.get(k)), new Map);
	}

	getKeyByValue(obj, searchItem) {
		return _.findKey(obj, (val) => val === searchItem);
	}

	getMaxKeyInObject(obj) {
		return _.max(Object.keys(obj).map(Number));
	}

	getDateTimeWithoutSecond() {
		var dt = new Date();
		var d = dt.toLocaleDateString();
		var t = dt.toLocaleTimeString();
		t = t.replace(/\u200E/g, '');
		t = t.replace(/^([^\d]*\d{1,2}:\d{1,2}):\d{1,2}([^\d]*)$/, '$1$2');
		var result = d + ' ' + t;
		return result;
	}

	goToExternalUrl(url: string) {
		// window.open(url, '_blank');
		window.location.href = url;
	}

    isDefined(data) {
		return typeof data != 'undefined' || data != null;
    }
    
    isPriceValid(value) {
	    return (/^[0-9,.]*$/.test(value));
    }
    

    areAllObjectsInArrayEmpty(array: Array<object>): boolean {
		let result = _.map(array, (obj) => { return _.isEmpty(obj)});
		return _.every(result);
	}

    isValueInObjectExist(obj: {}, value: string) {
		return _.includes(obj, value);
	}

	isValueInArrayObjectExist(obj: Array<object>, key: string, value: string) {
		return _.some(obj, {[`${key}`]: value});
	}

	objectToMap(obj) {
		const mp = new Map();
		Object.keys(obj).forEach(k => { mp.set(k, obj[k]) });

		return mp;
	}

	mapToObject(myMap: Map<any,any>): {} {
		const obj = {};
		myMap.forEach((v,k) => { obj[k] = v });

		return obj;
	}

	reloadPage(time: number) {
		setTimeout(() => {
			window.location.reload();
		}, time);
	}

	removeLastCharacter(str: string): string {
		return str.slice(0,-1);
	}

    removeSpecificItemFromArray(faloheSet: Array<string>, item: string) {
        let indexOfItem = _.indexOf(faloheSet, item);
        faloheSet.splice(indexOfItem, 1);
        return faloheSet;
    }

    replaceNewLineToBrTags(str: string): string {
		return str.replace(/(?:\r\n|\r|\n)/g, '<br>')
	}

	replaceBrTagsToNewLine(str: string): string {
		return str.replace(/<br>/g,'\n');
	}

    sortOeffnungszeiten(offnungszeiten: Array<string>): Array<string> {
		let sortedOffnungszeiten = []
		let days = ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So'];

		for (let day of days) {
			sortedOffnungszeiten.push(offnungszeiten[_.findLastIndex(offnungszeiten,
				(o) => { return o.includes(day);
			})]);
		}

		return sortedOffnungszeiten;
	}

	sortiereArrayNachHoechsterFalohoId(dataFromDB): Array<object> {
		return _.sortBy(dataFromDB, (obj) => {
			return parseInt(obj.id, 10);
		});
	}
	
	trimAllValuesInObject(obj: {}) {
		let newObj = {};
		for(var key in obj) {
			if(_.isArray(obj[key])) {
				newObj[key] = this.trimArrayItems(obj[key]);
			} else if(_.isString(obj[key])) {
				newObj[key] = obj[key].trim()
			} else if(_.isNumber(obj[key])) {
				newObj[key] = obj[key];
			}
		}
		return newObj;
	}
	
	trimArrayItems(arr) {
		return _.map(arr, _.trim);
	}

    convertPlainObjectToMap(obj: {number,string}) {
		return new Map(Object.entries(obj));
	}
}
