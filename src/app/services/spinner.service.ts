//https://ionicframework.com/docs/api/components/loading/LoadingController/

//  spinner					string		The name of the SVG spinner for the loading indicator.
// 	content					string		The html content for the loading indicator.
// 	cssClass				string		Additional classes for custom styles, separated by spaces.
// 	showBackdrop			boolean		Whether to show the backdrop. Default true.
// 	enableBackdropDismiss	boolean		Whether the loading indicator should be dismissed by tapping the backdrop. Default false.
// 	dismissOnPageChange		boolean		Whether to dismiss the indicator when navigating to a new page. Default false.
// 	duration				number		How many milliseconds to wait before hiding the indicator. By default, it will show until dismiss() is called.

import {Injectable} from '@angular/core';
import {LoadingController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class SpinnerService {
    spinner: any;

    constructor(private loadingCtrl: LoadingController) {
    }

    async show() {
        this.spinner = await this.loadingCtrl.create({
            spinner: 'dots',
            duration: 5000,
            showBackdrop: true,
            message: 'Please wait...',
            translucent: true,
            cssClass: 'custom-class custom-loading'
        });
        return await this.spinner.present();
    }

    hide() {
        // Fixed Runtime Error Uncaught (in promise): removeView was not found
        this.spinner.dismiss().catch(() => {
        });
    }
}
