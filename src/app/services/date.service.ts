import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DateService {

    getFormatedDate(date) {
        return (date) ? date.toDate().toLocaleDateString() : '';
    }

    getGreetingTime() {
        var time = new Date().getHours();

        if (time < 5) {
            return 'Lalyo Tobo';
        } else if (time < 11) {
            return 'Brikh Safro';
        } else if (time < 19) {
            return 'Shlomo';
        } else if (time < 23) {
            return 'Brikh Ramsho'
        } else {
            return 'Lalyo Tobo';
        }
    }

    isToday(date) {
        if (date) {
            let dateInDB = date.toDate();
            let today = new Date();
            return (today.toDateString() == dateInDB.toDateString());
        }
        return false;
    }

    isToday2(date: Date) {
        if (date) {
            let today = new Date();
            return (today.toDateString() == date.toDateString());
        }
        return false;
    }

    transformDate(datum: string) {
        if (datum) {
            let splitSymbol = datum.includes('.') ? '.' : datum.includes('/') ? '/' : datum.includes('-') ? '-' : '';

            if (splitSymbol) {
                let datumsArray: Array<string> = datum.split(splitSymbol),
                    tag = (datumsArray[0].length <= 1) ? '0' + datumsArray[0] : datumsArray[0],
                    monat = (datumsArray[1].length <= 1) ? '0' + datumsArray[1] : datumsArray[1],
                    jahr = datumsArray[2];

                return [tag, monat, jahr].join('.');
            } else {
                return datum;
            }
        }
    }

    transform(date: any) {
        let datum = date.toDate();

        // weekday: 'long',
        // 	month: 'long',
        const options = {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric'
        };

        return this.transformDate(datum.toLocaleDateString('de-DE', options));
    }
    
    transform2(datum: any) {
        // weekday: 'long',
        // month: 'long',
        const options = {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric'
        };
        
        return this.transformDate(datum.toLocaleDateString('de-DE', options));
    }

    getProperDate(date: Date) {
        const options = {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric'
        };

        return this.transformDate(date.toLocaleDateString('de-DE', options));
    }

    transformTimestampToDate(date: any) {
        return new Date(date.seconds * 1000);
    }
}
