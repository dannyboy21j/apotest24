import {Injectable} from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class ImageService {
	
	getPreviewUrl(event: any): Promise<any> {
		return new Promise((resolve, reject) => {
			let fileData = (event.target) ? <File>event.target.files[0] : event;
			
			var mimeType = fileData.type;
			if (mimeType.match(/image\/*/) == null) {
				resolve("");
			}
			
			var reader = new FileReader();
			reader.readAsDataURL(fileData);
			reader.onload = (_event) => {
				resolve(reader.result);
			}
		});
	}
}
