import {Injectable} from '@angular/core';
import {ModalController} from "@ionic/angular";

@Injectable({
    providedIn: 'root'
})
export class ModalService {

    constructor(
        private modalCtrl: ModalController
    ) {
    }

    async showModal(component, obj) {
        const modal = await this.modalCtrl.create({
            component: component,
            componentProps: obj
        });

        await modal.present();
        return await modal.onDidDismiss();
    }
}
