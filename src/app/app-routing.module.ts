import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from "./modules/authentification/auth.guard";

const routes: Routes = [
	{
		path: '',
		redirectTo: 'login',
		pathMatch: 'full'
	},
	{
		path: 'tabs',
		loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule),
	},
	{
		path: 'login',
		loadChildren: () => import('./modules/authentification/login/login.module').then(m => m.LoginPageModule),
		canActivate: [AuthGuard],
	},
	{
		path: 'backoffice-landingpage',
		loadChildren: () => import('./pages/backoffice/landingpage/landingpage.module').then(m => m.LandingpagePageModule)
	},
	{
		path: 'backoffice-dashboard',
		loadChildren: () => import('./pages/backoffice/dashboard/dashboard.module').then(m => m.DashboardPageModule)
	},
	{
		path: 'backoffice-apothekeranlegen',
		loadChildren: () => import('./pages/backoffice/apothekeranlegen/apothekeranlegen.module').then(m => m.ApothekeranlegenPageModule)
	},
	{
		path: 'backoffice-apothekeanlegen',
		loadChildren: () => import('./pages/backoffice/apothekeanlegen/apothekeanlegen.module').then(m => m.ApothekeanlegenPageModule)
	},
	{
		path: 'backoffice-tester',
		loadChildren: () => import('./pages/backoffice/tester/tester.module').then(m => m.TesterPageModule)
	},
	{
		path: 'backoffice-testerdata',
		loadChildren: () => import('./pages/backoffice/tester/testerdata/testerdata.module').then(m => m.TesterdataPageModule)
	},
	{
		path: 'backoffice-auftragsschablonen',
		loadChildren: () => import('./pages/backoffice/auftragsschablonen/auftragsschablonen.module').then(m => m.AuftragsschablonenPageModule)
	},
	{
		path: 'backoffice-auftragsschablonedetails',
		loadChildren: () => import('./pages/backoffice/auftragsschablonen/auftragsschablonedetails/auftragsschablonedetails.module').then(m => m.AuftragsschablonedetailsPageModule)
	},
	{
		path: 'backoffice-apotheker',
		loadChildren: () => import('./pages/backoffice/apotheker/apotheker.module').then(m => m.ApothekerPageModule)
	},
	{
		path: 'backoffice-apothekerdetails',
		loadChildren: () => import('./pages/backoffice/apotheker/apothekerdetails/apothekerdetails.module').then(m => m.ApothekerdetailsPageModule)
	},
	{
		path: 'menupage/:id',
		loadChildren: () => import('./pages/shared/menupage/menupage.module').then(m => m.MenupagePageModule)
	},
	{
		path: 'intro',
		loadChildren: () => import('./pages/customer/intro/intro.module').then(m => m.IntroPageModule)
	}
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
	],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
