import {Component} from '@angular/core';
import {NavController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {ROUTES} from "./global-data/routes";
import {AuthService} from "./modules/authentification/auth.service";
import {GLOBAL, USER} from "./global-data/global";
import {DatabaseService} from "./services/database.service";
import {LOCALSTORAGE} from "./global-data/localstorage-names";
import {Storage} from "@ionic/storage";
import {FirebaseService} from "./services/firebase.service";
import {AdddataindatabaseService} from "./services/adddataindatabase.service";

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    ROUTES = ROUTES;
    GLOBAL = GLOBAL;
    USER = USER;
    email: string = '';

    public menuLinks = [
        {
            title: 'App Anleitung',
            url: '/menupage',
            id: 'anleitung',
            icon: ''
        },
        {
            title: 'Impressum',
            url: '/menupage',
            id: 'impressum',
            icon: ''
        },
        {
            title: 'Datenschutz',
            url: '/menupage',
            id: 'datenschutz',
            icon: ''
        },
    ];

    constructor(
        private auth: AuthService,
        private fire: FirebaseService,
        private database: DatabaseService,
        private navCtrl: NavController,
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private storage: Storage,
        private addService: AdddataindatabaseService
    ) {
        this.initializeApp();
    }

    async initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.database.initGlobaleTexte();
        });
    }
    
    async isUserLogin() {
        return await this.database.getEmail();
    }
    
    signOut() {
        this.storage.remove(LOCALSTORAGE.userEmail);
        this.storage.remove(LOCALSTORAGE.userTyp);
        this.storage.remove(LOCALSTORAGE.tabIndex);
        this.auth.SignOut();
    }
}
