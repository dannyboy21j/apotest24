export const GLOBAL = {
	currentApotheken: [],
	Apotheker: [],
	Apotheken: [],
	Tester: [],
	AlleGemachtenAuftraege: [],
	GlobaleTexte: undefined,
	MenuPages: undefined,
	Anleitungstexte: undefined,
	Rechnung: undefined,
	Formularueberschriften: undefined
};

export const USER = {
	typ: '',
	email: '',
};

export const Collection = {
	Apotheker: 'Apotheker',
	Apotheken: 'Apotheken',
	AlleGemachtenAuftraege: 'AlleGemachtenAuftraege',
	Auszahlungen: 'Auszahlungen',
	FormElementsGenerell: 'FormElementsGenerell',
	FormElementsSpeziell: 'FormElementsSpeziell',
	FormElementsGenerellProdukte: 'FormElementsGenerellProdukte',
	GlobaleTexte: 'GlobaleTexte',
	Krankheiten: 'Krankheiten',
	KundenNrCounter: 'KundenNrCounter',
	Tester: 'Tester',
	doc: {
		formularueberschriften: 'formularueberschriften',
		texte: 'texte',
		menupages: 'menupages',
		anleitungstexte: 'anleitungstexte',
		rechnung: 'rechnung',
	},
	sub: {
		formelements: 'formelements',
	}
};
