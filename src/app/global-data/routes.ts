const customer = '/tabs/customer/';
const apotheker = '/tabs/apotheker/';
const vip = '/tabs/vip/';

export const ROUTES = {
	login:  					'/login',
	menupage:  					'/menupage',
	
	customerTab1:               customer + 'tab1',
	customerApotheke:  		    customer + 'customer-apotheke',
	customerSucheApotheke:	    customer + 'customer-sucheapotheke',
	customerNeuerAuftrag:  		customer + 'neuer-auftrag',
	
	apoDashboard:               apotheker + 'apo-dashboard',
	apoTab1:                    apotheker + 'tab1',
	apoTab2:                    apotheker + 'tab2',
	apoTab3:                    apotheker + 'tab3',
	apoProfil:                  apotheker + 'profil',
	apoAnleitung:               apotheker + 'anleitung',
	apoAuftraege:  			    apotheker + 'apo-auftraege',
	apoAuswertungLandingpage:  	apotheker + 'apo-auswertung-landingpage',
	apoZwischenseiteKrankheiten:apotheker + 'apo-zwischenseite-krankheiten',

	vipTab1:                    vip + 'tab1',
	vipAuftraege:               vip + 'vip-auftraege',
	vipListing:                 vip + 'vip-listing',
	vipAuszahlung:              vip + 'vip-auszahlung',
	vipRechnung:                vip + 'vip-rechnung',
	
	sharedErledigterAuftrag:    '/tabs/erledigter-auftrag',
	
	intro:                      'intro',
	
	backoffice:  				'/backoffice-landingpage',
	backofficeDashboard:		'/backoffice-dashboard',
	backofficeApothekerAnlegen:	'/backoffice-apothekeranlegen',
	backofficeApothekeAnlegen:	'/backoffice-apothekeanlegen',
	backofficeApotheker:	    '/backoffice-apotheker',
	backofficeApothekerDetails: '/backoffice-apothekerdetails',
	backofficeTester:	        '/backoffice-tester',
	backofficeTesterData:	    '/backoffice-testerdata',
	backofficeAuftragsschablonen:'/backoffice-auftragsschablonen',
	backofficeAuftragsschablonenDetails:'/backoffice-auftragsschablonedetails',
};
