export const REGEX = {
	email: '[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$',
	facebook: '^(http(s)?:\/\/)?(www\.)?facebook\.com\/.{1,150}',
	free: '.*',
	mindestZeichen: '.{6,}',
	name: '[^<>?$,.#+/%()]+',
	noNumbers: '^([^0-9]*)$',
	mobile: '^((\+49-?)|0)?[0-9]{10}$',
	plz: '\\d{5}',
	tel: '[0-9]{2,5}(-|/){0,1}[0-9]{3,12}',
	url: '^(http(s)?://)?(www.)?(.)+(.)([a-z]){2,4}(/)?',
	vierZahlen: '\\d{4}'
};
