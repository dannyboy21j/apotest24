import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../auth.service";
import {Collection, USER} from "../../../global-data/global";
import {Storage} from "@ionic/storage";
import {ToastService} from "../../../services/toast.service";
import {NavController} from "@ionic/angular";
import {FirebaseService} from "../../../services/firebase.service";
import {DatabaseService} from "../../../services/database.service";
import {ROUTES} from "../../../global-data/routes";
import {rejects} from "assert";
import {LOCALSTORAGE} from "../../../global-data/localstorage-names";

@Component({
	selector: 'app-login',
	templateUrl: './login.page.html',
	styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
	USER = USER;
	ROUTES = ROUTES;
	hide = true;
	email;
	password;
	password2;
	showLogin = true;
	showRegistration = false;
	showPasswortVergessen = false;
	
	myForm: FormGroup;
	myForm2: FormGroup;
	myForm3: FormGroup;
	
	errorMessageSignIn: string = '';
	errorMessageSignUp: string = '';
	showPasswortVergessenTab: boolean = false;
	tabIndex: number = 0;
	
	validation_messages = {
		'email': [
			{type: 'required', message: 'Email is required.'},
			{type: 'pattern', message: 'Please enter a valid email.'}
		],
		'password': [
			{type: 'required', message: 'Password is required.'},
			{type: 'minlength', message: 'Password must be at least 5 characters long.'}
		]
	};
	
	errorCodes = {
		wrongPassword: {
			code: 'auth/wrong-password',
			message: 'Sie haben ein falsches Passwort eingegeben'
		},
		passwordNotEqual: 'Passwörter stimmen nicht überein',
		default: 'Sie haben falsche Zugangsdaten eingebeneben. Versuchen Sie es nochmal'
	};
	
	constructor(
		private auth: AuthService,
		private database: DatabaseService,
		private fire: FirebaseService,
		private fb: FormBuilder,
		private navCtrl: NavController,
		private storage: Storage,
		private toast: ToastService
	) {
	}
	
	ngOnInit() {
		this.myForm = this.fb.group({
			// Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
			email: new FormControl('', Validators.compose([Validators.required, Validators.email])),
			password: new FormControl('', Validators.compose([Validators.minLength(5), Validators.required]))
		});
		
		this.myForm2 = this.fb.group({
			email: new FormControl('', Validators.compose([Validators.required, Validators.email])),
			password: new FormControl('', Validators.compose([Validators.minLength(5), Validators.required])),
			password2: new FormControl('', Validators.compose([Validators.minLength(5), Validators.required])),
		});
		
		this.myForm3 = this.fb.group({
			email: new FormControl('', Validators.compose([Validators.required, Validators.email])),
		});
	}
	
	async onSubmitSignIn(form: FormGroup) {
		let email = form.value.email;
		let password = form.value.password;
		
		if (form.valid && email && password) {
			this.auth.SignIn(email, password)
				.then(async (res: { user: any }) => {
					if (res.user.email) {
						this.database.setEmail(res.user.email);
						if (res.user.uid === "yJCEt8IjRnUIKF6bRuGtYW38fnm2") {
							await this.database.categorizeTesterOrApotheker('', 'VIP');
						} else {
							await this.database.categorizeTesterOrApotheker(res.user.email);
						}
						this.navCtrl.navigateForward([await this.database.getLandingPageRoute()]);
						this.toast.presentToast('Sie sind angemeldet', 'success');
						this.errorMessageSignIn = '';
						form.reset();
					}
				})
				.catch((res) => {
					this.errorMessageSignIn = this.errorCodes.default;
					if (res.code === this.errorCodes.wrongPassword.code) {
						this.errorMessageSignIn = this.errorCodes.wrongPassword.message;
					}
				});
		} else {
			this.errorMessageSignIn = this.errorCodes.default;
			return false;
		}
	}
	
	onSubmitSignUp(form: FormGroup) {
		let email = form.value.email.trim();
		let password = form.value.password.trim();
		let password2 = form.value.password2.trim();
		
		if (form.valid && email && password && password2 && (password === password2)) {
			this.auth.SignUp(email, password).then(async (res: { user: any }) => {
					let email = (res && res.user && res.user.email) ? res.user.email : null;
					
					if (email) {
						this.database.setEmail(email);
						this.getKundennr().then(async(kundenNr) => {
							this.fire.setCollection(Collection.Tester, email, {'email': email, 'uid': res.user.uid, knr: kundenNr });
							await this.database.categorizeTesterOrApotheker(email);
							this.toast.presentToast('Sie sind angemeldet', 'success');
							this.navCtrl.navigateForward([await this.database.getLandingPageRoute()]);
							this.errorMessageSignUp = '';
							form.reset();
						}).catch(() => {this.toast.presentToast('Es gab Probleme: Bitte melden Sie sich bei uns', 'error'); });
					}
				})
				.catch(error => {
					this.errorMessageSignUp = error.message;
				});
		} else if (password !== password2) {
			this.errorMessageSignUp = this.errorCodes.passwordNotEqual;
			return false
		} else {
			this.errorMessageSignUp = this.errorCodes.default;
			return false;
		}
	}
	
	getKundennr() {
		return new Promise((resolve) => {
			this.fire.getCollection(Collection.KundenNrCounter).subscribe((res: any) => {
				
				if(res.length) {
					let kundenNrPlusEins = res[0].counter + 1;
					resolve(kundenNrPlusEins);
				} else {
					rejects('Danyel LOGIN ERROR');
				}
			});
		});
	}
	
	onSubmitResetPassword(form: FormGroup) {
		let email = form.value.email;
		
		if (form.valid && email) {
			this.auth.resetPassword(email);
			form.reset();
		} else {
			return false;
		}
	}
	
	getErrorMessage() {
		return this.myForm.get('email').hasError('required') ? 'You must enter a value' :
			this.email.hasError('email') ? 'Not a valid email' : '';
		// return this.email.hasError('required') ? 'You must enter a value' :
		//     this.email.hasError('email') ? 'Not a valid email' : '';
		//
	}
	
	passwortVergessen() {
		this.showPasswortVergessenTab = true;
		this.tabIndex = 2;
	}
	
	visibilityFormular(param) {
		if (param === 'LOGIN') {
			this.showLogin = true;
			this.showRegistration = false;
			this.showPasswortVergessen = false;
		} else if (param === 'REGISTRATION') {
			this.showLogin = false;
			this.showRegistration = true;
			this.showPasswortVergessen = false;
		} else if (param === 'VERGESSEN') {
			this.showLogin = false;
			this.showRegistration = false;
			this.showPasswortVergessen = true;
		}
	}
}
