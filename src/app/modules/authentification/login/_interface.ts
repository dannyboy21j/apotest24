export interface IUser {
	user: User;
	credential?: any;
	additionalUserInfo: AdditionalUserInfo;
	operationType: string;
}
export interface User {
	uid: string;
	displayName?: any;
	photoURL?: any;
	email: string;
	emailVerified: boolean;
	phoneNumber?: any;
	isAnonymous: boolean;
	tenantId?: any;
	providerData: ProviderData[];
	apiKey: string;
	appName: string;
	authDomain: string;
	stsTokenManager: StsTokenManager;
	redirectEventId?: any;
	lastLoginAt: string;
	createdAt: string;
}

export interface ProviderData {
	uid: string;
	displayName?: any;
	photoURL?: any;
	email: string;
	phoneNumber?: any;
	providerId: string;
}

export interface StsTokenManager {
	apiKey: string;
	refreshToken: string;
	accessToken: string;
	expirationTime: number;
}

export interface AdditionalUserInfo {
	providerId: string;
	isNewUser: boolean;
}


