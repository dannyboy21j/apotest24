import {Injectable, OnInit} from '@angular/core';
import {AngularFireAuth} from "@angular/fire/auth";
import {Observable} from "rxjs";
import {ROUTES} from "../../global-data/routes";
import {NavController} from "@ionic/angular";
import {ToastService} from "../../services/toast.service";
import {EventsService} from "../../services/events.service";

@Injectable({
    providedIn: 'root'
})
export class AuthService implements OnInit {
    userData: Observable<any>;

    constructor(
        private event: EventsService,
        private fire: AngularFireAuth,
        private navCtrl: NavController,
        private toast: ToastService,
    ) {
    }

    ngOnInit() {
        this.userData = this.fire.authState;
    }

    SignUp(email: string, password: string) {
        return this.fire.createUserWithEmailAndPassword(email, password);
    }

    SignIn(email: string, password: string) {
       return this.fire.signInWithEmailAndPassword(email, password);
    }

    SignOut() {
        this.fire.signOut().then(() => {
            this.toast.presentToast('Sie sind abgemeldet', 'success');
            this.navCtrl.navigateForward([ROUTES.login]);
            // setTimeout(() => {window.location.reload()}, 100)
        });
    }

    resetPassword(email: string) {
        this.fire.sendPasswordResetEmail(email).then((res) => {
            this.toast.presentToast('Wir haben Ihnen eine Mail geschickt', 'success');
        });
    }

    isUserSignIn() {
        this.fire.onAuthStateChanged((user: any) => {
            if (user) {
                // User is signed in.
                var displayName = user.displayName;
                var email = user.email;
                var emailVerified = user.emailVerified;
                var photoURL = user.photoURL;
                var isAnonymous = user.isAnonymous;
                var uid = user.uid;
                var providerData = user.providerData;
            } else {
                // User is signed out.
            }
        });
    }

    userDetails() {
        return this.fire.currentUser;
    }
    
    clear: any;
    getEmail() {
        this.clear = setInterval(() => {
            let email = this.fire.currentUser.then((res => {
                let email = res.email;
                
                if(email) {
                    this.event.publish('pub-email', {email});
                    clearInterval(this.clear);
                } else {
                    setTimeout(() => clearInterval(this.clear) , 5000);
                }
            }));
        }, 100);
    }
}
