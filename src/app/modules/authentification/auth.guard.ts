import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {AuthService} from "./auth.service";
import {NavController} from "@ionic/angular";
import {DatabaseService} from "../../services/database.service";
import {ROUTES} from "../../global-data/routes";
import {Storage} from "@ionic/storage";
import {USER} from "../../global-data/global";
import {LOCALSTORAGE} from "../../global-data/localstorage-names";
import {AngularFireAuth} from "@angular/fire/auth";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate{
    
    constructor(
        private auth: AuthService,
        private database: DatabaseService,
        private fire: AngularFireAuth,
        private navCtrl: NavController,
        private storage: Storage,
    ) {}

    async canActivate (next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        
        let isUserSignIn = await this.auth.userDetails();
        let userTyp = await this.storage.get(LOCALSTORAGE.userTyp);
        let isUserLogIn = (isUserSignIn || userTyp);
        
        if(state.url.includes('/tabs')) {
            let userTyp = await this.storage.get(LOCALSTORAGE.userTyp);
            
            let ermittelTypAnhandDerUrl = '';
            if(state.url.includes('customer')) {
                ermittelTypAnhandDerUrl = 'TESTER';
            } else if(state.url.includes('apotheker')) {
                ermittelTypAnhandDerUrl = 'APOTHEKER';
            } else if(state.url.includes('vip')) {
                ermittelTypAnhandDerUrl = 'VIP';
            }
            
            let stimmtUrlMitWertImLocalStorageUeberein = (userTyp === ermittelTypAnhandDerUrl);
            
            if(isUserLogIn && stimmtUrlMitWertImLocalStorageUeberein) {
                USER.email = await this.storage.get(LOCALSTORAGE.userEmail);
                return true;
            } else {
                this.navCtrl.navigateForward(ROUTES.login);
            }
        }
        else if(state.url === '/login') {
            if(isUserLogIn) {
                this.navCtrl.navigateForward([await this.database.getLandingPageRoute()]);
            } else {
                return true;
            }
        }
    }
}
