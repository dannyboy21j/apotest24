import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AngularFireAnalyticsModule} from "@angular/fire/analytics";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {AngularFireStorageModule} from "@angular/fire/storage";
import {AngularFireModule} from "@angular/fire";
import {environment} from "../../environments/environment.prod";


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFirestoreModule,
        AngularFireAnalyticsModule,
        AngularFireStorageModule,
        AngularFireAuthModule
    ]
})
export class FirebaseModule {
}
