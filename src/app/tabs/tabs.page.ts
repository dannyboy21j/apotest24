import {Component, OnInit, ViewChild} from '@angular/core';
import {DatabaseService} from "../services/database.service";
import {EventsService} from "../services/events.service";
import {Collection} from "../global-data/global";
import {FirebaseService} from "../services/firebase.service";
import {Storage} from "@ionic/storage";
import {LOCALSTORAGE} from "../global-data/localstorage-names";

@Component({
	selector: 'app-tabs',
	templateUrl: 'tabs.page.html',
	styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {
	tabs = ['tab1', 'tab2', 'tab3'];
	tabsCssClass = [];
	userTyp = undefined;
	userTypIsSet: boolean = false;
	data = {
		TESTER: {
			tabs: {
				tab1: 'customer/tab1',
				tab2: 'customer/tab2',
				tab3: 'customer/tab3'
			},
			icons: {
				tab1: 'search',
				tab2: 'cash',
				tab3: 'person'
			},
			labels: {
				tab1: 'Suche',
				tab2: 'Umsätze',
				tab3: 'Profil'
			},
			badge: {
				tab1: [false, 0],
				tab2: [false, 0],
				tab3: [false, 0],
			}
		},
		APOTHEKER: {
			tabs: {
				tab1: 'apotheker/tab2',
				tab2: 'apotheker/tab1',
				tab3: 'apotheker/tab3'
			},
			icons: {
				tab1: '/assets/icon/dashboard/apotheken.svg',
				tab2: '/assets/icon/dashboard/themen.svg',
				tab3: '/assets/icon/dashboard/team.svg'
			},
			labels: {
				tab1: 'Apotheken',
				tab2: 'Themen',
				tab3: 'Mitarbeiter'
			},
			badge: {
				tab1: [false, 0],
				tab2: [false, 0],
				tab3: [false, 0],
			}
		},
		VIP: {
			tabs: {
				tab1: 'vip/tab1',
				tab2: 'vip/tab2',
				tab3: 'vip/tab3'
			},
			icons: {
				tab1: 'home',
				tab2: 'cash',
				tab3: 'person'
			},
			labels: {
				tab1: 'Alle Apotheker',
				tab2: 'Auszahlungen',
				tab3: 'Profil'
			},
			badge: {
				tab1: [false, 0],
				tab2: [false, 0],
				tab3: [false, 0],
			}
		}
	};

	constructor(
		private database: DatabaseService,
		private eventService: EventsService,
		private fire: FirebaseService,
		private storage: Storage
	) {
		this.eventService.subscribe('pub:userTyp', (res) => {
			this.userTyp = res.userTyp;
			this.userTypIsSet = true;
			this.setFirstTabWithColor();
			this.showBadges();
		});
	}
	
	async ngOnInit() {
		this.userTyp = await this.storage.get(LOCALSTORAGE.userTyp);
		this.setFirstTabWithColor();
		this.showBadges()
	}

	async showBadges() {
		let email = await this.database.getEmail();
		
		if (this.userTyp === 'VIP') {
			this.fire.getCollectionWithQuery(Collection.AlleGemachtenAuftraege, 'genehmigungsStatus', 'inpruefung')
				.subscribe((res => {
					let showBadge = (res.length > 0);
					this.data.VIP.badge.tab1 = [showBadge, res.length];
					
					// Warum wird das gemacht? Um das Array "this.tabs" nochmal zu trigger
					this.tabs = ['tab1', 'tab2', 'tab3','wirdGleichGelöscht'];
					this.tabs.pop();
				}));
			
			this.fire.getCollectionWithQuery(Collection.Auszahlungen, 'genehmigungsStatus', 'beantragt')
				.subscribe((res => {
					let showBadge = (res.length > 0);
					this.data.VIP.badge.tab2 = [showBadge, res.length];
					
					// Warum wird das gemacht? Um das Array "this.tabs" nochmal zu trigger
					this.tabs = ['tab1', 'tab2', 'tab3','wirdGleichGelöscht'];
					this.tabs.pop();
				}));
		}
		
		if (this.userTyp === 'TESTER') {
			this.fire.getCollectionWithTwoQuery(Collection.AlleGemachtenAuftraege,
				'genehmigungsStatus', 'offen',
				'email', email)
				.subscribe((res => {
					let showBadge = (res.length > 0);
					this.data.TESTER.badge.tab2 = [showBadge, res.length];
				}));
		}
	}
	
	setFirstTabWithColor() {
		if (this.userTyp === 'APOTHEKER') {
			this.tabsCssClass = ['', '', ''];
		} else {
			this.tabsCssClass = ['primary', '', ''];
		}
	}
	
	setSelectedTab(tabNr: number) {
		this.tabsCssClass = ['', '', ''];
		if (this.userTyp === 'APOTHEKER') {
			this.tabsCssClass[tabNr] = tabNr === 0 ? 'primary' : tabNr === 1 ? 'orange' : tabNr === 2 ? 'success' : '';
		} else {
			this.tabsCssClass[tabNr] = 'primary';
		}
		this.storage.set(LOCALSTORAGE.tabIndex, tabNr);
	}
}
