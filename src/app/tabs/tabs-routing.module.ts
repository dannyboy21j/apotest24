import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TabsPage} from './tabs.page';
import {AuthGuard} from "../modules/authentification/auth.guard";

const routes: Routes = [
    {
        path: '',
        component: TabsPage,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'erledigter-auftrag',
                loadChildren: () => import('../pages/shared/erledigter-auftrag/erledigter-auftrag.module').then(m => m.ErledigterAuftragPageModule)
            },
            {
                path: 'customer',
                children: [
                    {
                        path: 'tab1',
                        loadChildren: () => import('../pages/customer/tab1/tab1.module').then(m => m.Tab1PageModule)
                    },
                    {
                        path: 'tab2',
                        loadChildren: () => import('../pages/customer/tab2/tab2.module').then(m => m.Tab2PageModule)
                    },
                    {
                        path: 'tab3',
                        loadChildren: () => import('../pages/customer/tab3/tab3.module').then(m => m.Tab3PageModule)
                    },
                    {
                        path: 'customer-apotheke',
                        loadChildren: () => import('../pages/customer/apotheke/apotheke.module').then(m => m.ApothekePageModule)
                    },
                    {
                        path: 'customer-kartenansicht',
                        loadChildren: () => import('../pages/customer/sucheapotheke/kartenansicht/kartenansicht.module').then(m => m.KartenansichtPageModule)
                    },
                    {
                        path: 'customer-sucheapotheke',
                        loadChildren: () => import('../pages/customer/sucheapotheke/sucheapotheke.module').then(m => m.SucheapothekePageModule)
                    },
                    {
                        path: 'neuer-auftrag',
                        loadChildren: () => import('../pages/customer/neuer-auftrag/neuer-auftrag.module').then(m => m.NeuerAuftragPageModule)
                    },
                ]
            },
            {
                path: 'apotheker',
                children: [
                    {
                        path: 'tab1',
                        loadChildren: () => import('../pages/apotheker/tab1/tab1.module').then(m => m.Tab1PageModule)
                    },
                    {
                        path: 'tab2',
                        loadChildren: () => import('../pages/apotheker/tab2/tab2.module').then(m => m.Tab2PageModule)
                    },
                    {
                        path: 'tab3',
                        loadChildren: () => import('../pages/apotheker/tab3/tab3.module').then(m => m.Tab3PageModule)
                    },
                    {
                        path: 'profil',
                        loadChildren: () => import('../pages/apotheker/profil/profil.module').then( m => m.ProfilPageModule)
                    },
                    {
                        path: 'anleitung',
                        loadChildren: () => import('../pages/apotheker/anleitung/anleitung.module').then( m => m.AnleitungPageModule)
                    },
                    {
                        path: 'apo-auswertung-landingpage',
                        loadChildren: () => import('../pages/apotheker/auswertung/landingpage/landingpage.module').then(m => m.LandingpagePageModule)
                    },
                    {
                        path: 'apo-zwischenseite-krankheiten',
                        loadChildren: () => import('../pages/apotheker/zwischenseitekrankheiten/zwischenseitekrankheiten.module').then(m => m.ZwischenseitekrankheitenPageModule)
                    },
                    {
                        path: 'apo-auftraege',
                        loadChildren: () => import('../pages/apotheker/auftraege/auftraege.module').then(m => m.AuftraegePageModule)
                    },
                    {
                        path: 'apo-dashboard',
                        loadChildren: () => import('../pages/apotheker/dashboard/dashboard.module').then( m => m.DashboardPageModule)
                    }
                ]
            },
            {
                path: 'vip',
                children: [
                    {
                        path: 'tab1',
                        loadChildren: () => import('../pages/vip/tab1/tab1.module').then(m => m.Tab1PageModule)
                    },
                    {
                        path: 'tab2',
                        loadChildren: () => import('../pages/vip/tab2/tab2.module').then(m => m.Tab2PageModule)
                    },
                    {
                        path: 'tab3',
                        loadChildren: () => import('../pages/vip/tab3/tab3.module').then(m => m.Tab3PageModule)
                    },
                    {
                        path: 'vip-auftraege',
                        loadChildren: () => import('../pages/vip/auftraege/auftraege.module').then(m => m.AuftraegePageModule)
                    },
                    {
                        path: 'vip-listing',
                        loadChildren: () => import('../pages/vip/listetester/listetester.module').then(m => m.ListetesterPageModule)
                    },
                    {
                        path: 'vip-auszahlung',
                        loadChildren: () => import('../pages/vip/auszahlung/auszahlung.module').then( m => m.AuszahlungPageModule)
                    },
                    {
                        path: 'vip-rechnung',
                        loadChildren: () => import('../pages/vip/rechnung/rechnung.module').then( m => m.RechnungPageModule)
                    }
                ]
            }
        ]
    },
    // {
    //     path: '',
    //     redirectTo: '/tabs/tab1',
    //     pathMatch: 'full'
    // }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TabsPageRoutingModule {
}
