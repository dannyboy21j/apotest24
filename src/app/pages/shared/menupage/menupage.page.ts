import {Component, OnInit} from '@angular/core';
import {GLOBAL} from "../../../global-data/global";
import {ActivatedRoute} from "@angular/router";

@Component({
	selector: 'app-menupage',
	templateUrl: './menupage.page.html',
	styleUrls: ['./menupage.page.scss'],
})
export class MenupagePage implements OnInit {
	id: string = 'datenschutz';
	GLOBAL = {
		MenuPages: {
			datenschutz: {
				title: '',
				content: ''
			}
		}
	}
	
	constructor(
		private router: ActivatedRoute,
	) {
	}
	
	ngOnInit() {
		this.router.params.subscribe((params) => {
			this.id = params.id;
			this.GLOBAL.MenuPages = GLOBAL.MenuPages;
		});
	}
	
}
