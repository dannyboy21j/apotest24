import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenupagePageRoutingModule } from './menupage-routing.module';

import { MenupagePage } from './menupage.page';
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		MenupagePageRoutingModule,
		ComponentsModule
	],
  declarations: [MenupagePage]
})
export class MenupagePageModule {}
