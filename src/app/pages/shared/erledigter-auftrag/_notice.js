var IFormElement = [
    {
        "order": 0,
        "required": false,
        "subtyp": "geschlecht",
        "title": "radio_geschlecht",
        "typ": "radio"
    },
    {
        "order": 1,
        "required": true,
        "subtyp": "janein",
        "title": "radio_weiterearzneimittel",
        "typ": "radio"
    },
    {
        "max": 20,
        "min": 2,
        "order": 2,
        "placeholder": "Langantwort-Text",
        "required": true,
        "textarea": true,
        "title": "textarea_feedback",
        "typ": "textarea"
    },
    {
        "order": 6,
        "required": true,
        "skala": true,
        "title": "skala_freundlichkeit",
        "typ": "skala"
    },
    {
        "max": 10,
        "min": 3,
        "order": 3,
        "placeholder": "Kurzantwort-Text",
        "required": false,
        "subtyp": "number",
        "title": "input_alter",
        "typ": "input"
    },
    {
        "order": 4,
        "required": true,
        "subtyp": "janein",
        "title": "radio_giveaway",
        "typ": "radio"
    },
    {
        "order": 5,
        "required": true,
        "subtyp": "apotheke",
        "title": "radio_apotheke",
        "typ": "radio"
    },
    {
        "checkboxen": [
            "Riopan2",
            "Gaviscon",
            "Nux Vumica",
            "Iberogast"
        ],
        "formarray": "produktempfehlung",
        "label": "Welche der untenstehenden Produkte wurde dir beim Kauf des Produkts vom Mitarbeiter empfohlen?",
        "title": "produktempfehlung",
        "typ": "checkbox"
    }
];

var GetaetigteAuftraege = [
    {
        "krankheitsId": "lFgcAvxu73ApqYUnlTKB",
        "input_alter": "333",
        "produktempfehlung": [
            true,
            true,
            true,
            false
        ],
        "radio_apotheke": "bußmann",
        "radio_geschlecht": "w",
        "radio_giveaway": "ja",
        "radio_weiterearzneimittel": "nein",
        "skala_freundlichkeit": "4",
        "textarea_feedback": "langantwort"
    }
];
