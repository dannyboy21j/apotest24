import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {FirebaseService} from "../../../services/firebase.service";
import {ToastService} from "../../../services/toast.service";
import {Collection} from "../../../global-data/global";
import {AuthService} from "../../../modules/authentification/auth.service";
import * as _ from 'lodash';
import {DatabaseService} from "../../../services/database.service";
import {NavController} from "@ionic/angular";

type TParams = {
	apothekenId: string,
	title: string,
	auftragsDocId: string,
	krankheitsId: string,
	krankheitDocId: string,
	apothekerMail: string,
	testerMail: string,
	genehmigungsStatus: string,
	korrekturbeschreibung: string,
	showSubmitButton: boolean,
	binIchVIP: string
};

@Component({
	selector: 'app-erledigter-auftrag',
	templateUrl: './erledigter-auftrag.page.html',
	styleUrls: ['./erledigter-auftrag.page.scss'],
})
export class ErledigterAuftragPage implements OnInit {
	slideOpts = {
		slidesPerView: 3,
		spaceBetween: 15,
		pager: false
	};
	myForm: FormGroup;
	auftragsDocId: string;
	krankheitDocId: string;
	apothekenId: string;
	krankheitsId: string;
	apothekerMail: string;
	testerMail: string;
	binIchVIP: boolean;
	korrekturbeschreibung: string;
	genehmigungsStatus: string;
	
	showSubmitButton: any;
	produktBilderRechnung = [];
	produktBilderImmobilie = [];
	mitarbeiter = [];
	
	produktempfehlung: FormArray;
	title: string;
	subtitle: string = '';
	skalaSize = _.range(5);

	jaNein = [{label: 'Ja', value: 'ja'}, {label: 'Nein', value: 'nein'}];
	geschlecht = [{label: 'weiblich', value: 'w'}, {label: 'männlich', value: 'm'}];

	apotheken = [
		{label: 'Bußmann Apotheke', value: 'bußmann'},
		{label: 'Steinhof Apotheke', value: 'steinhof'}
	];

	check = [
		{label: 'Bußmann Apotheke', value: 'bußmann'},
		{label: 'Steinhof Apotheke', value: 'steinhof'}
	];
	
	produktBilder = {
		immobilie: [],
		rechnung: []
	};
	showImageAmountHint = {
		immobilie: true,
		rechnung: true,
	};

	formElements: IFormElement[] = [];
	formTitle: any = {};
	resultForms: IFormElement[] = [];
	prefilledFormData = {};

	constructor(
		private auth: AuthService,
		private database: DatabaseService,
		private fire: FirebaseService,
		private formBuilder: FormBuilder,
		private navCtrl: NavController,
		private router: ActivatedRoute,
		private toast: ToastService
	) {
		this.myForm = this.formBuilder.group({
			produktempfehlung: this.formBuilder.array([])
		});
	}

	ngOnInit() {
		this.router.params.subscribe((params: TParams) => {
			this.auftragsDocId = params.auftragsDocId;
			this.krankheitsId = params.krankheitsId;
			this.title = params.title;
			this.binIchVIP = (params.binIchVIP === 'true');
			this.krankheitDocId = params.krankheitDocId;
			this.genehmigungsStatus = params.genehmigungsStatus;
			this.apothekerMail = params.apothekerMail;
			this.testerMail = params.testerMail;
			this.apothekenId = params.apothekenId || '1';
			this.showSubmitButton = params.showSubmitButton;
			this.korrekturbeschreibung = params.korrekturbeschreibung;
			this.genehmigungsStatus = params.genehmigungsStatus;

			if(this.showSubmitButton === 'false') {
				this.showSubmitButton = false;
			} else {
				this.showSubmitButton = true;
			}
			
			if(this.apothekerMail) {
				this.retrieveFormularContent();
				this.getMitarbeiterForDropDown();
			}
		});
	}

	onSubmit(form: FormGroup) {
		// TODO: Interface fehleranfällig, da in der DB die Felder geändert und hinzugefügt werden und das Interface entsprechend angepasst werden muss
		let formData = form.value;
		let targetObject = this.prefilledFormData;
		
		if(this.genehmigungsStatus === 'abgelehnt') {
			targetObject['genehmigungsStatus'] = 'inpruefung';
		}
		_.merge(targetObject, formData);

		if(formData) {
			this.fire.updateObj(Collection.AlleGemachtenAuftraege, this.auftragsDocId, targetObject)
				.then(() => {
					this.toast.presentToast('Daten erfolgreich gesendet', 'success', 2000)
					.then(() => this.navCtrl.back());
				});
		} else {
			this.toast.presentToast('Keine Daten zum Abschicken vorhanden', 'danger');
		}
	}

	retrieveFormularContent() {
		const sub$ = this.fire.getCollection(Collection.FormElementsGenerell)
			.subscribe((formelementsGenerell) => {
				this.fire.getCollectionWithTwoQuery(Collection.FormElementsSpeziell,
					'apotheker', this.apothekerMail,
					'id', this.krankheitsId
				).subscribe(async (formelementsSpeziell)=> {
					const mergedFormelements = formelementsGenerell.concat(formelementsSpeziell);
					this.setFormularContent(mergedFormelements);
					sub$.unsubscribe();
				});
		});
	}
	
	setFormularContent(mergedFormelements) {
		let formSchablonen = _.orderBy(mergedFormelements, ['order'], ['asc']);
		
		this.fire.getSpecificDocument(Collection.AlleGemachtenAuftraege, this.auftragsDocId)
			.then((res: any) => {
				let erledigterAuftrag: IAlleGemachtenAuftraege = res.data();
				var keys = Object.keys(erledigterAuftrag);
				this.produktBilderRechnung = erledigterAuftrag.photo_rechnung || [];
				this.produktBilderImmobilie = erledigterAuftrag.photo_immobilie || [];
				this.prefilledFormData = erledigterAuftrag;
				
				const sub$ = this.fire.getDocumentById(Collection.GlobaleTexte, Collection.doc.formularueberschriften)
					.subscribe((res2) => {
						this.formTitle = res2;
						let tmpResultForm = [];
						
						for (let schablone of formSchablonen) {
							if (_.includes(keys, schablone.title)) {
								tmpResultForm.push(schablone);
							}
						}
			
						this.resultForms = tmpResultForm;
						this.registerFormControlName();
						sub$.unsubscribe();
				});
		});
	}

	registerFormControlName() {
		let tmpObj = {};

		// Build FormControl ------------- NEW FORMCONTROL();
		this.resultForms.forEach((obj: IFormElement) => {
			let isRequired = obj.required ? Validators.required : null;
			if (obj.title) {
				tmpObj[obj.title] = new FormControl('', Validators.compose([isRequired]));
			}

			// Build - FormArray (Checkboxen) -------- NEW FORMARRAY();
			if (obj.typ === 'checkbox') {
				const formControls = obj.checkboxen.map((egal) => new FormControl(false));
				tmpObj[obj.formarray] = new FormArray(formControls);
			}
		});

		// Create FormBuilder Group ------- this.fb.group();
		this.myForm = this.formBuilder.group(tmpObj);
		this.preFillControls();
	}

	preFillControls() {
		this.resultForms.forEach(form => {
			const value = this.prefilledFormData[form.title];
			this.myForm.get(form.title).setValue(value);
		});

		// Beispiel ausführlich in Klartext:
		// this.myForm.get('input_alter').setValue('33');
		// this.myForm.get('radio_geschlecht').setValue('m');
		// this.myForm.get('radio_giveaway').setValue('nein');
		// this.myForm.get('radio_apotheke').setValue('bußmann');
		// this.myForm.get('skala_freundlichkeit').setValue('4');
		// this.myForm.get('produktempfehlung').setValue([false,true,true,false]);
	}
	
	getInputValue(title) {
		const pFD = this.prefilledFormData
		return (title === 'input_alter') ? pFD['input_alter'] : pFD['input_kosten'];
	}

	getControls() {
		return (<FormArray>this.myForm.get('formCheckboxControl')).controls;
	}
	
	testBestaetigen() {
		this.fire.updateCollectionProperty(Collection.AlleGemachtenAuftraege, this.auftragsDocId,
			'genehmigungsStatus', 'offen')
			.then(() => {
				this.toast.presentToast('Test wurde geprüft', 'success');
				this.navCtrl.back();
			});
	}
	
	testAblehnen() {
		if(this.korrekturbeschreibung == undefined) {
			this.korrekturbeschreibung = '';
		} else {
			this.fire.updateCollectionTwoProperty(Collection.AlleGemachtenAuftraege, this.auftragsDocId,
				'genehmigungsStatus', 'abgelehnt',
				'korrekturbeschreibung', this.korrekturbeschreibung).then(() => {
					this.toast.presentToast('Test wurde erfolgreich abgelehnt', 'success');
					this.navCtrl.back();
				});
		}
	}
	
	setKorrekturbeschreibung(value) {
		this.korrekturbeschreibung = value;
	}
	
	getMitarbeiterForDropDown() {
		const sub$ = this.fire.getCollectionWithQuery(Collection.Apotheker, 'email', this.apothekerMail)
			.subscribe((apotheker: any) => {
				let tmpMitarbeiter = [];
				
				Object.keys(apotheker[0]).forEach(key => {
					if (key.startsWith('mitarbeiter_')) {
						tmpMitarbeiter.push(apotheker[0][key]);
					}
				});
				this.mitarbeiter = _.sortBy(tmpMitarbeiter);
				
				sub$.unsubscribe();
			});
	}
	
	outputProduktBilder(produktBilder: Array<{ base64: string }>, subtyp: string) {
		this.produktBilder[subtyp] = produktBilder;
		this.isPhotosAmountCorrect(subtyp);
	}
	
	isPhotosAmountCorrect(subtyp: string) {
		let photoObj = _.filter(this.formElements, {typ: 'photo', subtyp});
		
		if (photoObj.length) {
			// Bitte mindestens x Bilder und maximal y Bilder
			if (this.produktBilder[subtyp].length === photoObj[0].maxPhotos) {
				this.showImageAmountHint[subtyp] = false;
			} else {
				this.showImageAmountHint[subtyp] = true;
			}
		}
	}
}
