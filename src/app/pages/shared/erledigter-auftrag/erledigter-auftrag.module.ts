import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ErledigterAuftragPageRoutingModule } from './erledigter-auftrag-routing.module';
import { ErledigterAuftragPage } from './erledigter-auftrag.page';
import {ComponentsModule} from "../../../components/components.module";
import {MatFormFieldModule} from "@angular/material/form-field";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		ErledigterAuftragPageRoutingModule,
		ComponentsModule,
		ReactiveFormsModule,
		MatFormFieldModule
	],
  declarations: [ErledigterAuftragPage]
})
export class ErledigterAuftragPageModule {}
