import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {ErledigterAuftragPage} from './erledigter-auftrag.page';

const routes: Routes = [
	{
		path: '',
		component: ErledigterAuftragPage
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ErledigterAuftragPageRoutingModule {
}
