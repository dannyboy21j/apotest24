import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { SucheapothekePageRoutingModule } from './sucheapotheke-routing.module';
import { SucheapothekePage } from './sucheapotheke.page';
import {MatTabsModule} from "@angular/material/tabs";
import {ListenansichtPageModule} from "./listenansicht/listenansicht.module";
import {KartenansichtPageModule} from "./kartenansicht/kartenansicht.module";
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		SucheapothekePageRoutingModule,
		ComponentsModule,
		MatTabsModule,
		ListenansichtPageModule,
		KartenansichtPageModule
	],
  declarations: [SucheapothekePage]
})
export class SucheapothekePageModule {}
