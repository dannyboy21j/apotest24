import {Component, NgZone, ViewChild} from '@angular/core';
import {IonSearchbar, NavController} from "@ionic/angular";
import * as $ from 'jquery';
import * as _ from 'lodash';
import {DatabaseService} from "../../../../services/database.service";
import {FirebaseService} from "../../../../services/firebase.service";
import {ROUTES} from "../../../../global-data/routes";
import {Collection} from "../../../../global-data/global";

@Component({
	selector: 'kartenansicht',
	templateUrl: './kartenansicht.page.html',
	styleUrls: ['./kartenansicht.page.scss'],
})
export class KartenansichtPage {
	@ViewChild('searchbar', {static: true}) searchbar: IonSearchbar;
	
	autocomplete: any;
	zoom: number;
	latitude: number;
	longitude: number;
	address: string;
	geoCoder;
	bounds: any;
	map: any;
	marker: any;
	infoWindow: any;
	allMarkers = [];
	infoWindows = [];
	wurdeGesuchtViaSearchfield = false;
	mapType = 'roadmap'; // roadmap || satellite  || hybrid || terrain
	alleApotheken: any; //[{id: string, data: {}}];
	
	constructor(
		private database: DatabaseService,
		private fire: FirebaseService,
		private navCtrl: NavController,
		private ngZone: NgZone
	) {
		let self = this;
		$('body').on('click', '.click-on-infowindow', function (event) {
			let queryParams = JSON.stringify($(this).data('apo'));
			self.navCtrl.navigateForward([ROUTES.customerApotheke, {queryParams}]);
		});
	}
	
	mapReady(map) {
		this.map = map;
		this.showInitalMarkerForAllApotheken();
		this.initAutoComplete();
		this.setLatLongGermany();
		this.listener();
	}
	
	async showInitalMarkerForAllApotheken() {
		this.alleApotheken = await this.database.getDataWithDocId(Collection.Apotheken);
		this.setMarkerAndInfoWindowForAll(this.alleApotheken);
	}
	
	async initAutoComplete() {
		this.autocomplete = new google.maps.places.Autocomplete(await this.searchbar.getInputElement(), {
			types: [] // 'establishment' / 'address' / 'geocode'
		});
		
		this.autocomplete.addListener('place_changed', () => {
			this.ngZone.run(() => {
				this.wurdeGesuchtViaSearchfield = true;
				const place: google.maps.places.PlaceResult = this.autocomplete.getPlace();
				
				if (place.geometry === undefined || place.geometry === null) return;
				
				this.latitude = place.geometry.location.lat();
				this.longitude = place.geometry.location.lng();
				
				// Zeige für alle Apotheken einen Marker, die in der ausgewählten Stadt existieren
				this.removeMarker();
				this.setForAllApothekenAnMarker(place.formatted_address, place.name);
				this.listener();
			});
		});
	}
	
	listener() {
		// SPRING IMMER ZUR MITTE
		this.map.addListener('centerchanged', () => {
			window.setTimeout(() => {
				this.map.panTo(this.marker.getPosition());
			}, 2000);
		});
		
		// CLICK AUF MARKER
		if (this.marker) {
			this.marker.addListener('click', (event) => {
				
				// this.map.setCenter(this.marker.getPosition());
				// this.getAdressDependOnZoomCheck(this.latitude, this.longitude);
				// this.map.setZoom(20);
			});
			
			// Könnte auch 'dragstart'
			this.marker.addListener('dragend', (evt) => {
				this.getAdressDependOnZoomCheck(evt.latLng.lat(), evt.latLng.lng());
			});
		}
		
		
		this.map.addListener('click', (evt) => {
			// this.removeMarker();
			this.getAdressDependOnZoomCheck(evt.latLng.lat(), evt.latLng.lng());
		});
	}
	
	addInfoWindow(marker, apo, index) {
		let apoData = JSON.stringify(apo.data);
		var infoContent = `
			<style>
				.click-on-infowindow {
  					font-size: 16px !important;
  					cursor: pointer;
				}
				.click-on-infowindow:hover {
  					text-decoration: underline;
				}
			</style>
			<div class='click-on-infowindow' data-apo='${apoData}'>
				<div><strong>${apo.data.name}</strong></div>
				<div>${apo.data.plz} ${apo.data.ort}</div>
				<div>${apo.data.str}</div>
			</div>
		`;
		
		let infoWindow = new google.maps.InfoWindow({
			content: infoContent,
			maxWidth: 370
		});
		
		this.infoWindows.push(infoWindow);
		
		google.maps.event.addListener(marker, 'click', () => {
			// Erstmal alle InfoWindows (Popups) löschen, damit nur ein InfoWindow geöffnet wird und nicht zwei oder mehr gleichzeitig
			this.infoWindows.forEach((info) => {
				info.close();
			})
			infoWindow.open(this.map, marker);
		});
	}
	
	setForAllApothekenAnMarker(address: string, name: string) {
		// mögliche Werte
		// "33415 Verl, Deutschland"
		// "48599 Gronau (Westfalen), Deutschland"
		// "Gütersloh, Deutschland"
		// "Berlin, Deutschland"
		// "Vegesack, Bremen, Deutschland"
		// "Blumenthal, Bremen, Deutschland"
		// "Gronau (Westfalen), 48599 Gronau (Westfalen), Deutschland"
		
		// PLZ ermitteln
		let extractNumbers = address.match(/\d/g),
			plz = (extractNumbers) ? extractNumbers.join("") : undefined,
		
			teile = name.split(' '),
			ort = teile.length == 2 ? teile[0].trim() : name,
			filteredApotheken = _.filter(this.alleApotheken, (obj) => {return obj.data.ort === ort});

		// Da es z.b Gronau in NRW und in einem anderen Bundesland existiert, wird nochmal die ersten zwei Zahlen der PLZ als Suchkriterium
		if(plz) {
			filteredApotheken = _.filter(this.alleApotheken, function (o) {
				return _.startsWith(o.data.plz, plz.slice(0, 2));
			});
		}
		this.setMarkerAndInfoWindowForAll(filteredApotheken);
	}
	
	setMarkerAndInfoWindowForAll(filteredApotheken) {
		// let apotheken = ["Neue Str. 17 28757 Bremen", "Hermann Fortmann Str. 41 28757 Bremen", "Beckerhookstr. 114, 48599 Gronau"];
		this.bounds = new google.maps.LatLngBounds();
		
		let marker;
		filteredApotheken.forEach((apo, index) => {
			// Wenn Latitude und Longitude in DB gespeichert sind, dann müssen der Geoservice genutzt werden
			if(apo.data.latitude) {
				setMarkerAndWindow(this, apo, apo.data.latitude, apo.data.longitude, index);
			} else {
				this.getLatLngByAddress(apo, `${apo.data.str},${apo.data.plz} ${apo.data.ort}`)
					.then((result: { latitude, longitude }) => {
						setMarkerAndWindow(this, apo, result.latitude, result.longitude, index);
				})
			}
		});
		
		function setMarkerAndWindow(self, apo, lat, long, index) {
			marker = self.setMarker(lat, long);
			self.addInfoWindow(marker, apo, index);
			self.bounds.extend(marker.position);
			self.map.fitBounds(self.bounds);
		}
	}
	
	
	/*  GETTER & SETTER   U T I L I T Y  M E T H O D E N */
	getCoordinates(evt) {
		this.getAdressDependOnZoomCheck(evt.coords.lat, evt.coords.lng);
		
	}
	
	getAdressDependOnZoomCheck(lat, lng) {
		const zoomLevel = this.map.getZoom();
		
		if (zoomLevel > 16) {
			// this.removeMarker();
			this.getAddressByLatLng(lat, lng);
			this.setMarker(lat, lng);
		}
	}
	
	getAddressByLatLng(lat, lng) {
		this.geoCoder = new google.maps.Geocoder();
		
		this.geoCoder.geocode({location: {lat, lng}}, (results, status) => {
			if (status === 'OK') {
				if (results[0]) {
					this.address = results[0].formatted_address;
					this.infoWindow.open(this.map, this.marker);
				} else {
					// window.alert('No results found');
					// console.log('DANYEL Keine Ergebnisse: ' +  status);
				}
			} else {
				// window.alert('Geocoder failed due to: ' + status);
				console.log('DANYEL: ' +  status);
			}
		});
	}
	
	getLatLngByAddress(apo, address: string) {
		return new Promise((resolve, reject) => {
			this.geoCoder = new google.maps.Geocoder();
			
			this.geoCoder.geocode({'address': address}, async (results, status) => {
				if (status === 'OK') {
					if (results[0]) {
						let latitude = results[0].geometry.location.lat();
						let longitude = results[0].geometry.location.lng();
						apo.data.latitude = latitude;
						apo.data.longitude = longitude;
						await this.database.updateCollection(Collection.Apotheken, apo.id, apo.data);
						resolve({latitude, longitude})
						// this.setMarker(latitude, longitude);
						
					} else {
						// window.alert('No results found');
						console.log('DANYEL: ' +  status);
					}
				} else {
					// window.alert('Geocoder failed due to: ' + status);
					console.log('DANYEL: ' +  status);
				}
			});
		});
	}
	
	setMarker(lat, lng) {
		const image = {
			url: 'assets/icon_googlemaps.png',
			scaledSize : new google.maps.Size(34, 44),
		};
		this.marker = new google.maps.Marker({
			position: {lat, lng},
			map: this.map,
			title: 'Hello World!',
			icon: image,
			// animation: google.maps.Animation.BOUNCE,
			draggable: false
		});
		
		// Zoom auf 15, weil sonst der Zoom bei 20 ist, was zu nah ist
		if(this.wurdeGesuchtViaSearchfield) {
			this.setZoom(15);
		}
		this.marker.setMap(this.map);
		this.allMarkers.push(this.marker);
		return this.marker;
	}
	
	setZoom(zoomLevel) {
		setTimeout(()=> {
			this.map.setZoom(zoomLevel);
		}, 100);
	}
	setLatLongGermany() {
		this.latitude = 51.165691;
		this.longitude = 10.451526;
		this.zoom = 5;
	}
	
	setLatLongNeueStr() {
		this.latitude = 53.174204;
		this.longitude = 8.625277;
		this.zoom = 20;
	}
	
	// Get Current Location Coordinates
	setCurrentLocation() {
		if ('geolocation' in navigator) {
			navigator.geolocation.getCurrentPosition((position) => {
				this.latitude = position.coords.latitude;
				this.longitude = position.coords.longitude;
				this.zoom = 8;
				this.getAddressByLatLng(this.latitude, this.longitude);
			});
		}
	}
	
	removeMarker() {
		if (this.marker) {
			for (let marker of this.allMarkers) {
				marker.setMap(null);
			}
		}
	}
}

