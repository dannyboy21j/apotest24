import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {AgmCoreModule} from "@agm/core";
import {KartenansichtPage} from "./kartenansicht.page";
import {KartenansichtPageRoutingModule} from "./kartenansicht-routing.module";
import {ComponentsModule} from "../../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		KartenansichtPageRoutingModule,
		ComponentsModule,
		AgmCoreModule
	],
	exports: [
		KartenansichtPage
	],
	declarations: [KartenansichtPage]
})
export class KartenansichtPageModule {
}
