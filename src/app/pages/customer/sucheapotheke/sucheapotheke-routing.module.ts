import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SucheapothekePage } from './sucheapotheke.page';

const routes: Routes = [
  {
    path: '',
    component: SucheapothekePage
  },
  {
    path: 'listenansicht',
    loadChildren: () => import('./listenansicht/listenansicht.module').then( m => m.ListenansichtPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SucheapothekePageRoutingModule {}
