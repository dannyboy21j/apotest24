import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListenansichtPage } from './listenansicht.page';

const routes: Routes = [
  {
    path: '',
    component: ListenansichtPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListenansichtPageRoutingModule {}
