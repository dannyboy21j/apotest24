import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ListenansichtPageRoutingModule } from './listenansicht-routing.module';
import { ListenansichtPage } from './listenansicht.page';
import {ComponentsModule} from "../../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		ListenansichtPageRoutingModule,
		ComponentsModule
	],
	exports: [
		ListenansichtPage
	],
	declarations: [ListenansichtPage]
})
export class ListenansichtPageModule {}
