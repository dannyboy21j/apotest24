import {Component, OnInit, ViewChild} from '@angular/core';
import {IonSearchbar, NavController} from "@ionic/angular";
import * as _ from 'lodash';
import {Collection, GLOBAL} from "../../../../global-data/global";
import {DatabaseService} from "../../../../services/database.service";
import {FirebaseService} from "../../../../services/firebase.service";
import {ROUTES} from "../../../../global-data/routes";

@Component({
	selector: 'listenansicht',
	templateUrl: './listenansicht.page.html',
	styleUrls: ['./listenansicht.page.scss'],
})
export class ListenansichtPage implements OnInit {
	GLOBAL = GLOBAL;
	@ViewChild('searchbar', {static: true}) searchbar: IonSearchbar;
	alleApothekenInOrt = [];
	ort: string;
	autosuggest = [];
	autosuggestCopy = [];
	autosuggestLimit: number = 2;
	
	constructor(
		private database: DatabaseService,
		private fire: FirebaseService,
		private navCtrl: NavController
	) {
	}
	
	ngOnInit() {
		this.initializeItems();
	}
	
	getItems(event) {
		this.resetItems();
		let eingabe = event.target.value.toLowerCase();
		
		if (eingabe && eingabe.trim() != '') {
			this.autosuggest = _.filter(this.autosuggest, (item) => {
				return _.includes(item.apothekenname.toLowerCase(), eingabe)
					|| _.includes(item.ort.toLowerCase(), eingabe)
					|| _.includes(item.plz, eingabe);
			});
		}
	}
	
	async clickOnOrt(item) {
		this.ort = item.ort;
		let plz = item.plz,
			name = item.apothekenname;
		
		let alleApotheken: any = await this.database.getAlleApotheken()
		if (alleApotheken.length) {
			this.alleApothekenInOrt = _.filter(GLOBAL.Apotheken, {plz, ort: this.ort, name});
			if(this.alleApothekenInOrt.length) {
				// Autosuggest List auf Länge null bringen, damit das Ergebnis nach oben springt
				this.autosuggest = [];
			}
		}
	}
	
	async initializeItems() {
		let autosuggest = [];
		let alleApotheken: any = await this.database.getData(Collection.Apotheken);
		alleApotheken = _.orderBy(alleApotheken, ['ort']);
		
		for(let apotheke of alleApotheken) {
			autosuggest.push({
				plz: apotheke.plz,
				ort: apotheke.ort,
				apothekenname: apotheke.name
			})
		}
		this.autosuggest = autosuggest;
		this.autosuggestCopy = this.autosuggest.slice();
	}
	
	resetItems() {
		this.autosuggest = this.autosuggestCopy;
	}
	
	goToApotheke(apotheke) {
		let queryParams = JSON.stringify(apotheke);
		this.navCtrl.navigateForward([ROUTES.customerApotheke, {queryParams}]);
	}
	
	closeSelectedApotheke() {
		this.alleApothekenInOrt = [];
		this.resetItems();
	}
}
