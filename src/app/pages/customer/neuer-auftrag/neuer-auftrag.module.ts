import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NeuerAuftragPageRoutingModule } from './neuer-auftrag-routing.module';
import { NeuerAuftragPage } from './neuer-auftrag.page';
import {ComponentsModule} from "../../../components/components.module";
import {MatExpansionModule} from "@angular/material/expansion";
import {FabComponent} from "../../../components/fab/fab.component";
import {ScrolltopComponent} from "../../../components/scrolltop/scrolltop.component";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		NeuerAuftragPageRoutingModule,
		ComponentsModule,
		ReactiveFormsModule,
		MatExpansionModule
	],
	declarations: [NeuerAuftragPage, FabComponent, ScrolltopComponent]
})
export class NeuerAuftragPageModule {}
