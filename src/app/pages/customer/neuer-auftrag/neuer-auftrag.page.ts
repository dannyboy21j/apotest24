import {Component, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {FirebaseService} from '../../../services/firebase.service';
import {AuthService} from '../../../modules/authentification/auth.service';
import {ToastService} from '../../../services/toast.service';
import {Collection, GLOBAL} from '../../../global-data/global';
import {ImageService} from '../../../services/image.service';
import {SpinnerService} from '../../../services/spinner.service';
import {IonContent, NavController} from "@ionic/angular";
import {DatabaseService} from "../../../services/database.service";
import * as _ from 'lodash';
import {EventsService} from "../../../services/events.service";
import {MatAccordion} from "@angular/material/expansion";

type TKrankheit = {
	id: string,
	docId: string
	title: string,
	subtitle: string
};

type TParams = {
	docId: string;
	apothekenId: string;
	apothekenName: string;
	apothekerMail: string;
	krankheitsId: string;
	title: string;
	preis: boolean;
	isPreisErhoehung: string;
	showSubmitButton: boolean;
};

@Component({
	selector: 'app-neuer-auftrag',
	templateUrl: './neuer-auftrag.page.html',
	styleUrls: ['./neuer-auftrag.page.scss'],
})
export class NeuerAuftragPage implements OnInit {
	@ViewChild(IonContent, {static: true}) content: IonContent;
	@ViewChild(MatAccordion) accordion: MatAccordion;
	
	scrollHeight = 0;
	
	panelOpenState = false;
	slideOpts = {
		slidesPerView: 3,
		spaceBetween: 15,
		pager: false
	};
	
	GLOBAL = GLOBAL;
	email: string;
	myForm: FormGroup;
	myFormCopy: FormGroup;
	produktempfehlung: FormArray;
	produktnamen: [];
	
	docId: string = '';
	krankheitsId: string;
	apothekenId: string;
	apothekenName: string;
	apothekerMail: string;
	headerTitle: string = '';
	preis: number;
	isPreisErhoehung: boolean;
	showSubmitButton: boolean;
	
	title: string = '';
	subtitle: string = '';
	skalaSize = _.range(5);
	
	jaNein = [{label: 'Ja', value: 'ja'}, {label: 'Nein', value: 'nein'}];
	geschlecht = [{label: 'weiblich', value: 'w'}, {label: 'männlich', value: 'm'}];
	apotheken = [];
	mitarbeiter = [];
	
	formElements: IFormElement[] = [];
	formTitle: any = {};
	
	produktBilder = {
		immobilie: [],
		rechnung: []
	};
	showImageAmountHint = {
		immobilie: true,
		rechnung: true,
	};
	
	formControls = {};
	wurdeFormularAbgeschickt = false;
	
	constructor(
		private auth: AuthService,
		private database: DatabaseService,
		private eventService: EventsService,
		private fb: FormBuilder,
		private fire: FirebaseService,
		private imageService: ImageService,
		private navCtrl: NavController,
		private router: ActivatedRoute,
		private spinner: SpinnerService,
		private toast: ToastService
	) {
		// this.docId = this.router.snapshot.paramMap.get('docId');
		this.router.params.subscribe(async (params: TParams) => {
			this.docId = params.docId;
			this.apothekenId = params.apothekenId;
			this.apothekenName = params.apothekenName;
			this.apothekerMail = params.apothekerMail;
			this.krankheitsId = params.krankheitsId;
			this.headerTitle = params.title;
			this.preis = Number(params.preis);
			this.isPreisErhoehung = (params.isPreisErhoehung === 'true');
			this.showSubmitButton = params.showSubmitButton;
			
			const $sub = this.fire.getCollectionWithQuery(Collection.Apotheker, 'email', this.apothekerMail)
				.subscribe((apotheker: any) => {
					let tmpMitarbeiter = [];
					
					Object.keys(apotheker[0]).forEach(key => {
						if (key.startsWith('mitarbeiter_')) {
							tmpMitarbeiter.push(apotheker[0][key]);
						}
					});
					this.mitarbeiter = _.sortBy(tmpMitarbeiter);
					$sub.unsubscribe();
				});
		});
		this.myForm = this.fb.group({
			produktempfehlung: this.fb.array([])
		});
	}
	
	scrollEvent(event){
		this.scrollHeight = event.detail.currentY;
	}
	
	async ngOnInit() {
		this.email = await this.database.getEmail();
		if (this.email) {
			this.setTitleAndDescription();
			this.retrieveFormularContent();
		} else {
			this.toast.presentToast('Keine Email-Adresse. Bitten nochmal einloggen', 'danger');
		}
	}
	
	outputProduktBilder(produktBilder: Array<{ base64: string }>, subtyp: string) {
		this.produktBilder[subtyp] = produktBilder;
		this.isPhotosAmountCorrect(subtyp);
	}
	
	onSubmit(form: FormGroup) {
		let formData: IForm = form.value;
		if (this.showImageAmountHint.immobilie || (this.showImageAmountHint.rechnung && this.isKauf)) {
		// Wenn nur ein Hinweis, dass zu wenig bzw. keine Bilder gemacht worden sind, dann unterbreche das Submit
			this.toast.presentToast('Bitte Mindestanzahl an Bilder hochladen', 'danger');
			return false;
		}
		
		if (parseInt(formData.input_alter) < 16) {
			this.toast.presentToast('Sie müssen über 16 Jahre alt sein', 'danger');
			return false;
		}
		// TODO: Interface fehleranfällig, da in der DB die Felder geändert und hinzugefügt werden und das Interface entsprechend angepasst werden muss
		formData.krankheitDocId = this.docId;
		formData.krankheitsId = this.krankheitsId;
		formData['email'] = this.email;
		formData['apothekenId'] = this.apothekenId;
		formData['apothekerMail'] = this.apothekerMail;
		formData['preis'] = Number(this.preis);
		formData['isPreisErhoehung'] = this.isPreisErhoehung;
		formData['datum'] = new Date();
		formData['genehmigungsStatus'] = 'inpruefung';
		formData['checkbox_produktempfehlung_name'] = this.getProduktEmpfehlungsNamen(formData);
		if(formData['input_kosten']) {
			formData['input_kosten'] = formData['input_kosten'].replace(',','.');
		}
		if (this.produktBilder['rechnung'].length) {
			formData['photo_rechnung'] = this.produktBilder['rechnung'];
		}
		if (this.produktBilder['immobilie'].length) {
			formData['photo_immobilie'] = this.produktBilder['immobilie'];
		}
		
		if (formData) {
			this.fire.addCollection(Collection.AlleGemachtenAuftraege, formData)
				.then(() => {
					this.eventService.publish('pub:doneNeuerAuftrag');
					this.wurdeFormularAbgeschickt = true;
					setTimeout(() => {
						this.navCtrl.back()
					}, 10000);
					
					// this.toast.presentToast('Daten erfolgreich gesendet', 'success', 2000)
					// 	.then(() => this.navCtrl.back());
				});
		}
	}
	
	getProduktEmpfehlungsNamen(formData) {
		let checkboxBooleans = formData['checkbox_produktempfehlung'];
		let prepareData = [];
		
		if (checkboxBooleans && checkboxBooleans.length) {
			checkboxBooleans.forEach((value, index) => {
				prepareData.push({
					produktname: this.produktnamen[index],
					value: value ? 1 : 0
				});
			});
		}
		return prepareData;
	}
	
	setTitleAndDescription() {
		const $sub = this.fire.getDocumentById(Collection.Krankheiten, this.docId)
			.subscribe((res: TKrankheit) => {
				this.title = res.title;
				this.subtitle = res.subtitle;
				$sub.unsubscribe();
			});
	}
	
	async retrieveFormularContent() {
		const $sub = this.fire.getCollection(Collection.FormElementsGenerell).subscribe(formelementsGenerell => {
			// Spezielle Produktempfehlung. Falls keine vorhanden, dann nimm generelle Produktempfehlung
			this.fire.getCollectionWithThreeQuery(Collection.FormElementsSpeziell, 'apotheker', this.apothekerMail,
				'id', this.krankheitsId,
				'typ', 'checkbox'
			).subscribe((formelementsSpeziell) => {
				if(formelementsSpeziell.length === 0) {
					const $sub = this.fire.getCollectionWithTwoQuery(Collection.FormElementsGenerellProdukte,
						'id', this.krankheitsId,
						'typ', 'checkbox'
					).subscribe((FormElementsGenerellProdukte) => {
						const mergedFormelements = formelementsGenerell.concat(FormElementsGenerellProdukte);
						this.setFormularContent(mergedFormelements);
						this.defineProduktnamen(mergedFormelements);
						$sub.unsubscribe();
					});
				} else {
					const mergedFormelements = formelementsGenerell.concat(formelementsSpeziell);
					this.setFormularContent(mergedFormelements);
					this.defineProduktnamen(mergedFormelements);
				}
				$sub.unsubscribe();
			});
		});
	}
	
	defineProduktnamen(mergedFormelements) {
		if (mergedFormelements.length) {
			let produktCheckboxen = _.filter(mergedFormelements, {formarray: "checkbox_produktempfehlung"});
			if (produktCheckboxen.length) {
				this.produktnamen = produktCheckboxen[0].checkboxen;
			}
		}
	}
	
	setFormularContent(mergedFormelements) {
		const $sub = this.fire.getDocumentById(Collection.GlobaleTexte, Collection.doc.formularueberschriften).subscribe((ueberschriften) => {
			this.formTitle = ueberschriften;
			let formElements = _.orderBy(mergedFormelements, ['order'], ['asc']);
			// formElements.push({
			// 	typ: 'select',
			// 	title: 'select_mitarbeiter'
			// });
			this.prepareApotheken(formElements);
			this.registerFormControlName(formElements);
			$sub.unsubscribe();
		});
	}
	
	registerFormControlName(formElements) {
		let tmpObj = {};
		
		// Build FormControl ------------- NEW FORMCONTROL();
		formElements.forEach((obj: IFormElement) => {
			let isRequired = obj.required ? Validators.required : null;
			
			if(obj.title === 'checkbox_produktempfehlung') {
				isRequired = obj.required ? Validators.requiredTrue : null;
			}
			
			if(obj.title === 'input_alter') {
				tmpObj['input_alter'] = new FormControl('', Validators.compose([isRequired, Validators.min(16), Validators.pattern(/[\d]/)]));
			} else if (obj.title && obj.typ !== 'photo') {
				tmpObj[obj.title] = new FormControl('', Validators.compose([isRequired]));
			}
			
			// Build - FormArray (Checkboxen) -------- NEW FORMARRAY();
			if (obj.typ === 'checkbox') {
				const formControls = obj.checkboxen.map((egal) => new FormControl(false));
				tmpObj[obj.formarray] = new FormArray(formControls);
			}
		});
		
		// Create FormBuilder Group ------- this.fb.group();
		const form = this.fb.group(tmpObj);
		this.myForm = form;
		this.formControls = tmpObj;
		Object.keys(this.formControls).forEach(v => this.formControls[v] = false);
	}
	
	getControls() {
		return (<FormArray>this.myForm.get('formCheckboxControl')).controls;
	}
	
	prepareApotheken(formElements) {
		const $sub = this.fire.getCollectionWithQuery(Collection.Apotheken, 'apotheker', this.apothekerMail)
			.subscribe((Apotheken) => {
				Apotheken.forEach((apotheke: any) => {
					this.apotheken.push({
						label: apotheke.name,
						value: apotheke.formvalue,
					});
				});
				for (let form of formElements) {
					if (form.subtyp === 'apotheke') {
						form.apotheken = this.apotheken;
					}
				}
				
				this.formElements = formElements;
				$sub.unsubscribe();
			});
	}
	
	isPhotosAmountCorrect(subtyp: string) {
		let photoObj = _.filter(this.formElements, {typ: 'photo', subtyp});

		if (photoObj.length) {
			// Bitte mindestens x Bilder und maximal y Bilder
			if (this.produktBilder[subtyp].length === photoObj[0].maxPhotos) {
				this.showImageAmountHint[subtyp] = false;
			} else {
				this.showImageAmountHint[subtyp] = true;
			}
		}
	}
	
	isKauf = undefined;
	gabEsEinenKauf(isKauf: string) {
		let variableFormControls = [
			'input_kosten',
			'radio_zeitung',
			'radio_tuete',
			'radio_treuestempel',
			'radio_giveaway',
			'radio_weiterearzneimittel'
		];
		for (let form of this.formElements) {
			if (isKauf === 'ja') {
				this.isKauf = true;
				if (form.showIsKauf === false) {
					form.showIsKauf = true;
				}
			} else if (isKauf === 'nein') {
				this.isKauf = false;
				if (form.showIsKauf === true) {
					form.showIsKauf = false;
				}
			}
		}
		if (isKauf === 'ja') {
			variableFormControls.forEach((formControl) => {
				this.myForm.addControl(formControl, this.fb.control('', [Validators.required]));
			});
		} else if (isKauf === 'nein') {
			variableFormControls.forEach((formControl) => this.myForm.removeControl(formControl));
		}
	}
	
	getFirstWords(subTitle) {
		let splittedText = subTitle.split(" ")
		return splittedText.slice(0,3).join(" ") + ' ...';
	}
	
	setCorrectBorderColor(data) {
		const controlName = data.controlName;

		if (data.myForm.controls[controlName]['status'] === 'VALID') {
			this.formControls[controlName] = true;
		} else {
			this.formControls[controlName] = false;
		}
	}
}
