import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../../modules/authentification/auth.service";
import {DatabaseService} from "../../../services/database.service";
import {FirebaseService} from "../../../services/firebase.service";
import {NavController} from "@ionic/angular";
import {ToastService} from "../../../services/toast.service";
import {Collection, USER} from "../../../global-data/global";
import * as _ from 'lodash';

type TUserInfo = {
	email: string,
	uid: string,
	vorname?: string,
	nachname?: string,
	str?: string,
	ort?: string,
	plz?: string,
	tel?: string
};

@Component({
	selector: 'app-tab3',
	templateUrl: './tab3.page.html',
	styleUrls: ['./tab3.page.scss'],
})
export class Tab3Page implements OnInit {
	myForm: FormGroup;
	userData: TUserInfo;
	
	formPersonalData = [
		{label: 'Vorname', control: 'vorname', readOnly: false, placeholder: 'Max'},
		{label: 'Nachname', control: 'nachname', readOnly: false, placeholder: 'Mustermann'},
		{label: 'Str. u. Hausnr.', control: 'str', readOnly: false, placeholder: 'Lindenstr. 17'},
		{label: 'PLZ', control: 'plz', readOnly: false, placeholder: '27656'},
		{label: 'Ort', control: 'ort', readOnly: false, placeholder: 'Köln'},
		{label: 'Telefon/Handy (0176-457625)', control: 'tel', readOnly: false, placeholder: '0176-457625'},
		{label: 'Email', control: 'email', readOnly: true, placeholder: 'max@gmail.com'},
		{label: 'Uid', control: 'uid', readOnly: true, placeholder: ''},
	];
	
	constructor(
		private auth: AuthService,
		private database: DatabaseService,
		private formBuilder: FormBuilder,
		private fire: FirebaseService,
		private navCtrl: NavController,
		private toast: ToastService
	) {
	}
	
	ngOnInit() {
		this.retrieveStoredData();
		this.registerFormControlName();
	}
	
	registerFormControlName() {
		let controlObj = {};
		for (let item of this.formPersonalData) {
			controlObj[item.control] = [''];
			// controlObj[item.control] = ['', Validators.required]
		}
		this.myForm = this.formBuilder.group(controlObj);
		
		// this.myForm = this.formBuilder.group({
		// 	vorname:    [this.userData.vorname,     Validators.required],
		// 	nachname:   [this.userData.nachname,    Validators.required],
		// 	str:        [this.userData.str,         Validators.required],
		// 	plz:        [this.userData.plz,         Validators.required],
		// 	ort:        [this.userData.ort,         Validators.required],
		// 	tel:        [this.userData.tel,         Validators.required],
		// 	email:      [this.userData.email,       Validators.required],
		// });
		
		// Validators.pattern('[a-z0-9.@]*')]
		// Validators.minLength(15)]
		// Validators.email
	}
	
	onSubmit(form: FormGroup) {
		let values = form.value;
		_.merge(this.userData, values);
		
		this.fire.updateObj(Collection.Tester, USER.email, this.userData).then(() => {
			this.toast.presentToast('Daten erfolgreich gesendet', 'success')
				.then(() => this.navCtrl.pop());
		});
	}
	
	retrieveStoredData() {
		this.fire.getDocumentById(Collection.Tester, USER.email).subscribe((res: TUserInfo) => {
			this.userData = res;
			this.preFillControls();
		});
	}
	
	preFillControls() {
		Object.keys(this.userData).forEach((key) => {
			this.myForm.get(key).setValue(this.userData[key]);
		});
		// this.myForm.get('input_alter').setValue('33');
	}
}
