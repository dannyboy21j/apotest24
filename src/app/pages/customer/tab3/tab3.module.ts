import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Tab3PageRoutingModule } from './tab3-routing.module';
import { Tab3Page } from './tab3.page';
import {ComponentsModule} from "../../../components/components.module";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		Tab3PageRoutingModule,
		ComponentsModule,
		ReactiveFormsModule,
		MatFormFieldModule,
		MatInputModule
	],
  declarations: [Tab3Page]
})
export class Tab3PageModule {}
