import {Component, OnInit} from '@angular/core';
import {LOCALSTORAGE} from "../../../global-data/localstorage-names";
import {ROUTES} from "../../../global-data/routes";
import {Storage} from "@ionic/storage";
import {NavController} from "@ionic/angular";

@Component({
	selector: 'app-intro',
	templateUrl: './intro.page.html',
	styleUrls: ['./intro.page.scss'],
})
export class IntroPage {
	ROUTES = ROUTES;
	showButton = false;
	
	slideOpts = {
		initialSlide: 0,
		speed: 400,
		pager: true
	};
	
	constructor(
		private navCtrl: NavController,
		private storage: Storage
	) {
	}
	
	checkbox(value) {
		this.storage.set(LOCALSTORAGE.introSlider, value);
	}
	
	goToPage(ROUTE) {
		this.navCtrl.navigateForward([ROUTE, {comeFromIntroSlider: true}]);
	}
	
	endIsReached() {
		this.showButton = true;
	}
}
