import {Component, OnInit} from '@angular/core';
import {DatabaseService} from "../../../services/database.service";
import {Collection} from "../../../global-data/global";
import {DateService} from "../../../services/date.service";
import {NavController} from "@ionic/angular";
import {ROUTES} from "../../../global-data/routes";
import {AlertService} from "../../../services/alert.service";
import {ToastService} from "../../../services/toast.service";
import {FirebaseService} from "../../../services/firebase.service";
import * as _ from 'lodash';

@Component({
	selector: 'app-tab2',
	templateUrl: './tab2.page.html',
	styleUrls: ['./tab2.page.scss'],
})
export class Tab2Page implements OnInit {
	alleErledigtenAuftraege = [];
	alleOffenenAuftraege = [];
	offenesGuthaben: number = 0;

	constructor(
		private alertService: AlertService,
		private database: DatabaseService,
		private date: DateService,
		private fire: FirebaseService,
		private navCtrl: NavController,
		private toast: ToastService,
	) {
	}

	ngOnInit() {
		this.wievieleAuftraegeWurdenInAllenApothekeGemacht();
		this.ermittleAuszahlungstabelle();
	}

	async wievieleAuftraegeWurdenInAllenApothekeGemacht() {
		let auftraegeDesTesters: any = await this.database.getAlleGemachtenAuftraegeDesTestersWithDocId();
		let alleApotheken: any = await this.database.getAlleApotheken();
		let auftragsschablonen: any = await this.database.getData(Collection.Krankheiten);

		let prepareData = [];
		let offenesGuthaben = 0;

		for(let auftragWithDocId of auftraegeDesTesters) {
			let auftrag = auftragWithDocId.data;
			let obj = {};
			obj = _.clone(auftrag);
			obj['datum'] = this.date.transform(auftrag.datum);
			obj['auftragsDocId'] = auftragWithDocId.id;

			if(auftrag['genehmigungsStatus'] === 'offen') {
				offenesGuthaben = offenesGuthaben + Number(auftrag.preis);
				
				if(auftrag['input_kosten']) {
					offenesGuthaben = offenesGuthaben + Number(auftrag.input_kosten);
				}
			}

			let apotheke = _.filter(alleApotheken, {id: auftrag.apothekenId, apotheker: auftrag.apothekerMail });
			if(apotheke.length) { obj['apothekenname'] = apotheke[0].name;}
			
			obj['krankheitsname'] = _.filter(auftragsschablonen, {id: auftrag.krankheitsId})[0].title;

			prepareData.push(obj);
		}

		this.offenesGuthaben = offenesGuthaben;
		this.alleErledigtenAuftraege = this.getSortedListByGenehmingungsstatus(prepareData);
		this.alleOffenenAuftraege = _.filter(this.alleErledigtenAuftraege, {genehmigungsStatus:'offen'});
	}

	tabelle = [];
	async ermittleAuszahlungstabelle() {
		this.fire.getCollectionWithQuery(Collection.Auszahlungen, 'email', await this.database.getEmail())
			.subscribe((res) => {
				this.tabelle = _.orderBy(res, ['datum_erledigt'], ['desc']);
			});
	}

	submitAntrag() {
		// TODO: Nachfrage mit Confirmation, ob der Tester wirklich einen Antrag schicken möchte
		// Schicke an VIP eine Nachricht mit allen Tests, die nicht den Status
		// isGenehmigt='erledigt' u. nicht isGenehmigt='inPrüfung' haben

		let offeneAuftraege = _.filter(this.alleErledigtenAuftraege, {genehmigungsStatus: 'offen'});

		if(offeneAuftraege.length) {
			this.alertService.presentAlertConfirm('Auszahlung beantragen', 'Möchten Sie eine Auzahlung beantragen?')
				.then((res: { data: object, role: string }) => {
					if (res && res.role === 'ja') {
						let auszahlungen = [];

						for(let auftrag of offeneAuftraege) {
							auftrag.genehmigungsStatus = 'beantragt';
							
							let obj = {
								auftragsDocId: auftrag.auftragsDocId,
								krankheitsId: auftrag.krankheitsId,
								krankheitsName: auftrag.krankheitsname,
								apothekerMail: auftrag.apothekerMail,
								apothekenName: auftrag.apothekenname,
								gesamtkosten: {
									preis: auftrag.preis,
									kosten: auftrag['input_kosten'] ? Number(auftrag['input_kosten']) : 0
								}
							};
							
							auszahlungen.push(obj);
						}

						let auszahlungsDocId = this.fire.getBeforeDocumentId(Collection.Auszahlungen);
						let obj = {
							'auszahlungsDocId': auszahlungsDocId,
							'datum_antrag': new Date(),
							'auszahlungen': auszahlungen,
							'email': offeneAuftraege[0].email,
							'genehmigungsStatus': 'beantragt'
						};

						this.fire.setCollection(Collection.Auszahlungen, auszahlungsDocId, obj)
							.then(() => {
								for(let auftrag of auszahlungen) {
									this.fire.updateCollectionProperty(Collection.AlleGemachtenAuftraege, auftrag.auftragsDocId,
										'genehmigungsStatus','beantragt');
								}
								this.offenesGuthaben = 0;
								this.alleOffenenAuftraege = [];
								this.toast.presentToast('Auszahlung beantragt', 'success');
							})
							.catch(() => {
								this.toast.presentToast('Auszahlung konnte nicht beantragt werden', 'error');
							});
					}
				});
		}
	}

	getGuthabenTextInTabelle(item) {
		let text = '';
		if (item.genehmigungsStatus === 'beantragt') {
			text = 'Offenes Guthaben: ';
		} else if (item.genehmigungsStatus === 'erledigt') {
			text = 'Auszahldatum: ';
		}

		return text + this.date.transform(item.datum_antrag)
	}
	
	getSortedListByGenehmingungsstatus(unsortierteListe) {
		function getItems(status) {
			let tmp = [];
			for(let item of unsortierteListe) {
				if(item.genehmigungsStatus === status) {
					tmp.push(item);
				}
			}
			return tmp;
		}
		
		let abgelehnt = getItems('abgelehnt');
		let offen = getItems('offen');
		let beantragt = getItems('beantragt');
		let inpruefung = getItems('inpruefung');
		let erledigt = getItems('erledigt');
		
		return [].concat(abgelehnt, offen, beantragt, inpruefung, erledigt);
	}
	
	getPreisUndKosten(form) {
		let gesamtkosten = Number(form.preis);
		
		if(form['input_kosten']) {
			gesamtkosten = gesamtkosten + Number(form.input_kosten);
		}
		return gesamtkosten.toFixed(2);
	}
	
	getPreisUndKosten2(item){
		let auszahlungen = item.auszahlungen;
		let gesamtkosten = 0;
		
		for(let auszahlung of auszahlungen) {
			gesamtkosten = gesamtkosten + auszahlung.gesamtkosten.preis + auszahlung.gesamtkosten.kosten;
		}
		return gesamtkosten
	}
	
	goToTest(param){
		let title = param.krankheitsname,
			auftragsDocId = param.auftragsDocId,
			apothekenId = param.apothekenId,
			apothekerMail = param.apothekerMail,
			testerMail = param.email,
			binIchVIP = false,
			krankheitsId = param.krankheitsId,
			korrekturbeschreibung = param.korrekturbeschreibung,
			genehmigungsStatus = param.genehmigungsStatus;
		
		this.navCtrl.navigateForward([ROUTES.sharedErledigterAuftrag, {
			auftragsDocId,
			apothekerMail,
			testerMail,
			apothekenId,
			krankheitsId,
			title,
			binIchVIP,
			korrekturbeschreibung,
			genehmigungsStatus,
			showSubmitButton: genehmigungsStatus === 'abgelehnt'
		}]);
	}
}
