import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {Collection} from "../../../global-data/global";
import {AuthService} from "../../../modules/authentification/auth.service";
import {FirebaseService} from "../../../services/firebase.service";
import {NavController} from "@ionic/angular";
import * as _ from 'lodash';
import {ROUTES} from "../../../global-data/routes";
import {DatabaseService} from "../../../services/database.service";

type TParams = {
	apotheker: string,
	id: string,
	name: string,
	plz: string,
	ort: string,
	str: string,
};

@Component({
	selector: 'app-apotheke',
	templateUrl: './apotheke.page.html',
	styleUrls: ['./apotheke.page.scss'],
})
export class ApothekePage implements OnInit {
	apothekenName: string;
	apothekermail: string;
	kontingent: number;
	apothekenId: string;
	email: string;
	queryParams: any;
	isKontingentErschoepft = false;
	allForms = [];
	apothekerHatTestsAngelegtAm: any;
	
	constructor(
		private auth: AuthService,
		private database: DatabaseService,
		private fire: FirebaseService,
		private navCtrl: NavController,
	    private router: ActivatedRoute
    ) {
	}
	
	ngOnInit() {
		this.router.params.subscribe(async (params: Params) => {
			this.queryParams = JSON.parse(params['queryParams']);
			this.apothekermail = this.queryParams.apotheker;
			this.apothekenId = this.queryParams.id;
			this.email = await this.database.getEmail();
			this.checkKontingentVonApotheker();
		});
	}
	
	async checkKontingentVonApotheker() {
		let alleAuftraegeInApotheke: any = await this.database.getAlleGemachtenAuftraegeZuEinemApothekerInEinerBestimmtenApotheke(this.apothekermail, this.apothekenId);
		if(alleAuftraegeInApotheke.length >= this.queryParams.kontingentTests) {
			this.isKontingentErschoepft = true;
		}
		this.ermittleAnzahlDurchgefuehrterTests();
	}
	
	ermittleAnzahlDurchgefuehrterTests() {
		this.fire.getCollectionWithThreeQuery(Collection.AlleGemachtenAuftraege,
			'email', this.email,
			'apothekenId', this.apothekenId,
			'apothekerMail', this.apothekermail,
		).subscribe(async(AlleGemachtenAuftraege: IAlleGemachtenAuftraege[]) => {
			const sub$ = this.fire.getCollectionWithQuery(Collection.Apotheker, 'email', this.apothekermail).subscribe(async(_apotheker) => {
				const apotheker = _apotheker[0];
				let krankheiten: any = await this.database.getData(Collection.Krankheiten);
				if(this.isKontingentErschoepft) {
					_.map(krankheiten, (obj) => {
						obj.kontingentErschoepft = true;
						return obj;
					});
				}
				
				for(let krankheit of krankheiten) {
					krankheit.anzahlDurchgefuehrterTests = _.filter(AlleGemachtenAuftraege, {krankheitsId: krankheit.id} ).length;
					krankheit.preis = apotheker['preis_' + krankheit.id];
				}
				// let _krankheiten =  await this.checkPreisErhoehungFallsEinTestLangeNichtBenutztWurde(apotheker, krankheiten);
				this.allForms = _.orderBy(krankheiten, ['anzahlDurchgefuehrterTests'], ['desc']);
				sub$.unsubscribe();
			});
		});
	}
	
	// Jetzt muss für jeden Test gecheckt werden, ob ein Test durchgeführt worden ist und falls nicht, liegen 3 Monate seit dem zurück.
	// Wenn ja, dann mach eine z.b 20% Preiserhöhung
	// Erhöhung um 20% in die Datenbank reinschreiben
	async checkPreisErhoehungFallsEinTestLangeNichtBenutztWurde(apotheker, krankheiten) {
		let alleAuftraege: any = await this.database.getAlleGemachtenAuftraegeZuEinemApothekerInEinerBestimmtenApotheke(this.apothekermail, this.apothekenId);

		krankheiten.forEach((obj, index) => {
			if(apotheker['preis_' + obj.id]) {
				let auftrag = _.filter(alleAuftraege, { krankheitsId: obj.id });
				let zuletztGemachterTest = _.orderBy(auftrag, ['datum'], ['desc'])[0];

				if(alleAuftraege.length === 0) {
					krankheiten[index].preis = apotheker['preis_' + obj.id]
					krankheiten[index].isPreisErhoehung = false;
				}
				// Wenn noch kein Test gefunden bzw. durchgeführt wurde wurde also zuletztGemachterTest = undefiend ist,
				// dann muss gecheckt werden, wann (Datum) der Apotheker die Tests eingepflegt hat
				else if(!zuletztGemachterTest && this.isOlderThenThreeMonth(this.apothekerHatTestsAngelegtAm)
					|| (!obj.bereitsErledigterAuftrag && !obj.kontingentErschoepft && zuletztGemachterTest && this.isOlderThenThreeMonth(zuletztGemachterTest['datum']))) {
					let istPreis = apotheker['preis_' + obj.id];
					krankheiten[index].preis = istPreis + (istPreis * apotheker.preisErhoehung) / 100;
					krankheiten[index].isPreisErhoehung = true;
					krankheiten[index].preisErhoehung = apotheker.preisErhoehung;
				} else if (obj.bereitsErledigterAuftrag) {
					let bereitsGesetzterPreis = _.filter(alleAuftraege, {krankheitsId: obj.id});
					krankheiten[index].preis = bereitsGesetzterPreis[0].preis;
				} else {
					krankheiten[index].preis = apotheker['preis_' + obj.id]
					krankheiten[index].isPreisErhoehung = false;
				}
			} else {
				krankheiten[index].preis = 0;
				krankheiten[index].isPreisErhoehung = false;
			}
		})

		return krankheiten;
	}
	
	isOlderThenThreeMonth(zuletztGemachterTest): boolean {
		// 3 Monate in ms: 7884000000 -http://extraconversion.com/de/zeit/monate/monate-zu-millisekunden.html
		// let einTagInMs = 86400000;
		let _zuletztGemachterTest = zuletztGemachterTest.toDate().getTime(); // in ms
		let heutigesDatum = new Date().getTime(); // in ms
		let dreiMonateInMs = 7884000000;
		
		return (heutigesDatum - _zuletztGemachterTest) > dreiMonateInMs;
	}
	
	getAnzahlDurchgefuehfterTests(item) {
		const anzahl = item.anzahlDurchgefuehrterTests;
		return anzahl > 0 ? 'Durchgeführte Tests: ' + anzahl : '';
	}
	
	openAuftrag(form) {
		if(!this.isKontingentErschoepft || !form.kontingentErschoepft) {
			let docId = form.docId,
				apothekerMail = this.apothekermail,
				testerMail = this.email,
				krankheitsId = form.id,
				apothekenId = this.apothekenId,
				apothekenName = this.queryParams.name,
				title = form.title,
				preis = form.preis,
				isPreisErhoehung = form.isPreisErhoehung,
				showSubmitButton = true;
			
			this.navCtrl.navigateForward([ROUTES.customerNeuerAuftrag, {
				apothekerMail,
				testerMail,
				apothekenId,
				docId,
				krankheitsId,
				apothekenName,
				title,
				preis,
				isPreisErhoehung,
				showSubmitButton
			}]);
		}
	}
}
