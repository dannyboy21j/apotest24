import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ApothekePageRoutingModule } from './apotheke-routing.module';
import { ApothekePage } from './apotheke.page';
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		ApothekePageRoutingModule,
		ComponentsModule
	],
  declarations: [ApothekePage]
})
export class ApothekePageModule {}
