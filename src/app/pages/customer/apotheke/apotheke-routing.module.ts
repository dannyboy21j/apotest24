import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ApothekePage} from './apotheke.page';
const routes: Routes = [
	{
		path: '',
		component: ApothekePage
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ApothekePageRoutingModule {
}
