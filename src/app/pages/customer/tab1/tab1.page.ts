import {Component, OnInit} from '@angular/core';
import {DatabaseService} from "../../../services/database.service";
import {NavController} from "@ionic/angular";
import {Collection, GLOBAL} from "../../../global-data/global";
import {ROUTES} from "../../../global-data/routes";
import * as _ from 'lodash';
import {EventsService} from "../../../services/events.service";

@Component({
	selector: 'app-tab1',
	templateUrl: './tab1.page.html',
	styleUrls: ['./tab1.page.scss'],
})
export class Tab1Page implements OnInit {
	GLOBAL = GLOBAL;
	ROUTES = ROUTES;
	email: string;
	subTitle: string;
	groupedByApotheken = [];
	welcomeName: string;
	
	constructor(
		private database: DatabaseService,
		private eventService: EventsService,
		private navCtrl: NavController,
	) {
		this.subscribe();
	}
	
	ngOnInit() {
		this.database.initGlobaleTexte();
		this.getWelcomeName();
		this.wievieleAuftraegeWurdenInJederApothekeGemacht();
	}
	
	subscribe() {
		this.eventService.subscribe('pub:doneNeuerAuftrag', () => {
			this.wievieleAuftraegeWurdenInJederApothekeGemacht();
		});
	}
	
	async getWelcomeName() {
		let email: any = await this.database.getEmail();
		let testUser: any = await this.database.getCurrentTester(email);
		if(testUser.length) {
			let name;
			name = (testUser[0].vorname) ? testUser[0].vorname : '';
			name = (testUser[0].nachname) ? name + ' ' + testUser[0].nachname : '';
			this.welcomeName = (name === '') ? testUser[0].email : name;
		}
	}
	
	async wievieleAuftraegeWurdenInJederApothekeGemacht() {
		let auftraegeDesTesters: any = await this.database.getAlleGemachtenAuftraegeDesTesters();
		let alleApotheken: any = await this.database.getData(Collection.Apotheken);
		
		let apothekenInDenenEinAuftragGemachtWurde = [];
		
		for(let gemachterAuftrag of auftraegeDesTesters) {
			for(let apotheke of alleApotheken) {
				if(gemachterAuftrag.apothekerMail == apotheke.apotheker && gemachterAuftrag.apothekenId == apotheke.id) {
					apothekenInDenenEinAuftragGemachtWurde.push(apotheke);
					break;
				}
			}
		}
		
		let dataWithAkiveAuftraege = await this.gesamtAuftraegeJeApotheker(apothekenInDenenEinAuftragGemachtWurde);
		let grouped = _.groupBy(dataWithAkiveAuftraege, (obj) => [[obj.apotheker], [obj.id]]);
		this.groupedByApotheken = _.orderBy(grouped, (obj) => {return obj.length}, ['desc'])
	};
	
	// Hier muss die Gesamtanzahl jeder Apotheke ermittelt werden
	async gesamtAuftraegeJeApotheker(apothekenInDenenEinAuftragGemachtWurde) {
		let krankheiten: any = await this.database.getData(Collection.Krankheiten);
		let aktiveAuftraege = [];
		
		for(let apotheke of apothekenInDenenEinAuftragGemachtWurde) {
			apotheke['aktiveAuftraege'] = krankheiten.length;
			aktiveAuftraege.push(apotheke);
		}
		
		return aktiveAuftraege;
	}
	
	goToPage(ROUTE) {
		this.navCtrl.navigateForward(ROUTE);
	}
	
	goToApotheke(apotheke) {
		let queryParams = JSON.stringify(apotheke);
		this.navCtrl.navigateForward([ROUTES.customerApotheke, {queryParams}]);
	}
}
