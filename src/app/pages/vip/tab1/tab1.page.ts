import {Component, OnInit} from '@angular/core';
import {Collection} from "../../../global-data/global";
import {DatabaseService} from "../../../services/database.service";
import * as _ from 'lodash';
import {NavController} from "@ionic/angular";
import {ROUTES} from "../../../global-data/routes";
import {FirebaseService} from "../../../services/firebase.service";

@Component({
	selector: 'app-alleapotheker',
	templateUrl: './tab1.page.html',
	styleUrls: ['./tab1.page.scss'],
})
export class Tab1Page implements OnInit {
	alleApotheker = [];

	constructor(
		private database: DatabaseService,
		private fire: FirebaseService,
		private navCtrl: NavController
	) {
	}

	async ngOnInit() {
		let alleApotheker: any = await this.database.getData(Collection.Apotheker);
		this.ermittleAlleOffenenAuftrage(alleApotheker);
	}

	async ermittleAlleOffenenAuftrage(alleApotheker) {
		let alleAuftraege = await this.database.getData(Collection.AlleGemachtenAuftraege);

		for(let item of alleApotheker) {
			let anzahl = _.countBy(alleAuftraege,{genehmigungsStatus: 'inpruefung', apothekerMail: item.email});

			if(anzahl.true) {
				item.anzahlOffenerTest = anzahl['true'];
			} else {
				item.anzahlOffenerTest = 0;
			}
		}
		this.alleApotheker = _.orderBy(alleApotheker, ['anzahlOffenerTest'], ['desc']);
	}

	getName(apotheker) {
		let name = '',
			nachname = apotheker.nachname,
			vorname = apotheker.vorname;

		if(vorname || nachname) {
			if(nachname) { name = nachname;}
			if(vorname) { name = nachname + ', ' + vorname; }
		} else {
			name = apotheker.email;
		}
		return name
	}

	goToKrankheiten(apotheker) {
		let queryParams = JSON.stringify(apotheker);
		this.navCtrl.navigateForward([ROUTES.vipAuftraege, {queryParams}]);
	}
}
