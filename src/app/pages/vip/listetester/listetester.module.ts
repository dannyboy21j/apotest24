import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListetesterPageRoutingModule } from './listetester-routing.module';

import { ListetesterPage } from './listetester.page';
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		ListetesterPageRoutingModule,
		ComponentsModule
	],
  declarations: [ListetesterPage]
})
export class ListetesterPageModule {}
