import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListetesterPage } from './listetester.page';

const routes: Routes = [
  {
    path: '',
    component: ListetesterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListetesterPageRoutingModule {}
