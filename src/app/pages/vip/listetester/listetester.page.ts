import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {DatabaseService} from "../../../services/database.service";
import * as _ from 'lodash';
import {ROUTES} from "../../../global-data/routes";
import {NavController} from "@ionic/angular";
import {UtilService} from "../../../services/util.service";
import {DateService} from "../../../services/date.service";

@Component({
	selector: 'app-listetester',
	templateUrl: './listetester.page.html',
	styleUrls: ['./listetester.page.scss'],
})
export class ListetesterPage implements OnInit {
	apothekermail: string;
	krankheitsId: string;
	title: string;
	auftraege: any;
	alleApotheken: any;
	anfangsbuchstaben = [];
	showKeinTreffer: boolean;
	
	constructor(
		public date: DateService,
		private database: DatabaseService,
		private navCtrl: NavController,
		private router: ActivatedRoute,
	) {
	}
	
	ngOnInit() {
		this.router.params.subscribe((params: Params) => {
			this.apothekermail = params.apothekermail;
			this.krankheitsId = params.krankheitsId;
			this.title = params.title;
			this.initListing();
		});
	}
	
	async initListing() {
		let alleAuftraege: any = await this.database.getAuftraegeEinerBestimmtenKrankheitEinesApothekersWithDocId(this.apothekermail, this.krankheitsId);
		
		if(alleAuftraege.length) {
			this.alleApotheken = await this.database.getAlleApotheken();
			this.extrahiereFirstLetterOfTester(alleAuftraege);
		} else {
			this.showKeinTreffer = true;
		}
	}
	
	extrahiereFirstLetterOfTester(alleAuftraege) {
		let anfangsbuchstaben = [];
		
		for(let obj of alleAuftraege) {
			let item = obj.data;
			anfangsbuchstaben.push(item.email.charAt(0));
			let apotheke = _.filter(this.alleApotheken, {id: item.apothekenId, apotheker: item.apothekerMail});
			item.apothekenname = (apotheke.length) ? apotheke[0].name : '';
		}
		
		this.auftraege = _.orderBy(alleAuftraege, (obj) =>
			[obj.data.genehmigungsStatus === 'inpruefung', obj.data.genehmigungsStatus === 'abgelehnt'], ['desc']);
		// this.groupedByApotheken = _.orderBy(grouped, (obj) => {return obj.length}, ['desc'])
		this.anfangsbuchstaben = _.uniq(anfangsbuchstaben).sort();
	}
	
	goToTest(_tester) {
		let tester = _tester.data,
			auftragsDocId = _tester.id,
			apothekerMail = tester.apothekerMail,
			apothekenId = tester.apothekenId,
			krankheitDocId = tester.krankheitDocId,
			krankheitsId = tester.krankheitsId,
			genehmigungsStatus = tester.genehmigungsStatus,
			korrekturbeschreibung = tester.korrekturbeschreibung,
			showSubmitButton = false,
			testerMail = tester.email,
			binIchVIP = true,
			title = tester.email + ' (' + this.title + ')';
		
		this.navCtrl.navigateForward([ROUTES.sharedErledigterAuftrag, {
			auftragsDocId,
			apothekerMail,
			apothekenId,
			krankheitDocId,
			krankheitsId,
			genehmigungsStatus,
			korrekturbeschreibung,
			showSubmitButton,
			testerMail,
			binIchVIP,
			title
		}]);
	}
}
