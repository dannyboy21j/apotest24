import {Component, OnInit} from '@angular/core';
import {Collection} from "../../../global-data/global";
import {AuthService} from "../../../modules/authentification/auth.service";
import {DatabaseService} from "../../../services/database.service";
import {FirebaseService} from "../../../services/firebase.service";
import {NavController} from "@ionic/angular";
import {ROUTES} from "../../../global-data/routes";
import * as _ from 'lodash';
import {ActivatedRoute, Params} from "@angular/router";

@Component({
	selector: 'app-auftraege',
	templateUrl: './auftraege.page.html',
	styleUrls: ['./auftraege.page.scss'],
})
export class AuftraegePage implements OnInit {
	aktiveTests = [];
	inaktiveTests = [];
	kontingentTests: number;
	title: any;
	anzahlKrankheiten = {};
	
	constructor(
		private auth: AuthService,
		private router: ActivatedRoute,
		private database: DatabaseService,
		private fire: FirebaseService,
		private navCtrl: NavController,
	) {
	}
	
	async ngOnInit() {
		this.router.params.subscribe((params: Params) => {
			let apotheker = JSON.parse(params['queryParams']);
			this.ermittelAktiveTests(apotheker);
			this.title = apotheker.vorname + ' ' + apotheker.nachname ;
		});
	}
	
	async ermittelAktiveTests(apotheker) {
		let aktiveTests = [];
		let inaktiveTests = [];
		this.kontingentTests = apotheker.kontingentTests;
		const Krankheiten: any = await this.database.getData(Collection.Krankheiten);
		const alleGemachtenAuftraegeZuEinemApotheker: any = await this.database.getAlleGemachtenAuftraegeZuEinemApotheker(apotheker.email);
		
		for (let item of Krankheiten) {
			let anzahl = _.countBy(alleGemachtenAuftraegeZuEinemApotheker, {genehmigungsStatus: 'inpruefung', krankheitsId: item.id});
			
			if(anzahl.true) {
				item.anzahlOffenerTest = anzahl['true'];
			}
			let result = _.filter(alleGemachtenAuftraegeZuEinemApotheker, {krankheitsId: item.id});
			item.anzahlAbsolviert = result.length;
			item.apothekermail = apotheker.email;
			aktiveTests.push(item);
			
			// if (item.anzahlAbsolviert >= apotheker.kontingentTests) {
			// 	inaktiveTests.push(item);
			// } else {
			// 	aktiveTests.push(item);
			// }
		}
		
		// Wieviele offene Tests gibt es für jeden Krankheit?
		this.aktiveTests = _.orderBy(aktiveTests, ['anzahlOffenerTest']);
		this.inaktiveTests = _.orderBy(inaktiveTests, ['anzahlOffenerTest']);
	}
	
	goToAuftraegePage(test) {
		let apothekermail = test.apothekermail;
		let krankheitsId = test.id;
		let title = test.title;
		this.navCtrl.navigateForward([ROUTES.vipListing, {apothekermail, krankheitsId, title}]);
	}
}
