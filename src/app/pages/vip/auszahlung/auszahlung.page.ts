import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {FirebaseService} from "../../../services/firebase.service";
import {Collection} from "../../../global-data/global";
import {ToastService} from "../../../services/toast.service";
import {DateService} from "../../../services/date.service";
import * as _ from 'lodash';
import {NavController} from "@ionic/angular";
import {ROUTES} from "../../../global-data/routes";

@Component({
	selector: 'app-auszahlung',
	templateUrl: './auszahlung.page.html',
	styleUrls: ['./auszahlung.page.scss'],
})
export class AuszahlungPage implements OnInit {
	email: string;
	auszahlungsDocId: string;
	genehmigungsStatus: string;
	guthabenhoehe: number;
	tabelle = [];
	
	constructor(
		public date: DateService,
		private fire: FirebaseService,
		private navCtrl: NavController,
		private router: ActivatedRoute,
		private toast: ToastService,
	) {
		this.router.params.subscribe((params) => {
			this.email = params.email;
			this.auszahlungsDocId = params.auszahlungsDocId;
			this.genehmigungsStatus = params.genehmigungsStatus;
		});
	}
	
	ngOnInit() {
		this.ermittleAuszahlungstabelle();
		this.ermittleBeantragteGuthabenHoehe();
	}
	
	ermittleAuszahlungstabelle() {
		this.fire.getCollectionWithTwoQuery(
			Collection.Auszahlungen,
			'email', this.email,
			'genehmigungsStatus', 'erledigt')
		.subscribe((res) => {
			this.tabelle = _.orderBy(res, ['datum_erledigt'],['desc']);
		});
	}
	
	ermittleBeantragteGuthabenHoehe() {
		this.fire.getCollectionWithTwoQuery(
			Collection.Auszahlungen,
			'email', this.email,
			'genehmigungsStatus', 'beantragt')
			.subscribe((res: any) => {
				if(res.length) {
					let guthaben = 0;
	
					for(let item of res[0].auszahlungen) {
						guthaben = guthaben + Number(item.gesamtkosten.preis) + Number(item.gesamtkosten.kosten);
					}
					this.guthabenhoehe = guthaben;
				}
			});
	}
	
	auszahlungBestaetigen() {
		this.fire.getSpecificDocument(Collection.Auszahlungen, this.auszahlungsDocId)
		.then((res: any) => {
			let auszahlungsDaten = res.data();
			auszahlungsDaten['genehmigungsStatus'] = 'erledigt';
			auszahlungsDaten['datum_erledigt'] = new Date();
			
			this.fire.updateObj(Collection.Auszahlungen, this.auszahlungsDocId, auszahlungsDaten)
			.then(() => {
				for(let auszahlung of auszahlungsDaten.auszahlungen) {
					this.fire.updateCollectionProperty(Collection.AlleGemachtenAuftraege, auszahlung.auftragsDocId, 'genehmigungsStatus', 'erledigt');
				}

				this.genehmigungsStatus = 'erledigt';
				this.toast.presentToast('Auszahlung bestätigt', 'success');
			});
		})
	}
	
	getPreisUndKosten(data) {
		let guthaben = 0;
		
		for(let item of data.auszahlungen) {
			guthaben = guthaben + Number(item.gesamtkosten.preis) + Number(item.gesamtkosten.kosten);
		}
		return guthaben;
	}
	
	goToRechnungsPage(item: any) {
		const queryParams = JSON.stringify(item);
		this.navCtrl.navigateForward([ROUTES.vipRechnung, {queryParams}])
	}
}
