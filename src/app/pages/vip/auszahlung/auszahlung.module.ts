import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuszahlungPageRoutingModule } from './auszahlung-routing.module';

import { AuszahlungPage } from './auszahlung.page';
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		AuszahlungPageRoutingModule,
		ComponentsModule
	],
  declarations: [AuszahlungPage]
})
export class AuszahlungPageModule {}
