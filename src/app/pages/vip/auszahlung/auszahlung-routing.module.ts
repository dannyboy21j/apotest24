import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuszahlungPage } from './auszahlung.page';

const routes: Routes = [
  {
    path: '',
    component: AuszahlungPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuszahlungPageRoutingModule {}
