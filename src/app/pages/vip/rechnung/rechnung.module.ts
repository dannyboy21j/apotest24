import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RechnungPageRoutingModule } from './rechnung-routing.module';

import { RechnungPage } from './rechnung.page';
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RechnungPageRoutingModule,
		ComponentsModule
	],
  declarations: [RechnungPage]
})
export class RechnungPageModule {}
