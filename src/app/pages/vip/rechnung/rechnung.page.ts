import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import * as html2pdf from 'html2pdf.js';
import {DateService} from "../../../services/date.service";
import * as _ from 'lodash';
import {FirebaseService} from "../../../services/firebase.service";
import {DatabaseService} from "../../../services/database.service";
import {Collection, GLOBAL} from "../../../global-data/global";

@Component({
	selector: 'app-rechnung',
	templateUrl: './rechnung.page.html',
	styleUrls: ['./rechnung.page.scss'],
})
export class RechnungPage implements OnInit {
	rechnungsdaten = {};
	auszahlungsDocId: string;
	gesamtSumme = 0;
	testerData: any;
	GLOBAL = GLOBAL;
	
	constructor(
		public date: DateService,
		private database: DatabaseService,
		private fire: FirebaseService,
		private router: ActivatedRoute,
	) {
	}
	
	ngOnInit() {
		this.router.params.subscribe(async (params: Params) => {
			let rechnungsdaten = JSON.parse(params['queryParams']);
			this.auszahlungsDocId = rechnungsdaten.auszahlungsDocId;
			this.ermittleKundennr(rechnungsdaten);
		});
	}
	
	async ermittleKundennr(rechnungsdaten) {
		this.testerData = await this.database.getCurrentTester(rechnungsdaten.email);
		rechnungsdaten['knr'] = this.testerData[0].knr;
		this.ermittleAuszahlungsHaufigkeit(rechnungsdaten);
		
	}
	
	async ermittleAuszahlungsHaufigkeit(rechnungsdaten) {
		let auszahlungen: any = await this.database.getAuszahlungenEinesBestimmtenTesters(rechnungsdaten.email);
		let anzahlAuszahlungen = _.filter(auszahlungen, {genehmigungsStatus: 'erledigt'});
		
		if(anzahlAuszahlungen.length) {
			rechnungsdaten['auszahlungsHaeufigkeit'] = anzahlAuszahlungen.length;
			this.rechnungsdaten = rechnungsdaten;
			this.ermittelGesamtSumme(rechnungsdaten.auszahlungen);
		}
	}
	
	ermittelGesamtSumme(auszahlungen): void {
		let gesamtsumme = 0;
		for(let item of auszahlungen) {
			gesamtsumme = gesamtsumme + item.gesamtkosten.preis + item.gesamtkosten.kosten;
		}
		this.gesamtSumme = gesamtsumme;
	}
	
	print() {
		var element = document.getElementById('rechnung');
		var opt = {
			margin:       1,
			filename:     'Apo24_Rechnung_' + this.auszahlungsDocId + '.pdf',
			image:        { type: 'jpeg', quality: 0.98 },
			html2canvas:  { scale: 1 },
			jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
		};

		html2pdf().from(element).set(opt).save();
	}
	
	isEmpty(rechnungsdaten) {
		return _.isEmpty(rechnungsdaten);
	}
	
	getDate(date) {
		return this.date.transform2(this.date.transformTimestampToDate(date));
	}
	
	getFirmenAdresse() {
		return GLOBAL.Rechnung.firmenadresse;
	}
	
	getKontoDaten() {
		return GLOBAL.Rechnung.kontodaten;
	}
	
	getTesterData() {
		let tester = this.testerData[0];
		let vorname =   ( tester.vorname === '') ? '?' :  tester.vorname;
		let nachname =  ( tester.nachname === '') ? '?' :  tester.nachname;
		let str =       ( tester.str === '') ? '?' :  tester.str;
		let plz =       ( tester.plz === '') ? '?' :  tester.plz;
		let ort =       ( tester.ort === '') ? '?' : tester.ort;
		
		return `${vorname} ${nachname}<br>${str}<br>${plz} ${ort}`;
	}
}
