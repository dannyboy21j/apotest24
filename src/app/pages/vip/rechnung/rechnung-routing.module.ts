import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RechnungPage } from './rechnung.page';

const routes: Routes = [
  {
    path: '',
    component: RechnungPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RechnungPageRoutingModule {}
