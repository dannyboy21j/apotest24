import {Component, OnInit} from '@angular/core';
import {ROUTES} from "../../../global-data/routes";
import {FirebaseService} from "../../../services/firebase.service";
import {NavController} from "@ionic/angular";
import {Collection} from "../../../global-data/global";
import * as _ from 'lodash';

@Component({
	selector: 'app-tab2',
	templateUrl: './tab2.page.html',
	styleUrls: ['./tab2.page.scss'],
})
export class Tab2Page implements OnInit {
	auftraege: any = [];
	showKeinTreffer: boolean;
	
	constructor(
		private fire: FirebaseService,
		private navCtrl: NavController,
	) {
	}
	
	ngOnInit() {
		this.fire.getCollection(Collection.Auszahlungen).subscribe((res) => {
			this.auftraege = _.orderBy(res, {genehmigungsStatus: 'beantragt'}, ['desc']);
			
			if(!this.auftraege.length) {
				this.showKeinTreffer = true;
			}
		});
		
	}
	
	goToTest(data) {
		let email = data.email,
			auszahlungsDocId = data.auszahlungsDocId,
			genehmigungsStatus = data.genehmigungsStatus;
		
		this.navCtrl.navigateForward([ROUTES.vipAuszahlung, {
			email,
			auszahlungsDocId,
			genehmigungsStatus
		}]);
	}
}
