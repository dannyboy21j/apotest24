import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuftragsschablonedetailsPageRoutingModule } from './auftragsschablonedetails-routing.module';

import { AuftragsschablonedetailsPage } from './auftragsschablonedetails.page';
import {ComponentsModule} from "../../../../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AuftragsschablonedetailsPageRoutingModule,
    ComponentsModule
  ],
  declarations: [AuftragsschablonedetailsPage]
})
export class AuftragsschablonedetailsPageModule {}
