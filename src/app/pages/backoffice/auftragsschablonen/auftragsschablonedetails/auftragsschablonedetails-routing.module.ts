import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuftragsschablonedetailsPage } from './auftragsschablonedetails.page';

const routes: Routes = [
  {
    path: '',
    component: AuftragsschablonedetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuftragsschablonedetailsPageRoutingModule {}
