import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {DatabaseService} from "../../../../services/database.service";
import * as _ from 'lodash';
import {FirebaseService} from "../../../../services/firebase.service";
import {Collection} from "../../../../global-data/global";

@Component({
	selector: 'app-auftragsschablonedetails',
	templateUrl: './auftragsschablonedetails.page.html',
	styleUrls: ['./auftragsschablonedetails.page.scss'],
})
export class AuftragsschablonedetailsPage implements OnInit {
	formelements;
	anzahlElemente = [];
	title: any;
	docId: any;
	
	constructor(
		private router: ActivatedRoute,
		private fire: FirebaseService
	) {
	}
	
	ngOnInit() {
		this.router.params.subscribe((params) => {
			this.title = params.title;
			this.docId = params.docId;
			this.retrieveFormelements();
		});
	}
	
	async retrieveFormelements() {
		this.fire.getCollection(Collection.FormElementsGenerell).subscribe(formelements => {
			this.formelements = _.orderBy(formelements, ['order']);
			this.anzahlElemente = this.formelements.length;
		});
	}
}
