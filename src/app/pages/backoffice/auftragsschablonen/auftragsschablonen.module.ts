import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuftragsschablonenPageRoutingModule } from './auftragsschablonen-routing.module';

import { AuftragsschablonenPage } from './auftragsschablonen.page';
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		AuftragsschablonenPageRoutingModule,
		ComponentsModule
	],
  declarations: [AuftragsschablonenPage]
})
export class AuftragsschablonenPageModule {}
