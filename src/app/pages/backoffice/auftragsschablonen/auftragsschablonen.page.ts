import {Component, OnInit} from '@angular/core';
import {Collection} from "../../../global-data/global";
import {DatabaseService} from "../../../services/database.service";
import {NavController} from "@ionic/angular";
import {ROUTES} from "../../../global-data/routes";

@Component({
	selector: 'app-auftragsschablonen',
	templateUrl: './auftragsschablonen.page.html',
	styleUrls: ['./auftragsschablonen.page.scss'],
})
export class AuftragsschablonenPage implements OnInit {
	krankheiten: any;
	
	constructor(
		private database: DatabaseService,
		private navCtrl: NavController
	) {
	}
	
	async ngOnInit() {
		this.krankheiten = await this.database.getData(Collection.Krankheiten);
	}
	
	goToDetails(item) {
		let docId = item.docId;
		let title = item.title;
		this.navCtrl.navigateForward([ROUTES.backofficeAuftragsschablonenDetails, {docId,title}]);
	}
}
