import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {DatabaseService} from "../../../../services/database.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Collection, USER} from "../../../../global-data/global";
import * as _ from 'lodash';
import {UtilService} from "../../../../services/util.service";
import {ToastService} from "../../../../services/toast.service";

type TUserInfo = {
	email: string,
	uid: string,
	vorname?: string,
	nachname?: string,
	str?: string,
	ort?: string,
	plz?: string,
	tel?: string
};

@Component({
	selector: 'app-testerdata',
	templateUrl: './testerdata.page.html',
	styleUrls: ['./testerdata.page.scss'],
})
export class TesterdataPage implements OnInit {
	myForm: FormGroup;
	userData: TUserInfo;
	email: any;
	
	formPersonalData = [
		{label: 'Vorname', control: 'vorname', readOnly: false, placeholder: 'Max'},
		{label: 'Nachname', control: 'nachname', readOnly: false, placeholder: 'Mustermann'},
		{label: 'Str. u. Hausnr.', control: 'str', readOnly: false, placeholder: 'Lindenstr. 17'},
		{label: 'PLZ', control: 'plz', readOnly: false, placeholder: '27656'},
		{label: 'Ort', control: 'ort', readOnly: false, placeholder: 'Köln'},
		{label: 'Telefon/Handy (0176-457625)', control: 'tel', readOnly: false, placeholder: '0176-457625'},
		{label: 'Email', control: 'email', readOnly: true, placeholder: 'max@gmail.com'},
		{label: 'Uid', control: 'uid', readOnly: true, placeholder: ''},
	];
	
	constructor(
		private database: DatabaseService,
		private formBuilder: FormBuilder,
		private router: ActivatedRoute,
		private toast: ToastService,
		private util: UtilService
	) {
	}
	
	ngOnInit() {
		this.router.params.subscribe((params) => {
			this.email = params.email;
			this.retrieveStoredData(params.email);
			this.registerFormControlName();
		});
	}
	
	onSubmit(form: FormGroup) {
		let formvalues = this.util.trimAllValuesInObject(form.value);
		_.merge(this.userData, formvalues);
		this.database.updateDataCurrentTester(this.email, this.userData).then((res) => {
			if(res === 'success') {
				this.toast.presentToast('Erfolgreich gespeichert', 'success');
			} else {
				this.toast.presentToast('Fehler', 'danger');
			}
		})
	}
	
	async retrieveStoredData(email) {
		let testUser: any = await this.database.getCurrentTester(email);
		this.userData = testUser[0];
		this.preFillControls();
	}
	
	registerFormControlName() {
		let controlObj = {};
		for (let item of this.formPersonalData) {
			controlObj[item.control] = [''];
			// controlObj[item.control] = ['', Validators.required]
		}
		this.myForm = this.formBuilder.group(controlObj);
		
		// this.myForm = this.formBuilder.group({
		// 	vorname:    [this.userData.vorname,     Validators.required],
		// 	nachname:   [this.userData.nachname,    Validators.required],
		// 	str:        [this.userData.str,         Validators.required],
		// 	plz:        [this.userData.plz,         Validators.required],
		// 	ort:        [this.userData.ort,         Validators.required],
		// 	tel:        [this.userData.tel,         Validators.required],
		// 	email:      [this.userData.email,       Validators.required],
		// });
		
		// Validators.pattern('[a-z0-9.@]*')]
		// Validators.minLength(15)]
		// Validators.email
	}
	
	
	preFillControls() {
		Object.keys(this.userData).forEach((key) => {
			this.myForm.get(key).setValue(this.userData[key]);
		});
		// this.myForm.get('input_alter').setValue('33');
	}
}
