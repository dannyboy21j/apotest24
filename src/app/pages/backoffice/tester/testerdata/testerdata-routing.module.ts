import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TesterdataPage } from './testerdata.page';

const routes: Routes = [
  {
    path: '',
    component: TesterdataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TesterdataPageRoutingModule {}
