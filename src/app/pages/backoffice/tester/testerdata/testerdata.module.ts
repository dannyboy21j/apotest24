import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TesterdataPageRoutingModule } from './testerdata-routing.module';
import { TesterdataPage } from './testerdata.page';
import {ComponentsModule} from "../../../../components/components.module";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		TesterdataPageRoutingModule,
		ComponentsModule,
		ReactiveFormsModule,
		MatFormFieldModule,
		MatInputModule
	],
  declarations: [TesterdataPage]
})
export class TesterdataPageModule {}
