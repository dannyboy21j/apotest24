import {Component, OnInit} from '@angular/core';
import {Collection} from "../../../global-data/global";
import {DatabaseService} from "../../../services/database.service";
import {NavController} from "@ionic/angular";
import {ROUTES} from "../../../global-data/routes";

@Component({
	selector: 'app-tester',
	templateUrl: './tester.page.html',
	styleUrls: ['./tester.page.scss'],
})
export class TesterPage implements OnInit {
	alleTester: any;
	
	constructor(
		private database: DatabaseService,
		private navCtrl: NavController
	) {
	}
	
	async ngOnInit() {
		this.alleTester = await this.database.getData(Collection.Tester);
	}
	
	goToTester(email) {
		this.navCtrl.navigateForward([ROUTES.backofficeTesterData, {email}]);
	}
	
}
