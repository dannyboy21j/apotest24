import {Component, OnInit} from '@angular/core';
import {Collection} from "../../../global-data/global";
import * as _ from 'lodash';
import {DatabaseService} from "../../../services/database.service";
import {NavController} from "@ionic/angular";
import {ROUTES} from "../../../global-data/routes";

@Component({
	selector: 'app-apotheker',
	templateUrl: './apotheker.page.html',
	styleUrls: ['./apotheker.page.scss'],
})
export class ApothekerPage implements OnInit {
	alleApotheker: any;
	
	constructor(
		private database: DatabaseService,
		private navCtrl: NavController
	) {
	}
	
	async ngOnInit() {
		let alleApotheker: any = await this.database.getDataWithDocId(Collection.Apotheker);
		this.alleApotheker = _.sortBy(alleApotheker, [(obj) => obj.data.email]);
	}
	
	goToApotheker(item) {
		let queryParams = JSON.stringify(item);
		this.navCtrl.navigateForward([ROUTES.backofficeApothekerDetails, {queryParams}])
	}
}
