import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApothekerPageRoutingModule } from './apotheker-routing.module';

import { ApothekerPage } from './apotheker.page';
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		ApothekerPageRoutingModule,
		ComponentsModule
	],
  declarations: [ApothekerPage]
})
export class ApothekerPageModule {}
