import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {ApothekerdetailsPageRoutingModule} from './apothekerdetails-routing.module';
import {ApothekerdetailsPage} from './apothekerdetails.page';
import {ComponentsModule} from "../../../../components/components.module";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		ApothekerdetailsPageRoutingModule,
		ComponentsModule,
		ReactiveFormsModule,
		MatFormFieldModule,
		MatInputModule
	],
	declarations: [ApothekerdetailsPage]
})
export class ApothekerdetailsPageModule {
}
