import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApothekerdetailsPage } from './apothekerdetails.page';

const routes: Routes = [
  {
    path: '',
    component: ApothekerdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApothekerdetailsPageRoutingModule {}
