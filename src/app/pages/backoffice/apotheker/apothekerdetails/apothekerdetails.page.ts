import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {Collection, USER} from "../../../../global-data/global";
import {ActivatedRoute} from "@angular/router";
import {UtilService} from "../../../../services/util.service";
import * as _ from 'lodash';
import {DatabaseService} from "../../../../services/database.service";
import {ToastService} from "../../../../services/toast.service";

type TApoInfo = {
	id: string
	data: {
		email: string,
		vorname?: string,
		nachname?: string,
		kontingentTests: number,
		mitarbeiter?: Array<string>
	}
};

@Component({
	selector: 'app-apothekerdetails',
	templateUrl: './apothekerdetails.page.html',
	styleUrls: ['./apothekerdetails.page.scss'],
})
export class ApothekerdetailsPage implements OnInit {
	myForm: FormGroup;
	apoData: TApoInfo;
	formElements = [];
	
	constructor(
		private database: DatabaseService,
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private toast: ToastService,
		private util: UtilService
	) {
	}
	
	ngOnInit() {
		this.route.params.subscribe((params) => {
			this.apoData = JSON.parse(params['queryParams']);
			this.registerFormControlName();
			this.preFillControls();
		});
	}
	
	registerFormControlName() {
		const arrayWithKeys = Object.keys(this.apoData.data);
		this.formElements = _.sortBy(arrayWithKeys);
		
		let controlObj = {};
		for (let item of arrayWithKeys) {
			controlObj[item] = [''];
			// controlObj[item.control] = ['', Validators.required]
		}
		this.myForm = this.formBuilder.group(controlObj);
		
		// this.myForm = this.formBuilder.group({
		// 	vorname:    [this.apoData.vorname,     Validators.required],
		// });
		// Validators.pattern('[a-z0-9.@]*')]
		// Validators.minLength(15)]
		// Validators.email
	}
	
	preFillControls() {
		for (let item of this.formElements) {
			this.myForm.get(item).setValue(this.apoData.data[item]);
		}
		// this.myForm.get('input_alter').setValue('33');
	}
	
	async onSubmit(form: FormGroup) {
		let formvalues = this.util.trimAllValuesInObject(form.value);
		_.merge(this.apoData.data, formvalues);

		this.apoData.data['kontingentTests'] = parseInt(formvalues['kontingentTests']);
		this.apoData.data['preisErhoehung'] = parseInt(formvalues['preisErhoehung']);
		
		let res = await this.database.updateCollection(Collection.Apotheker, this.apoData.id, formvalues);
		if(res === 'success') {
			this.toast.presentToast('Erfolgreich', 'success');
		} else {
			this.toast.presentToast('Fehler', 'error');
		}
	}
}
