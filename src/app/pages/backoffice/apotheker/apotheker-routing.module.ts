import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApothekerPage } from './apotheker.page';

const routes: Routes = [
  {
    path: '',
    component: ApothekerPage
  },
  {
    path: 'apothekerdetails',
    loadChildren: () => import('./apothekerdetails/apothekerdetails.module').then( m => m.ApothekerdetailsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApothekerPageRoutingModule {}
