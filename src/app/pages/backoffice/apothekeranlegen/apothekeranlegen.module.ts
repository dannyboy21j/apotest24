import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {ApothekeranlegenPageRoutingModule} from './apothekeranlegen-routing.module';
import {ApothekeranlegenPage} from './apothekeranlegen.page';
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		FormsModule,
		ApothekeranlegenPageRoutingModule,
		ComponentsModule
	],
	declarations: [ApothekeranlegenPage]
})
export class ApothekeranlegenPageModule {
}
