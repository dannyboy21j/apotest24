import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {FirebaseService} from "../../../services/firebase.service";
import {ToastService} from "../../../services/toast.service";
import {Collection} from "../../../global-data/global";
import {UtilService} from "../../../services/util.service";
import * as _ from 'lodash';
import {DatabaseService} from "../../../services/database.service";

@Component({
	selector: 'app-apothekeranlegen',
	templateUrl: './apothekeranlegen.page.html',
	styleUrls: ['./apothekeranlegen.page.scss'],
})
export class ApothekeranlegenPage implements OnInit{
	@ViewChild('myForm', {static: true}) myForm;
	formelements = [];
	mitarbeiterAnzahl = ['mitarbeiter'];
	disabledButtonRemove = false;
	
	constructor(
		private database: DatabaseService,
		private fire: FirebaseService,
		private toast: ToastService,
		private util: UtilService
	) {
	}
	
	async ngOnInit() {
		const firstPartArray = ['email', 'kontingentTests', 'vorname', 'nachname', 'preisErhoehung'];
		const tmpKrankheiten = await this.database.getData(Collection.Krankheiten);
		const krankheiten = _.map(tmpKrankheiten, 'id');
		const krankheitenPreis = krankheiten.map((krankheit) => {
			return 'preis_' + krankheit
		});
		
		this.formelements = firstPartArray.concat(krankheitenPreis);
	}
	
	submitForm(form: NgForm) {
		let formvalues: any = this.util.trimAllValuesInObject(form.value);
		let cleanedFormvalues: any = this.util.convertStringNumberToNumber(formvalues);
		
		// Check ob Apotheker bereits existiert
		const $sub = this.fire.getCollection(Collection.Apotheker).subscribe((res) => {
			let isApothekerAlreadyExists = _.filter(res, {email: formvalues.email});
			
			if(isApothekerAlreadyExists.length === 0) {
				cleanedFormvalues.angelegtAm = new Date();
				$sub.unsubscribe();
				this.fire.addCollection(Collection.Apotheker, cleanedFormvalues)
					.then(() => this.toast.presentToast('Erfolgreich hinzugefügt', 'success'));
			} else {
				this.toast.presentToast('Apotheker existiert bereits', 'danger');
			}
		});
	}
	
	addNewMitarbeiter() {
		this.mitarbeiterAnzahl.push('mitarbeiter');
		this.disabledButtonRemove = false;
		
	}
	
	removeNewMitarbeiter() {
		this.mitarbeiterAnzahl.pop();
		if(this.mitarbeiterAnzahl.length <= 0) {
			this.disabledButtonRemove = true;
		}
	}
}
