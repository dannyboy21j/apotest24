import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApothekeranlegenPage } from './apothekeranlegen.page';

const routes: Routes = [
  {
    path: '',
    component: ApothekeranlegenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApothekeranlegenPageRoutingModule {}
