import {Component, OnInit} from '@angular/core';
import {FirebaseService} from "../../../services/firebase.service";
import {AngularFirestore} from "@angular/fire/firestore";
import * as _ from 'lodash';
import {ToastService} from "../../../services/toast.service";

@Component({
	selector: 'app-landingpage',
	templateUrl: './landingpage.page.html',
	styleUrls: ['./landingpage.page.scss'],
})
export class LandingpagePage implements OnInit {
	docId: string;
	col: string;
	subcol: string;
	
	Krankheiten = {
		generell: 'S7qAR2sdKTUhPQAUaio3',
		durchfall: '13036HSy88jVkYwUZLcG',
		fusspilz: 'DGaUqLxYlrbmE3tRA7P6',
		haemorrhoiden: '20zHKYyDUHkeJreZCs3p',
		halsschmerzen: '5ov9CyhEUhcH0JQWF3Kf',
		kopfschmerzen: 'lFgcAvxu73ApqYUnlTKB',
		magenkraempfe: 'nrRBwkDQAU6PqS1MNebn',
		nasenneben: 'kkUbYKWIxwLPccqVODXn',
		sodbrennen: 'yvGFUK4VzxNnkdvI8Xp4'
	};
	
	forms = {
		apotheke: {
			// apotheken: this.db.doc('/Apotheken/eins'),
			order: 5,
			required: true,
			subtyp: 'apotheke',
			title: 'radio_apotheke',
			typ: 'radio'
		},
		feedback: {
			max: 20,
			min: 2,
			order: 2,
			placeholder: 'Langantwort-Text',
			required: true,
			textarea: true,
			title: 'textarea_feedback',
			typ: 'textarea'
		},
		beratung: {
			order: 10,
			required: true,
			skala: true,
			title: 'skala_beratung',
			typ: 'skala'
		},
		freundlichkeit: {
			order: 6,
			required: true,
			skala: true,
			title: 'skala_freundlichkeit',
			typ: 'skala'
		},
		alter: {
			max: 2,
			min: 1,
			order: 3,
			placeholder: 'Meine Antwort',
			required: true,
			subtyp: 'number',
			title: 'input_alter',
			typ: 'input'
		},
		photoImmobilie: {
			maxPhotos: 4,
			minPhotos: 2,
			order: 1,
			title: 'photo_immobilie',
			subtyp: 'immobilie',
			typ: 'photo'
		},
		photoRechnung: {
			maxPhotos: 4,
			minPhotos: 2,
			order: 1,
			title: 'photo_rechnung',
			subtyp: 'rechnung',
			typ: 'photo'
		},
		produktempfehlung: {
			checkboxen: ['Riopan3', 'Gaviscon3', 'Nux Vumica3', 'Iberogast3'],
			formarray: 'checkbox_produktempfehlung',
			title: 'checkbox_produktempfehlung',
			typ: 'checkbox'
		},
		geschlecht: {
			order: 0,
			required: true,
			subtyp: 'geschlecht',
			title: 'radio_geschlecht',
			typ: 'radio'
		},
		giveaway: {
			order: 4,
			required: true,
			subtyp: 'janein',
			title: 'radio_giveaway',
			typ: 'radio'
		},
		tuete: {
			order: 4,
			required: true,
			subtyp: 'janein',
			title: 'radio_tuete',
			typ: 'radio'
		},
		namensschild: {
			order: 7,
			required: true,
			subtyp: 'janein',
			title: 'radio_namensschild',
			typ: 'radio'
		},
		arbeitskleidung: {
			order: 7,
			required: true,
			subtyp: 'janein',
			title: 'radio_arbeitskleidung',
			typ: 'radio'
		},
		treuestempel: {
			order: 6,
			required: true,
			subtyp: 'janein',
			title: 'radio_treuestempel',
			typ: 'radio'
		},
		weiterearzneimittel: {
			order: 1,
			required: true,
			subtyp: 'janein',
			title: 'radio_weiterearzneimittel',
			typ: 'radio'
		},
		zeitung: {
			order: 8,
			required: true,
			subtyp: 'janein',
			title: 'radio_zeitung',
			typ: 'radio'
		},
	};
	
	constructor(
		private fire: FirebaseService,
		private db: AngularFirestore,
		private toast: ToastService
	) {
	}
	
	ngOnInit() {
		// this.col = 'AuftragsSchablonenIndividuell';
		this.col = 'AuftragsSchablonen';
		this.subcol = 'formelements';
		this.docId = this.Krankheiten.generell;
		this.setInDB(this.forms.photoImmobilie);
	}
	
	setInDB(obj) {
		// 1. CHECK, ob bereits Item vorhanden ist.
		this.fire.getSubCollection(this.col, this.docId, this.subcol).subscribe((res) => {
			let finding = _.filter(res, {title: obj.title});
			
			if (finding.length) {
				this.toast.presentToast('Krankheit existiert bereits', 'error');
			} else {
		// 2. Add Data
				this.fire.addDataInDBAutoKey(this.col, this.docId, this.subcol, obj)
				.then(() => this.toast.presentToast('Erfolgreich hinzugefügt', 'success'));
			}
		});
	}
	
}
