import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LandingpagePageRoutingModule } from './landingpage-routing.module';

import { LandingpagePage } from './landingpage.page';
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		LandingpagePageRoutingModule,
		ComponentsModule
	],
  declarations: [LandingpagePage]
})
export class LandingpagePageModule {}
