import {Component, OnInit} from '@angular/core';
import {ROUTES} from "../../../global-data/routes";

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.page.html',
	styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
	ROUTES = ROUTES;
	
	constructor() {
	}
	
	ngOnInit() {
	}
	
}
