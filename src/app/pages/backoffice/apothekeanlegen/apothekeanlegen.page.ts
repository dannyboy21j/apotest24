import {Component, ViewChild} from '@angular/core';
import {FirebaseService} from "../../../services/firebase.service";
import {ToastService} from "../../../services/toast.service";
import {UtilService} from "../../../services/util.service";
import {NgForm} from "@angular/forms";
import {Collection} from "../../../global-data/global";
import * as _ from 'lodash';

@Component({
	selector: 'app-apothekeanlegen',
	templateUrl: './apothekeanlegen.page.html',
	styleUrls: ['./apothekeanlegen.page.scss'],
})
export class ApothekeanlegenPage {
	@ViewChild('myForm', {static: true}) myForm;
	formelements = ['id', 'apotheker','name', 'ort', 'plz', 'str','formvalue','telefon','fax'];
	
	constructor(
		private fire: FirebaseService,
		private toast: ToastService,
		private util: UtilService
	) {
	}
	
	submitForm(form: NgForm) {
		let formvalues: any = this.util.trimAllValuesInObject(form.value);
		this.fire.addCollection(Collection.Apotheken, formvalues)
			.then(() => this.toast.presentToast('Apotheke hinzugefügt', 'success'));
	}
}
