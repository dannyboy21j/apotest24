import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApothekeanlegenPage } from './apothekeanlegen.page';

const routes: Routes = [
  {
    path: '',
    component: ApothekeanlegenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApothekeanlegenPageRoutingModule {}
