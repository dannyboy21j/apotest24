import {Component, OnInit} from '@angular/core';
import {DatabaseService} from "../../../services/database.service";
import {ROUTES} from "../../../global-data/routes";
import {NavController} from "@ionic/angular";
import * as _ from 'lodash';

@Component({
	selector: 'app-tab3',
	templateUrl: './tab3.page.html',
	styleUrls: ['./tab3.page.scss'],
})
export class Tab3Page implements OnInit {
	mitarbeiter = [];
	
	constructor(
		private database: DatabaseService,
		private navCtrl: NavController
	) {
	}
	
	async ngOnInit() {
		let apotheker: any = await this.database.getCurrentApotheker();
		let tmpMitarbeiter = [];
		
		Object.keys(apotheker[0]).forEach(key => {
			if(key.startsWith('mitarbeiter_')) {
				tmpMitarbeiter.push(apotheker[0][key]);
			}
		});
		// this.mitarbeiter = _.sortBy(tmpMitarbeiter);
		this.anzahlTestsProMitarbeiter(apotheker[0].email, _.sortBy(tmpMitarbeiter));
	}
	
	async anzahlTestsProMitarbeiter(email, mitarbeiter) {
		const alleGemachtenAuftraegeZuEinemApotheker: any = await this.database.getAlleGemachtenAuftraegeZuEinemApotheker(email);
		const vorkommenMitarbeiter = _.countBy(alleGemachtenAuftraegeZuEinemApotheker, 'select_mitarbeiter');
		const vorkommenMitarbeiterArray = Object.keys(vorkommenMitarbeiter);
		let tmpMitarbeiter = [];
		
		for(const arbeiter of mitarbeiter) {
			let obj = {};
			let tmpVorkommenMitarbeiter = 0;
			obj['name'] = arbeiter;
			
			for(const auftrag of alleGemachtenAuftraegeZuEinemApotheker) {
				if(auftrag['select_mitarbeiter'] === arbeiter && _.includes(vorkommenMitarbeiterArray, arbeiter)) {
					tmpVorkommenMitarbeiter = vorkommenMitarbeiter[arbeiter];
					break;
				}
			}
			obj['anzahlMitarbeiterTest'] = tmpVorkommenMitarbeiter;
			tmpMitarbeiter.push(obj);
		}
		this.mitarbeiter = _.orderBy(tmpMitarbeiter, ['anzahlMitarbeiterTest', 'name'], ['desc', 'asc'])
	}
	
	goToAuftraegePage(name) {
		let mitarbeitername = name,
			comeFrom = 'TAB3';
		
		this.navCtrl.navigateForward([ROUTES.apoZwischenseiteKrankheiten, {
			mitarbeitername,
			comeFrom
		}]);
	}
}
