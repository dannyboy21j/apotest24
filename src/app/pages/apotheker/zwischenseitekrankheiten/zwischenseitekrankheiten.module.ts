import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ZwischenseitekrankheitenPageRoutingModule } from './zwischenseitekrankheiten-routing.module';

import { ZwischenseitekrankheitenPage } from './zwischenseitekrankheiten.page';
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ZwischenseitekrankheitenPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ZwischenseitekrankheitenPage]
})
export class ZwischenseitekrankheitenPageModule {}
