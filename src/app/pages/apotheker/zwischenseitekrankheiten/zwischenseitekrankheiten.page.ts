import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {DatabaseService} from "../../../services/database.service";
import {Collection} from "../../../global-data/global";
import {ROUTES} from "../../../global-data/routes";
import {NavController} from "@ionic/angular";
import * as _ from 'lodash';

@Component({
	selector: 'app-zwischenseitekrankheiten',
	templateUrl: './zwischenseitekrankheiten.page.html',
	styleUrls: ['./zwischenseitekrankheiten.page.scss'],
})
export class ZwischenseitekrankheitenPage implements OnInit {
	apothekenName = '';
	apothekenId;
	comeFrom: string;
	themeColor: string;
	mitarbeitername: string = "";
	
	krankheiten: any = [];
	ergebnis = []
	
	constructor(
		private database: DatabaseService,
		private navCtrl: NavController,
		private router: ActivatedRoute
	) {
	}
	ngOnInit() {
		// Zeige alle Krankheiten, die in einer Apotheke gemacht wurden
		this.router.params.subscribe(async (params) => {
			this.apothekenId = params.apothekenId;
			this.comeFrom = params.comeFrom;
			this.mitarbeitername = params.mitarbeitername;
			this.apothekenName = params.apothekenName || '';
			this.krankheiten = await this.database.getData(Collection.Krankheiten);
			
			let auftraege: any;
			if(params.comeFrom === 'TAB2') {
				auftraege = await this.database.getAlleGemachtenAuftraegeZuEinemApothekerInEinerBestimmtenApotheke(params.apothekerEmail, params.apothekenId);
				this.themeColor = 'primary';
			} else if(params.comeFrom === 'TAB3') {
				auftraege = await this.database.getAlleAuftraegeEinesMitarbeiters(params.mitarbeitername);
				this.themeColor = 'success';
			}
			
			let ergebnis = Object.values(_.groupBy(auftraege, (obj) => [[obj.data.krankheitsId]]));
			this.ergebnis = _.orderBy(ergebnis).reverse();
		});
	}
	
	getKrankheitsName(auftrag) {
		return _.filter(this.krankheiten, {id: auftrag.data.krankheitsId})[0].title;
	}
	
	getGesamtZahlTests() {
		return _.sumBy(this.ergebnis, function(o) { return o.length; });
	}
	
	goToAuswertungPage(auftrag) {
		let comeFrom = this.comeFrom,
			mitarbeitername = this.mitarbeitername,
			apothekenId = this.apothekenId,
			apothekenName = this.apothekenName,
			title = '',
			krankheitsId = '';
		
		// if(this.apothekenName) {
		// 	title = this.apothekenName + '<br> Allgemeine Auswertung';
		// } else if (this.mitarbeitername) {
		// 	title = this.mitarbeitername + '<br> Allgemeine Auswertung';
		// }
		//
		if(auftrag !== 'Allgemeine Auswertung') {
			krankheitsId = auftrag.data.krankheitsId;
			title = this.getKrankheitsName(auftrag);
		} else {
			title = auftrag;
		}
		
		this.navCtrl.navigateForward([ROUTES.apoAuswertungLandingpage, {
			comeFrom,
			mitarbeitername,
			apothekenId,
			apothekenName,
			krankheitsId,
			title
		}]);
	}
}
