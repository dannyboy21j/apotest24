import {Component, OnInit} from '@angular/core';
import {DatabaseService} from "../../../services/database.service";
import {NavController} from "@ionic/angular";
import * as _ from 'lodash';
import {ROUTES} from "../../../global-data/routes";
import {SpinnerService} from "../../../services/spinner.service";

@Component({
	selector: 'app-tab2',
	templateUrl: './tab2.page.html',
	styleUrls: ['./tab2.page.scss'],
})
export class Tab2Page implements OnInit {
	alleApotheken = [];
	email: string;
	
	constructor(
		private database: DatabaseService,
		private navCtrl: NavController,
		private spinner: SpinnerService
	) {
	}
	
	async ngOnInit() {
		this.spinner.show();
		this.email = await this.database.getEmail();
		let apotheken: any = await this.database.getCurrentApotheken(this.email);
		this.calculateScoreValue(this.email, apotheken);
	}
	
	async calculateScoreValue(apothekerMail: string, apotheken: Array<object>) {
		for(let apotheke of apotheken) {
			let tests: any = await this.database.getAuftraegeEinerBestimmtenApothekeEinesApothekers(apothekerMail, apotheke['id']);
			let score = 0;
			
			for(let test of tests) {
				if(test.radio_giveaway && test.radio_giveaway === 'ja') {
					score = score + 5;
				}
				if(test.radio_zeitung && test.radio_zeitung === 'ja') {
					score = score + 5;
				}
				if(test.radio_treuestempel && test.radio_treuestempel === 'ja') {
					score = score + 5;
				}
				if(test.radio_namensschild && test.radio_namensschild === 'ja') {
					score = score + 5;
				}
				if(test.radio_tuete && test.radio_tuete === 'ja') {
					score = score + 5;
				}
				if(test.radio_arbeitskleidung && test.radio_arbeitskleidung === 'ja') {
					score = score + 5;
				}
				if(test['skala_beratung']) {
					score = score + Number(test['skala_beratung']);
				}
				if(test['skala_freundlichkeit']) {
					score = score + Number(test['skala_freundlichkeit']);
				}
			}
			// apotheke['score'] = _.floor((score/(tests.length * 26)) * 100) || 0;
			apotheke['score'] = score;
		}
		this.alleApotheken = _.orderBy(apotheken,['score'],['desc']);
		this.spinner.hide();
	}
	
	goToPage(apo) {
		let apothekenId = apo.id;
		let apothekerEmail = apo.apotheker;
		let apothekenName = apo.name;
		let comeFrom = 'TAB2';
		// let title = form.title;
		this.navCtrl.navigateForward([ROUTES.apoZwischenseiteKrankheiten, {apothekenId, apothekerEmail, comeFrom, apothekenName}]);
	}
}
