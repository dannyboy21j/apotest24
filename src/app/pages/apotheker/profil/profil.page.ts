import {Component, OnInit} from '@angular/core';
import {DatabaseService} from "../../../services/database.service";

@Component({
	selector: 'app-profil',
	templateUrl: './profil.page.html',
	styleUrls: ['./profil.page.scss'],
})
export class ProfilPage implements OnInit {
	profilText = '';
	
	constructor(
		private database: DatabaseService
	) {
	}
	
	async ngOnInit() {
		const apothekerData = await this.database.getCurrentApotheker();
		this.profilText = apothekerData[0].profiltext;
	}
}
