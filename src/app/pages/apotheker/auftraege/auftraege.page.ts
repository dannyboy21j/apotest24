import {Component} from '@angular/core';
import {NavController} from "@ionic/angular";
import {AuthService} from "../../../modules/authentification/auth.service";
import {FirebaseService} from "../../../services/firebase.service";
import {ROUTES} from "../../../global-data/routes";
import {Collection, GLOBAL} from "../../../global-data/global";
import {DatabaseService} from "../../../services/database.service";
import {EventsService} from "../../../services/events.service";
import * as _ from 'lodash';

@Component({
	selector: 'app-auftraege',
	templateUrl: './auftraege.page.html',
	styleUrls: ['./auftraege.page.scss'],
})
export class AuftraegePage {
	ROUTES = ROUTES;
	GLOBAL = GLOBAL;
	email: string;
	activeForms: ISchablone[] = [];
	inactiveForms: ISchablone[] = [];
	
	constructor(
		private auth: AuthService,
		private database: DatabaseService,
		private eventService: EventsService,
		private fire: FirebaseService,
		private navCtrl: NavController,
	) {
		this.auth.getEmail();
		this.subscriber();
	}
	
	subscriber() {
		this.eventService.subscribe('pub-email', (res)=> {
			if(res.email) {
				this.email = res.email;
				this.showAuftraege();
			}
		});
	}
	
	async showAuftraege() {
		let apothekermail = await this.database.getEmail();
		let krankheiten: any = await this.database.getData(Collection.Krankheiten);
		// let apotheker: any = await this.database.getCurrentApotheker(); -
		// Das wurde auskommentiert, da sonst eine Aktualisierung der Daten nicht stattfinden
		this.fire.getCollectionWithQuery(Collection.Apotheker, 'email', apothekermail).subscribe(async(apotheker: any) =>{
			if(krankheiten.length && apotheker.length) {
				let deaktiveAuftraege = apotheker[0].deaktAuftraege;
				
				for(let form of krankheiten) {
					let anzahl = await this.database.getAuftraegeEinerBestimmtenKrankheitEinesApothekers(apothekermail, form.id);
					form['anzahlAuftraege'] = (anzahl) ? anzahl : 0;
				}
				
				let activeForms = _.reject(krankheiten, (schablone) => {
					return _.includes(deaktiveAuftraege, schablone.id);
				});
				this.activeForms = activeForms;
				this.ermittelDeaktiveAuftraege(krankheiten, deaktiveAuftraege);
			}
		});
	}
	
	ermittelDeaktiveAuftraege(schablonen, deaktiveAuftraege) {
		this.inactiveForms = _.filter(schablonen, (schablone) => {
			return _.includes(deaktiveAuftraege, schablone.id);
		});
	}
	
	goToPage(form) {
		let krankheitsId = form.id;
		let title = form.title;
		this.navCtrl.navigateForward([ROUTES.apoAuswertungLandingpage, {krankheitsId, title}]);
	}
}


