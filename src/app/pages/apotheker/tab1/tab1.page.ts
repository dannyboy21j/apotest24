import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../modules/authentification/auth.service";
import {DatabaseService} from "../../../services/database.service";
import {FirebaseService} from "../../../services/firebase.service";
import {NavController} from "@ionic/angular";
import {Collection} from "../../../global-data/global";
import {ROUTES} from "../../../global-data/routes";
import * as _ from 'lodash';

@Component({
	selector: 'app-tab1',
	templateUrl: './tab1.page.html',
	styleUrls: ['./tab1.page.scss'],
})
export class Tab1Page implements OnInit {
	krankheiten = [];
	title: any;
	email: string;
	
	constructor(
		private auth: AuthService,
		private database: DatabaseService,
		private fire: FirebaseService,
		private navCtrl: NavController
	) {
	}
	
	async ngOnInit() {
		this.email = await this.database.getEmail();
		this.ermittelAktiveTests();
		this.title = 'Themenübersicht';
	}
	
	async ermittelAktiveTests() {
		const Krankheiten: any = await this.database.getData(Collection.Krankheiten);
		const auftraege: any = await this.database.getAlleGemachtenAuftraegeZuEinemApotheker(this.email);
		
		for(let item of Krankheiten) {
			item.anzahlAbsolviert = _.filter(auftraege, { krankheitsId: item.id }).length;
		}
		this.krankheiten = _.orderBy(Krankheiten, ['anzahlAbsolviert'], ['desc']);
	}
	
	goToAuftraegePage(krankheit) {
		let krankheitsId = krankheit.id,
			title = krankheit.title,
			comeFrom = 'TAB1';
		
		this.navCtrl.navigateForward([ROUTES.apoAuswertungLandingpage, {
			krankheitsId,
			title,
			comeFrom
		}]);
	}
}
