import { Component, OnInit } from '@angular/core';
import {GLOBAL} from "../../../global-data/global";

@Component({
  selector: 'app-anleitung',
  templateUrl: './anleitung.page.html',
  styleUrls: ['./anleitung.page.scss'],
})
export class AnleitungPage implements OnInit {
  GLOBAL = GLOBAL;
  constructor() { }

  ngOnInit() {
  }
}
