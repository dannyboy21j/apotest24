import {Component, Input, OnInit} from '@angular/core';
import {DatabaseService} from "../../../../services/database.service";
import * as _ from 'lodash';
import {GLOBAL} from "../../../../global-data/global";

@Component({
	selector: 'ueberblick',
	templateUrl: './ueberblick.page.html',
	styleUrls: ['./ueberblick.page.scss'],
})
export class UeberblickPage implements OnInit {
	@Input() krankheitsId;
	@Input() apothekenId;
	@Input() mitarbeitername;
	@Input() comeFrom;
	@Input() themeColor;

	GLOBAL = GLOBAL;
	showTreffer: boolean = false;
	valueW: number = 0;
	valueM: number = 0;
	zeitungJa: number = 0;
	zeitungNein: number = 0;
	giveawayJa: number = 0;
	giveawayNein: number = 0;
	gabEsKaufJa: number = 0;
	gabEsKaufNein: number = 0;
	treuestempelJa: number = 0;
	treuestempelNein: number = 0;
	tueteJa: number = 0;
	tueteNein: number = 0;
	namensschildJa: number = 0;
	namensschildNein: number = 0;
	kleidungJa: number = 0;
	kleidungNein: number = 0;
	weitereArzneimittelJa: number = 0;
	weitereArzneimittelNein: number = 0;
	
	constructor(
		private database: DatabaseService,
	) {
	}

	ngOnInit() {
		this.getAlleGemachtenAuftraege();
	}
	
	async getAlleGemachtenAuftraege() {
		let apothekermail = await this.database.getEmail();
		let auftraege: any;

		if(this.comeFrom === 'TAB1') {
			auftraege = await this.database.getAuftraegeEinerBestimmtenKrankheitEinesApothekers(apothekermail, this.krankheitsId);
		} else if (this.comeFrom === 'TAB2') {
			if(this.krankheitsId && this.krankheitsId !== 'undefined') {
				auftraege = await this.database.getAlleGemachtenAuftraegeEinerBestimmtenKrankheitZuEinemApothekerInEinerBestimmtenApothekeOhne(apothekermail, this.apothekenId, this.krankheitsId);
			} else {
				auftraege = await this.database.getAlleGemachtenAuftraegeZuEinemApothekerInEinerBestimmtenApothekeOhne(apothekermail, this.apothekenId);
			}
		} else if (this.comeFrom === 'TAB3') {
			if(this.krankheitsId) {
				auftraege = await this.database.getAlleAuftraegeEinesMitarbeitersZurEinerBestimmtenKrankheitOhne(this.mitarbeitername, this.krankheitsId);
			} else {
				auftraege = await this.database.getAlleAuftraegeEinesMitarbeitersOhne(this.mitarbeitername);
			}
		}
		if(auftraege && auftraege.length) {
			this.geschlechtData(auftraege);
			this.weitereArzneimittelData(auftraege);
			this.giveAwayData(auftraege);
			this.gabEsKaufData(auftraege);
			this.treuestempelData(auftraege);
			this.zeitungData(auftraege);
			this.tueteData(auftraege);
			this.namensschildData(auftraege);
			this.kleidungData(auftraege);
			this.freundlichkeit(auftraege);
			this.beratung(auftraege);
		}
	}
	
	showFullBalken = [true,true,true,true,true];
	anzahl = [0,0,0,0,0];
	prozentFreundlichkeit = [0,0,0,0,0];
	maxHeight = 180;
	balkenHeight = [0,0,0,0,0];
	showFreundlichkeit = false;

	freundlichkeit(res) {
		let freundlichkeit = _.map(_.countBy(res, "skala_freundlichkeit"), (val, key) => ({value: key, total: val}));
		freundlichkeit = _.reject(freundlichkeit, {value: 'undefined'});
		let gesamtTotal = _.sumBy(freundlichkeit, 'total');
		this.showFreundlichkeit = !_.every(freundlichkeit, ['total', 0]);

		if(this.showFreundlichkeit) {
			this.showTreffer = true;
			let hoechsterWert = _.maxBy(freundlichkeit, 'total');
			let prozentWert = 100 / Number(hoechsterWert.total);
			let balkenIndex = Number(hoechsterWert.value);
			this.balkenHeight[balkenIndex - 1] = this.maxHeight;
			
			for(let item of freundlichkeit) {
				if(item.value == '1') {
					this.balkenHeight[0] = (this.maxHeight * prozentWert * item.total) / 100;
					this.anzahl[0] = item.total;
					this.prozentFreundlichkeit[0] = (item.total / gesamtTotal) * 100;
				} else if(item.value == '2') {
					this.balkenHeight[1] = (this.maxHeight * prozentWert * item.total) / 100;
					this.anzahl[1] = item.total;
					this.prozentFreundlichkeit[1] = (item.total / gesamtTotal) * 100;
				} else if(item.value == '3') {
					this.balkenHeight[2] = (this.maxHeight * prozentWert * item.total) / 100;
					this.anzahl[2] = item.total;
					this.prozentFreundlichkeit[2] = (item.total / gesamtTotal) * 100;
				} else if(item.value == '4') {
					this.balkenHeight[3] = (this.maxHeight * prozentWert * item.total) / 100;
					this.anzahl[3] = item.total;
					this.prozentFreundlichkeit[3] = (item.total / gesamtTotal) * 100;
				} else if(item.value == '5') {
					this.balkenHeight[4] = (this.maxHeight * prozentWert * item.total) / 100;
					this.anzahl[4] = item.total;
					this.prozentFreundlichkeit[4] = (item.total / gesamtTotal) * 100;
				}
			}
			
			for(let i in this.balkenHeight) {
				if(this.balkenHeight[i] == 0) {
					this.showFullBalken[i] = false;
				}
			}
		}
	}
	
	showFullBalkenBeratung = [true,true,true,true,true];
	anzahlBeratung = [0,0,0,0,0];
	prozentBeratung = [0,0,0,0,0];
	balkenHeightBeratung = [0,0,0,0,0];
	showBeratung = false;
	
	beratung(res) {
		let beratung = _.map(_.countBy(res, "skala_beratung"), (val, key) => ({value: key, total: val}));
		beratung = _.reject(beratung, {value: 'undefined'});
		let gesamtTotal = _.sumBy(beratung, 'total');
		this.showBeratung = !_.every(beratung, ['total', 0]);
		
		if(this.showBeratung) {
			this.showTreffer = true;
			let hoechsterWert = _.maxBy(beratung, 'total');
			let prozentWert = 100 / Number(hoechsterWert.total);
			let balkenIndex = Number(hoechsterWert.value);
			this.balkenHeightBeratung[balkenIndex - 1] = this.maxHeight;
			
			for(let item of beratung) {
				if(item.value == '1') {
					this.balkenHeightBeratung[0] = (this.maxHeight * prozentWert * item.total) / 100;
					this.anzahlBeratung[0] = item.total;
					this.prozentBeratung[0] = (item.total / gesamtTotal) * 100;
				} else if(item.value == '2') {
					this.balkenHeightBeratung[1] = (this.maxHeight * prozentWert * item.total) / 100;
					this.anzahlBeratung[1] = item.total;
					this.prozentBeratung[1] = (item.total / gesamtTotal) * 100;
				} else if(item.value == '3') {
					this.balkenHeightBeratung[2] = (this.maxHeight * prozentWert * item.total) / 100;
					this.anzahlBeratung[2] = item.total;
					this.prozentBeratung[2] = (item.total / gesamtTotal) * 100;
				} else if(item.value == '4') {
					this.balkenHeightBeratung[3] = (this.maxHeight * prozentWert * item.total) / 100;
					this.anzahlBeratung[3] = item.total;
					this.prozentBeratung[3] = (item.total / gesamtTotal) * 100;
				} else if(item.value == '5') {
					this.balkenHeightBeratung[4] = (this.maxHeight * prozentWert * item.total) / 100;
					this.anzahlBeratung[4] = item.total;
					this.prozentBeratung[4] = (item.total / gesamtTotal) * 100;
				}
			}
			
			for(let i in this.balkenHeightBeratung) {
				if(this.balkenHeightBeratung[i] == 0) {
					this.showFullBalkenBeratung[i] = false;
				}
			}
		}
	}
	
	
	showFullBalkenW = {'first': undefined, 'second': undefined};
	balkenWidthM = {'first': undefined, 'second': undefined};
	prozentGeschlechtM;
	prozentGeschlechtW;
	showGeschlecht = false;
	
	geschlechtData(res) {
		let geschlecht = _.map(_.countBy(res, "radio_geschlecht"), (val, key) => ({value: key, total: val}));
		let gesamtTotal = _.sumBy(geschlecht, 'total');
		
		if(geschlecht.length) {
			let j = _.findIndex(geschlecht, {value: 'w'});
			let i = _.findIndex(geschlecht, {value: 'm'});
			
			if(j !== -1 && geschlecht[j] && geschlecht[j].total) {
				this.valueW = geschlecht[j].total;
				this.prozentGeschlechtW = ((this.valueW / gesamtTotal) * 100).toFixed();
			}
			if(i !== -1 && geschlecht[i] && geschlecht[i].total) {
				this.valueM = geschlecht[i].total;
				this.prozentGeschlechtM = ((this.valueM / gesamtTotal) * 100).toFixed();
			}
			
			if(i !== -1 || j !== -1) {
				this.showTreffer = true;
				this.showGeschlecht = true;
				this.showFullBalkenW = this.getObjectAboutFullBalkenStatus(this.valueW, this.valueM);
				this.balkenWidthM = this.getBalkenWidth(this.valueW, this.valueM);
			}
			
		}
	}

	
	showFullBalkenZeitung = {'first': undefined, 'second': undefined};
	balkenWidthZeitung = {'first': undefined, 'second': undefined};
	prozentZeitungJa;
	prozentZeitungNein;
	showZeitung = false;
	
	zeitungData(res) {
		let zeitung = _.map(_.countBy(res, "radio_zeitung"), (val, key) => ({value: key, total: val}));
		let gesamtTotal = _.sumBy(zeitung, 'total');
		
		if(zeitung.length) {
			let i = _.findIndex(zeitung, {value: 'ja'});
			let j = _.findIndex(zeitung, {value: 'nein'});
			
			if(i !== -1 && zeitung[i] && zeitung[i].total) {
				this.zeitungJa = zeitung[i].total;
				this.prozentZeitungJa = ((this.zeitungJa / gesamtTotal) * 100).toFixed();
			}
			if(j !== -1 && zeitung[j] && zeitung[j].total) {
				this.zeitungNein = zeitung[j].total;
				this.prozentZeitungNein = ((this.zeitungNein / gesamtTotal) * 100).toFixed();
			}
			
			if(i !== -1 || j !== -1) {
				this.showTreffer = true;
				this.showZeitung = true;
				this.showFullBalkenZeitung = this.getObjectAboutFullBalkenStatus(this.zeitungJa, this.zeitungNein);
				this.balkenWidthZeitung = this.getBalkenWidth(this.zeitungJa, this.zeitungNein);
			}
		}
	}
	
	
	showFullBalkenGiveAway = {'first': undefined, 'second': undefined};
	balkenWidthGiveAway = {'first': undefined, 'second': undefined};
	prozentAwayJa;
	prozentAwayNein;
	showGiveAway = false;
	
	giveAwayData(res) {
		let giveaway = _.map(_.countBy(res, "radio_giveaway"), (val, key) => ({value: key, total: val}));
		let gesamtTotal = _.sumBy(giveaway, 'total');
		
		if(giveaway.length) {
			let i = _.findIndex(giveaway, {value: 'ja'});
			let j = _.findIndex(giveaway, {value: 'nein'});
			
			if(i !== -1 && giveaway[i] && giveaway[i].total) {
				this.giveawayJa = giveaway[i].total;
				this.prozentAwayJa = ((this.giveawayJa / gesamtTotal) * 100).toFixed();
			}
			if(j !== -1 && giveaway[j] && giveaway[j].total) {
				this.giveawayNein = giveaway[j].total;
				this.prozentAwayNein = ((this.giveawayNein / gesamtTotal) * 100).toFixed();
			}
			if(i !== -1 || j !== -1) {
				this.showTreffer = true;
				this.showGiveAway = true;
				this.showFullBalkenGiveAway = this.getObjectAboutFullBalkenStatus(this.giveawayJa, this.giveawayNein);
				this.balkenWidthGiveAway = this.getBalkenWidth(this.giveawayJa, this.giveawayNein);
			}
		}
	}
	
	showFullBalkenGabEsKauf = {'first': undefined, 'second': undefined};
	balkenWidthGabEsKauf = {'first': undefined, 'second': undefined};
	prozentGabEsKaufJa;
	prozentGabEsKaufNein;
	showGabEsKauf = false;
	
	gabEsKaufData(res) {
		let gabEsKauf = _.map(_.countBy(res, "radio_kaufentscheidung"), (val, key) => ({value: key, total: val}));
		let gesamtTotal = _.sumBy(gabEsKauf, 'total');
		
		if(gabEsKauf.length) {
			let i = _.findIndex(gabEsKauf, {value: 'ja'});
			let j = _.findIndex(gabEsKauf, {value: 'nein'});
			
			if(i !== -1 && gabEsKauf[i] && gabEsKauf[i].total) {
				this.gabEsKaufJa = gabEsKauf[i].total;
				this.prozentGabEsKaufJa = ((this.gabEsKaufJa / gesamtTotal) * 100).toFixed();
			}
			if(j !== -1 && gabEsKauf[j] && gabEsKauf[j].total) {
				this.gabEsKaufNein = gabEsKauf[j].total;
				this.prozentGabEsKaufNein = ((this.gabEsKaufNein / gesamtTotal) * 100).toFixed();
			}
			if(i !== -1 || j !== -1) {
				this.showTreffer = true;
				this.showGabEsKauf = true;
				this.showFullBalkenGabEsKauf = this.getObjectAboutFullBalkenStatus(this.gabEsKaufJa, this.gabEsKaufNein);
				this.balkenWidthGabEsKauf = this.getBalkenWidth(this.gabEsKaufJa, this.gabEsKaufNein);
			}
		}
	}

	
	showFullBalkenStempel = {'first': undefined, 'second': undefined};
	balkenWidthStempel = {'first': undefined, 'second': undefined};
	prozentStempelJa;
	prozentStempelNein;
	showStempel = false;
	
	treuestempelData(res) {
		let treuestempel = _.map(_.countBy(res, "radio_treuestempel"), (val, key) => ({value: key, total: val}));
		let gesamtTotal = _.sumBy(treuestempel, 'total');
		
		if(treuestempel.length) {
			let i = _.findIndex(treuestempel, {value: 'ja'});
			let j = _.findIndex(treuestempel, {value: 'nein'});
			
			if(i !== -1 && treuestempel[i] && treuestempel[i].total) {
				this.treuestempelJa = treuestempel[i].total;
				this.prozentStempelJa = ((this.treuestempelJa / gesamtTotal) * 100).toFixed();
			}
			if(j !== -1 && treuestempel[j] && treuestempel[j].total) {
				this.treuestempelNein = treuestempel[j].total;
				this.prozentStempelNein = ((this.treuestempelNein / gesamtTotal) * 100).toFixed();
			}
			if(i !== -1 || j !== -1) {
				this.showTreffer = true;
				this.showStempel = true;
				this.showFullBalkenStempel = this.getObjectAboutFullBalkenStatus(this.treuestempelJa, this.treuestempelNein);
				this.balkenWidthStempel = this.getBalkenWidth(this.treuestempelJa, this.treuestempelNein);
			}
		}
	}
	
	
	showFullBalkenTuete = {'first': undefined, 'second': undefined};
	balkenWidthTuete = {'first': undefined, 'second': undefined};
	prozentTueteJa;
	prozentTueteNein;
	showTuete = false;
	
	tueteData(res) {
		let tuete = _.map(_.countBy(res, "radio_tuete"), (val, key) => ({value: key, total: val}));
		let gesamtTotal = _.sumBy(tuete, 'total');
		
		if(tuete.length) {
			let i = _.findIndex(tuete, {value: 'ja'});
			let j = _.findIndex(tuete, {value: 'nein'});
			
			if(i !== -1 && tuete[i] && tuete[i].total) {
				this.tueteJa = tuete[i].total;
				this.prozentTueteJa = ((this.tueteJa / gesamtTotal) * 100).toFixed();
			}
			if(j !== -1 && tuete[j] && tuete[j].total) {
				this.tueteNein = tuete[j].total;
				this.prozentTueteNein = ((this.tueteNein / gesamtTotal) * 100).toFixed();
			}
			if(i !== -1 || j !== -1) {
				this.showTreffer = true;
				this.showTuete = true;
				this.showFullBalkenTuete = this.getObjectAboutFullBalkenStatus(this.tueteJa, this.tueteNein);
				this.balkenWidthTuete = this.getBalkenWidth(this.tueteJa, this.tueteNein);
			}
		}
	}
	
	showFullBalkenNamensschild = {'first': undefined, 'second': undefined};
	balkenWidthNamensschild = {'first': undefined, 'second': undefined};
	prozentNamensschildJa;
	prozentNamensschildNein;
	showNamensschild = false;

	namensschildData(res) {
		let namensschild = _.map(_.countBy(res, "radio_namensschild"), (val, key) => ({value: key, total: val}));
		let gesamtTotal = _.sumBy(namensschild, 'total');
		
		if(namensschild.length) {
			let i = _.findIndex(namensschild, {value: 'ja'});
			let j = _.findIndex(namensschild, {value: 'nein'});
			
			if(i !== -1 && namensschild[i] && namensschild[i].total) {
				this.namensschildJa = namensschild[i].total;
				this.prozentNamensschildJa = ((this.namensschildJa / gesamtTotal) * 100).toFixed();
			}
			if(j !== -1 && namensschild[j] && namensschild[j].total) {
				this.namensschildNein = namensschild[j].total;
				this.prozentNamensschildNein = ((this.namensschildNein / gesamtTotal) * 100).toFixed();
			}
			if(i !== -1 || j !== -1) {
				this.showTreffer = true;
				this.showNamensschild = true;
				this.showFullBalkenNamensschild = this.getObjectAboutFullBalkenStatus(this.namensschildJa, this.namensschildNein);
				this.balkenWidthNamensschild = this.getBalkenWidth(this.namensschildJa, this.namensschildNein);
			}
		}
	}
	
	
	showFullBalkenKleidung = {'first': undefined, 'second': undefined};
	balkenWidthKleidung = {'first': undefined, 'second': undefined};
	prozentKleidungJa;
	prozentKleidungNein;
	showKleidung = false;
	
	kleidungData(res) {
		let kleidung = _.map(_.countBy(res, "radio_arbeitskleidung"), (val, key) => ({value: key, total: val}));
		let gesamtTotal = _.sumBy(kleidung, 'total');
		
		if(kleidung.length) {
			let i = _.findIndex(kleidung, {value: 'ja'});
			let j = _.findIndex(kleidung, {value: 'nein'});
			
			if(i !== -1 && kleidung[i] && kleidung[i].total) {
				this.kleidungJa = kleidung[i].total;
				this.prozentKleidungJa = ((this.kleidungJa / gesamtTotal) * 100).toFixed();
			}
			if(j !== -1 && kleidung[j] && kleidung[j].total) {
				this.kleidungNein = kleidung[j].total;
				this.prozentKleidungNein = ((this.kleidungNein / gesamtTotal) * 100).toFixed();
			}
			if(i !== -1 || j !== -1) {
				this.showTreffer = true;
				this.showKleidung = true;
				this.showFullBalkenKleidung = this.getObjectAboutFullBalkenStatus(this.kleidungJa, this.kleidungNein);
				this.balkenWidthKleidung = this.getBalkenWidth(this.kleidungJa, this.kleidungNein);
			}
		}
	}
	
	showFullBalkenWeitereArzneimittel = {'first': undefined, 'second': undefined};
	balkenWidthWeitereArzneimittel = {'first': undefined, 'second': undefined};
	prozentArzneiJa;
	prozentArzneiNein;
	showWeitereArzneimittel = false;
	
	weitereArzneimittelData(res) {
		let arznei = _.map(_.countBy(res, "radio_weiterearzneimittel"), (val, key) => ({value: key, total: val}));
		let gesamtTotal = _.sumBy(arznei, 'total');
		
		if(arznei.length) {
			let i = _.findIndex(arznei, {value: 'ja'});
			let j = _.findIndex(arznei, {value: 'nein'});
			
			if(i !== -1 && arznei[i] && arznei[i].total) {
				this.weitereArzneimittelJa = arznei[i].total;
				this.prozentArzneiJa = ((this.weitereArzneimittelJa / gesamtTotal) * 100).toFixed();
			}
			if(j !== -1 && arznei[j] && arznei[j].total) {
				this.weitereArzneimittelNein = arznei[j].total;
				this.prozentArzneiNein = ((this.weitereArzneimittelNein / gesamtTotal) * 100).toFixed();
			}
			if(i !== -1 || j !== -1) {
				this.showTreffer = true;
				this.showWeitereArzneimittel = true;
				this.showFullBalkenWeitereArzneimittel = this.getObjectAboutFullBalkenStatus(this.weitereArzneimittelJa, this.weitereArzneimittelNein);
				this.balkenWidthWeitereArzneimittel = this.getBalkenWidth(this.weitereArzneimittelJa, this.weitereArzneimittelNein);
			}
		}
	}
	
// HELPER ---------------------------------------------------
	getObjectAboutFullBalkenStatus(value1, value2) {
		let tmpObj = { 'first': undefined, 'second': undefined};
		
		if(value1 > value2) {
			tmpObj.first = true;
			tmpObj.second = (value2 > 0);
			
		} else if(value1 < value2) {
			tmpObj.second = true;
			tmpObj.first = (value1 > 0);
			
		} else if(value1 == value2) {
			if(value1 == 0 && value2 == 0) {
				tmpObj.first = false;
				tmpObj.second = false;
			} else {
				tmpObj.first = true;
				tmpObj.second = true;
			}
		}
		return tmpObj;
	}
	
	getBalkenWidth(value1, value2) {
		let max = '100';
		let tmpObj = { 'first': undefined, 'second': undefined};
		
		if(value1 > value2) {
			tmpObj.first = max;
			tmpObj.second = (value2 > 0) ? (100 / value1) * value2 : '';
		} else if(value1 < value2) {
			tmpObj.second = max;
			tmpObj.first = (value1 > 0) ? (100 / value2) * value1 : '';
		} else if(value1 == value2) {
			if(value1 != 0 && value2 != 0) {
				tmpObj.first = max;
				tmpObj.second = max;
			}
		}
		
		return tmpObj;
	}
}
