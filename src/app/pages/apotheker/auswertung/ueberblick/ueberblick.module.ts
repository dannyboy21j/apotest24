import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UeberblickPageRoutingModule } from './ueberblick-routing.module';

import { UeberblickPage } from './ueberblick.page';
import {ComponentsModule} from "../../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		UeberblickPageRoutingModule,
		ComponentsModule
	],
	exports: [
		UeberblickPage
	],
	declarations: [UeberblickPage]
})
export class UeberblickPageModule {}
