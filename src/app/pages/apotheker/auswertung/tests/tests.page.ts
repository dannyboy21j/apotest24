import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "../../../../modules/authentification/auth.service";
import {FormBuilder} from "@angular/forms";
import {FirebaseService} from "../../../../services/firebase.service";
import {IonSearchbar, NavController} from "@ionic/angular";
import * as _ from 'lodash';
import {ROUTES} from "../../../../global-data/routes";
import {Collection} from "../../../../global-data/global";
import {DatabaseService} from "../../../../services/database.service";

type TTester = {
	email: string,
	uid: string
};

@Component({
	selector: 'tests',
	templateUrl: './tests.page.html',
	styleUrls: ['./tests.page.scss'],
})
export class TestsPage implements OnInit {
	@ViewChild('searchbar', {static: true}) searchbar: IonSearchbar;
	@Input() krankheitsId;
	@Input() apothekenId;
	@Input() mitarbeitername;
	@Input() comeFrom;

	autosuggestList: any = [];
	autosuggestListCopy: any;
	placeholder: string;

	showKeinTreffer: boolean = false;

	constructor(
		private auth: AuthService,
		private database: DatabaseService,
		private fb: FormBuilder,
		private fire: FirebaseService,
		private navCtrl: NavController,
	) {
	}

	async ngOnInit() {
		let apothekermail = await this.database.getEmail();
		let auftraege: any;

		if(this.comeFrom === 'TAB1') {
			auftraege = await this.database.getAuftraegeEinerBestimmtenKrankheitEinesApothekersWithDocId(apothekermail, this.krankheitsId);
		} else if (this.comeFrom === 'TAB2') {
			if(this.krankheitsId && this.krankheitsId !== 'undefined') {
				auftraege = await this.database.getAlleGemachtenAuftraegeEinerBestimmtenKrankheitZuEinemApothekerInEinerBestimmtenApotheke(apothekermail, this.apothekenId, this.krankheitsId);
			} else {
				auftraege = await this.database.getAlleGemachtenAuftraegeZuEinemApothekerInEinerBestimmtenApotheke(apothekermail, this.apothekenId);
			}
		} else if (this.comeFrom === 'TAB3') {
			if(this.krankheitsId) {
				auftraege = await this.database.getAlleAuftraegeEinesMitarbeitersZurEinerBestimmtenKrankheit(this.mitarbeitername, this.krankheitsId);
			} else {
				auftraege = await this.database.getAlleAuftraegeEinesMitarbeiters(this.mitarbeitername);
			}
		}
		
		if(auftraege.length) {
			let apotheken: any = await this.database.getCurrentApotheken(apothekermail);
			
			for(let auftrag of auftraege) {
				let result = _.filter(apotheken, {id: auftrag.data.apothekenId});
				auftrag.data.apothekenname = (result.length) ? result[0].name : "";
			}
			
			this.autosuggestList = _.sortBy(auftraege, (obj) => {return obj.email} );
			this.autosuggestListCopy = this.autosuggestList.slice();
		}
	}

	getItems(ev: any) {
		let val = ev.target.value;

		if (val && val.length > 2) {
			this.filterItems(val);
		} else {
			this.autosuggestList = this.autosuggestListCopy;
		}
	}

	async goToTest(_auftrag: any) {
		let auftrag = _auftrag.data,
			krankheiten: any = await this.database.getData(Collection.Krankheiten),
			selectedSchablone = _.filter(krankheiten, {id: this.krankheitsId || auftrag.krankheitsId});
		
		if(krankheiten.length) {
			let title = 'Auswertung ' + selectedSchablone[0].title + '<br>' + auftrag.email,
				auftragsDocId = _auftrag.id,
				apothekenId = auftrag.apothekenId,
				apothekerMail = auftrag.apothekerMail,
				testerMail = auftrag.email,
				binIchVIP = false,
				krankheitsId = this.krankheitsId || auftrag.krankheitsId;
			
			this.navCtrl.navigateForward([ROUTES.sharedErledigterAuftrag,{
				apothekerMail,
				testerMail,
				auftragsDocId,
				krankheitsId,
				binIchVIP,
				title,
				apothekenId,
				showSubmitButton: false
			}]);
		}
	}

	filterItems(val: string) {
		this.autosuggestList = _.filter(this.autosuggestList, (item) => {
			let email = item.email.toLowerCase();
			let apotheke = item.apothekenname.toLowerCase();
			let eingabe = val.toLowerCase();
			
			return _.includes(email, eingabe) || _.includes(apotheke, eingabe);
		});
	}
}
