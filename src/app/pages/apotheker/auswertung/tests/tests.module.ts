import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestsPageRoutingModule } from './tests-routing.module';

import { TestsPage } from './tests.page';
import {ComponentsModule} from "../../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		TestsPageRoutingModule,
		ComponentsModule
	],
	exports: [
		TestsPage
	],
	declarations: [TestsPage]
})
export class TestsPageModule {}
