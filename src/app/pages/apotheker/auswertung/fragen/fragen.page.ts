import {Component, Input, OnInit} from '@angular/core';
import {Collection} from "../../../../global-data/global";
import {AuthService} from "../../../../modules/authentification/auth.service";
import {FirebaseService} from "../../../../services/firebase.service";
import {NavController} from "@ionic/angular";
import {ActivatedRoute} from "@angular/router";
import {UtilService} from "../../../../services/util.service";
import * as _ from 'lodash';
import {ModalService} from "../../../../services/modal.service";
import {DatabaseService} from "../../../../services/database.service";
import {ModalfeedbackComponent} from "./modalfeedback/modalfeedback.component";
import {EventsService} from "../../../../services/events.service";

type TErgebnis = {
	typ: string,
	title: string,
	resultat: Array<object>
};

@Component({
	selector: 'fragen',
	templateUrl: './fragen.page.html',
	styleUrls: ['./fragen.page.scss'],
})
export class FragenPage implements OnInit {
	@Input() krankheitsId;
	@Input() apothekenId;
	@Input() mitarbeitername;
	@Input() comeFrom;
	@Input() themeColor;
	
	slideOpts = {
		slidesPerView: 3,
		spaceBetween: 15,
		pager: false
	};
	ergebnis: TErgebnis[] = [];
	skalaFreundlichkeit: Array<{ value: string, total: number }> = [];
	skalaBeratung: Array<{ value: string, total: number }> = [];
	apotheke;
	produktnamen = [];
	photosImmobilien = [];
	photosRechnung = [];
	
	constructor(
		private auth: AuthService,
		private database: DatabaseService,
		private eventService: EventsService,
		private fire: FirebaseService,
		private modal: ModalService,
		private navCtrl: NavController,
		private router: ActivatedRoute,
		private util: UtilService
	) {
	}
	
	async ngOnInit() {
		let apothekermail = await this.database.getEmail();
		let auftraege: any;
		if(this.comeFrom === 'TAB1') {
			auftraege = await this.database.getAuftraegeEinerBestimmtenKrankheitEinesApothekers(apothekermail, this.krankheitsId);
		} else if (this.comeFrom === 'TAB2') {
			if(this.krankheitsId && this.krankheitsId !== 'undefined') {
				auftraege = await this.database.getAlleGemachtenAuftraegeEinerBestimmtenKrankheitZuEinemApothekerInEinerBestimmtenApothekeOhne(apothekermail, this.apothekenId, this.krankheitsId);
			} else {
				auftraege = await this.database.getAlleGemachtenAuftraegeZuEinemApothekerInEinerBestimmtenApothekeOhne(apothekermail, this.apothekenId);
			}
		} else if (this.comeFrom === 'TAB3') {
			if(this.krankheitsId) {
				auftraege = await this.database.getAlleAuftraegeEinesMitarbeitersZurEinerBestimmtenKrankheitOhne(this.mitarbeitername, this.krankheitsId);
			} else {
				auftraege = await this.database.getAlleAuftraegeEinesMitarbeitersOhne(this.mitarbeitername);
			}
		}
		
		this.fire.getDocumentById(Collection.GlobaleTexte, Collection.doc.formularueberschriften)
		.subscribe((alleFormUeberschriften: IFormTitel) => {
			// let alleKeysUberschrift = Object.keys(alleFormUeberschriften);
			
			// Ich überschreibe die Überschriften manuell, weil Ablahad eine feste Fragenreihenfolge haben möchte
			let alleKeysUberschrift = [
				"radio_geschlecht",
				"input_alter",
				"radio_apotheke",
				"radio_kaufentscheidung",
				"checkbox_produktempfehlung",
				"textarea_produktnichtempfohlen",
				"radio_weiterearzneimittel",
				"radio_giveaway",
				"radio_treuestempel",
				"radio_zeitung",
				"radio_tuete",
				"radio_namensschild",
				"radio_arbeitskleidung",
				"skala_freundlichkeit",
				"skala_beratung",
				"textarea_feedback",
			];
			
			// Weil Ablahad keine Fotos in diesem Bereich haben möchte, reduziere ich das Array
			// _.remove(alleKeysUberschrift, (elem) => {
			// 	return elem === "photo_rechnung" || elem === "photo_immobilie" || elem === "input_kosten";
			// });
			alleKeysUberschrift.forEach(key => {
				let RESULTAT = _.map(auftraege, (obj) => _.pick(obj, [key]));
				
				if (!this.util.areAllObjectsInArrayEmpty(RESULTAT)) {
					this.ergebnis.push({
						typ: key,
						title: alleFormUeberschriften[key],
						resultat: RESULTAT
					});
				}
			});
			this.summerizeAlter(_.filter(this.ergebnis, {typ: 'input_alter'}));
			// this.summerizeKosten(_.filter(this.ergebnis, {typ: 'input_kosten'}));
			this.summerizeMitarbeiter(_.filter(this.ergebnis, {typ: 'select_mitarbeiter'}));
			this.summerizeSkala('skala_freundlichkeit');
			this.summerizeSkala('skala_beratung');
			this.summerizeApotheke(_.filter(this.ergebnis, {typ: 'radio_apotheke'}));
			this.summerizeProduktempfehlung(_.filter(auftraege, 'checkbox_produktempfehlung_name'));
			// this.summerizePhotosImmobilien(_.filter(auftraege, 'photo_immobilie'));
			// this.summerizePhotosRechnung(_.filter(auftraege, 'photo_rechnung'));
			
			this.eventService.publish('pub:anzahlAusgefuellterAuftraege', {anzahlAuftraege: auftraege.length} )
			this.eventService.publish('pub:anzahlFragen', {anzahlFragen: this.ergebnis.length} )
		});
	}
	
	getCountJaNein(resultat: Array<object>, field: string, value: string): { true: number, false: number } {
		let count = _.countBy(resultat, [field, value]);
		return count;
	}
	
	alter = '';
	summerizeAlter(resultat) {
		if(resultat.length) {
			this.alter = Object.values(_.mapValues(resultat[0].resultat, 'input_alter')).join(" Jahre | ").concat(" Jahre")
		}
	}
	
	kosten = '';
	summerizeKosten(resultat) {
		if(resultat.length) {
			this.kosten = Object.values(_.mapValues(resultat[0].resultat, 'input_kosten')).join(" | ");
		}
	}
	
	mitarbeiter = '';
	summerizeMitarbeiter(resultat) {
		if(resultat.length) {
			let mitarbeiter = Object.values(_.mapValues(resultat[0].resultat, 'select_mitarbeiter'));
			this.mitarbeiter = mitarbeiter.join(', ')
		}
	}
	
	summerizeSkala(arg) {
		let obj = _.filter(this.ergebnis, {typ: arg});
		
		if(obj.length) {
			let obj2 = aha(arg);
			let obj3 = fillSkaleOneToFive(obj2);
			let obj4 = _.reject(obj3, (o) => o.value === 'undefined');
			
			if(arg === 'skala_freundlichkeit') {
				this.skalaFreundlichkeit = obj4;
			} else if(arg === 'skala_beratung') {
				this.skalaBeratung = obj4;
			}
			
			function aha(typ) {
				return _.map(_.countBy(obj[0].resultat, typ), (val, key) => ({
					value: Number(key),
					total: val
				}));
			}
			
			function fillSkaleOneToFive(obj) {
				if(obj.length) {
					let tmpObj = [];
					
					for(let i = 0; i < 5; i++) {
						let isSkalaWertVorhanden = _.some(_.map(obj, (o) => o.value === (i+1) ));
						
						if(isSkalaWertVorhanden) {
							tmpObj.push(_.filter(obj, {value: i+1})[0]);
						} else {
							tmpObj.push({ value: (i+1), total: 0});
						}
					}
					return tmpObj;
				} else {
					return obj;
				}
			}
		}
	}
	
	summerizeApotheke(resultat) {
		if(resultat.length){
			this.apotheke = _.map(_.countBy(resultat[0].resultat, "radio_apotheke"), (val, key) => ({value: key, total: val}));
		}
	}
	
	// TODO: Danyel: Eventuell "this.allePhotos" für Rechnung u. Immobilien separat
	summerizePhotosImmobilien(resultat) {
		this.photosImmobilien = _.flatten(_.map(resultat, 'photo_immobilie'));
	}
	
	summerizePhotosRechnung(resultat) {
		this.photosRechnung = _.flatten(_.map(resultat, 'photo_rechnung'));
	}
	
	summerizeProduktempfehlung(resultat) {
		if(resultat && resultat.length) {
			let geflattet = _.flatten(_.map(resultat, 'checkbox_produktempfehlung_name'));
			let uniqProduktnamen = _.uniq(Object.values(_.mapValues(geflattet, 'produktname')))
			let prepare = [];
			for(var item of uniqProduktnamen) {
				var wert = 0;
				for(var item2 of geflattet) {
					if(item2.produktname === item) {
						wert = wert + item2.value
					}
				}
				prepare.push({value: item, total: wert});
			}
			this.produktnamen = prepare;
		}
	}
	
	openModal(data) {
		let array = [];
		for(let item of data.resultat) {
			array.push(item[data.typ]);
		}
		this.modal.showModal(ModalfeedbackComponent, {feedbacks: array});
	}
}
