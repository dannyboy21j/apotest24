import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FragenPage } from './fragen.page';

const routes: Routes = [
  {
    path: '',
    component: FragenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FragenPageRoutingModule {}
