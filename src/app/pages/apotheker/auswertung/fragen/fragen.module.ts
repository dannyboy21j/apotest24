import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FragenPageRoutingModule } from './fragen-routing.module';

import { FragenPage } from './fragen.page';
import {MatListModule} from "@angular/material/list";
import {ComponentsModule} from "../../../../components/components.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		FragenPageRoutingModule,
		MatListModule,
		ComponentsModule
	],
	exports: [
		FragenPage
	],
	declarations: [FragenPage],
})
export class FragenPageModule {}
