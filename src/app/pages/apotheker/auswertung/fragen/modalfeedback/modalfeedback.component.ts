import {Component, Input} from '@angular/core';
import {ModalController} from "@ionic/angular";

@Component({
	selector: 'app-modalfeedback',
	templateUrl: './modalfeedback.component.html',
	styleUrls: ['./modalfeedback.component.scss'],
})
export class ModalfeedbackComponent {
	@Input() feedbacks: [];
	
	constructor(
		private modalCtrl: ModalController,
	) {
	}
	
	dismiss() {
		this.modalCtrl.dismiss({
			dismissed: true
		});
	}
}
