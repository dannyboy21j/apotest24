import {Component, OnInit} from '@angular/core';
import {FirebaseService} from "../../../../services/firebase.service";
import {NavController} from "@ionic/angular";
import {ActivatedRoute} from "@angular/router";
import {AuthService} from "../../../../modules/authentification/auth.service";
import {DatabaseService} from "../../../../services/database.service";
import {EventsService} from "../../../../services/events.service";

type TParams = {
	krankheitsId: string,
	apothekenId: string,
	apothekenName: string,
	title: string,
	comeFrom: string,
	mitarbeitername: string
};

@Component({
	selector: 'app-tabs',
	templateUrl: './landingpage.page.html',
	styleUrls: ['./landingpage.page.scss'],
})
export class LandingpagePage implements OnInit {
	tabIndex: number = 0;
	krankheitsId: string;
	apothekenId: string;
	apothekenName: string;
	title: string;
	mitarbeitername: string;
	comeFrom: string;
	anzahlAuftraege: any;
	anzahlFragen: any;
	themeColor: string;
	
	constructor(
		private auth: AuthService,
		private database: DatabaseService,
		private eventService: EventsService,
		private fire: FirebaseService,
		private navCtrl: NavController,
		private router: ActivatedRoute
	) {
		this.subscribe();
		
		this.router.params.subscribe((params: TParams) => {
			this.krankheitsId = params.krankheitsId;
			if(params.comeFrom === 'TAB1' || params.comeFrom === 'TAB2') {
				if(params.comeFrom === 'TAB1') {
					this.themeColor = 'orange';
				}
				if(params.comeFrom === 'TAB2') {
					this.apothekenId = params.apothekenId;
					this.themeColor = 'primary';
				}
			} else if(params.comeFrom === 'TAB3') {
				this.mitarbeitername = params.mitarbeitername;
				this.themeColor = 'success';
			}
			this.title = params.title;
			this.apothekenName = params.apothekenName;
			this.comeFrom = params.comeFrom;
		});
	}

	async ngOnInit() {
		let apothekermail = await this.database.getEmail();
		let auftraege: any;

		if(this.comeFrom === 'TAB1') {
			auftraege = await this.database.getAuftraegeEinerBestimmtenKrankheitEinesApothekers(apothekermail, this.krankheitsId);
		
		} else if (this.comeFrom === 'TAB2') {
			if(this.krankheitsId && this.krankheitsId !== 'undefined') {
				auftraege = await this.database.getAlleGemachtenAuftraegeEinerBestimmtenKrankheitZuEinemApothekerInEinerBestimmtenApotheke(apothekermail, this.apothekenId, this.krankheitsId);
			} else {
				auftraege = await this.database.getAlleGemachtenAuftraegeZuEinemApothekerInEinerBestimmtenApotheke(apothekermail, this.apothekenId);
			}
		} else if (this.comeFrom === 'TAB3') {
			if(this.krankheitsId) {
				auftraege = await this.database.getAlleAuftraegeEinesMitarbeitersZurEinerBestimmtenKrankheit(this.mitarbeitername, this.krankheitsId);
			} else {
				auftraege = await this.database.getAlleAuftraegeEinesMitarbeiters(this.mitarbeitername);
			}
		}
		this.anzahlAuftraege = auftraege.length;
	}
	
	subscribe() {
		this.eventService.subscribe('pub:anzahlAusgefuellterAuftraege', (res: {anzahlAuftraege: number}) => {
			this.anzahlAuftraege = res.anzahlAuftraege;
		});
		this.eventService.subscribe('pub:anzahlFragen', (res: {anzahlFragen: number}) => {
			// Wenn es keine Aufträge bzw. Tests gibt, dann muss nicht die Anzahl der Fragen definiert werden
			// und kann ruhig mit 0 definiert sein
			this.anzahlFragen = this.anzahlAuftraege ? res.anzahlFragen : 0;
		});
	}

	tabChanged(tabIndex: number) {}
}
