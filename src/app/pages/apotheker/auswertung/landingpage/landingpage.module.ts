import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LandingpagePageRoutingModule } from './landingpage-routing.module';

import { LandingpagePage } from './landingpage.page';
import {MatTabsModule} from "@angular/material/tabs";
import {ComponentsModule} from "../../../../components/components.module";
import {UeberblickPageModule} from "../ueberblick/ueberblick.module";
import {FragenPageModule} from "../fragen/fragen.module";
import {TestsPageModule} from "../tests/tests.module";
import {MatBadgeModule} from "@angular/material/badge";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		LandingpagePageRoutingModule,
		MatTabsModule,
		ComponentsModule,
		UeberblickPageModule,
		FragenPageModule,
		TestsPageModule,
		MatBadgeModule
	],
  declarations: [LandingpagePage]
})
export class LandingpagePageModule {}
