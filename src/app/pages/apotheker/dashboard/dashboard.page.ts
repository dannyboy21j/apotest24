import {Component, OnInit} from '@angular/core';
import {NavController, Platform} from "@ionic/angular";
import {SpinnerService} from "../../../services/spinner.service";
import {ToastService} from "../../../services/toast.service";
import {ImageService} from "../../../services/image.service";
import {Camera, CameraOptions} from "@ionic-native/camera/ngx";
import {DatabaseService} from "../../../services/database.service";
import {Collection} from "../../../global-data/global";
import {FirebaseService} from "../../../services/firebase.service";
import {ROUTES} from "../../../global-data/routes";
import * as $ from 'jquery';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.page.html',
	styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
	isBrowser: boolean;
	imgUrl = 'assets/default_profile.png';
	apothekerData;
	vorname = '';
	nachname = '';
	email = '';
	ROUTES = ROUTES;
	
	options: CameraOptions = {
		quality: 60,
		targetWidth: 400,
		targetHeight: 400,
		destinationType: this.camera.DestinationType.DATA_URL,
		encodingType: this.camera.EncodingType.JPEG,
		mediaType: this.camera.MediaType.PICTURE,
	};
	
	constructor(
		private camera: Camera,
		private database: DatabaseService,
		private fire: FirebaseService,
		private imageService: ImageService,
		private navCtrl: NavController,
		private platform: Platform,
		private spinner: SpinnerService,
		private toast: ToastService
	) {
	}
	
	async ngOnInit() {
		this.isBrowser = !this.platform.is('cordova');
		this.setProfilBild();
	}
	
	async setProfilBild() {
		this.apothekerData = await this.database.getCurrentApotheker();
		this.vorname = this.apothekerData[0].vorname
		this.nachname = this.apothekerData[0].nachname
		this.email = this.apothekerData[0].email;
		if (this.apothekerData[0].profilbild !== "") {
			this.imgUrl = this.apothekerData[0].profilbild;
		}
	}
	
	storeProfilBild() {
		this.fire.updateCollectionProperty(Collection.Apotheker, this.apothekerData[0].apothekerDocId,
			'profilbild', this.imgUrl)
			.then(() => {
				this.toast.presentToast('Profilbild wurde aktualisiert', 'success');
			});
	}
	
	addImageInNativePlatform(imageSourceTyp) {
		this.spinner.show();
		this.camera.getPicture(this.options)
			.then((imageData) => {
				this.imgUrl = 'data:image/jpeg;base64,' + imageData;
				this.spinner.hide();
				this.storeProfilBild();
			})
			.catch((err) => {
				this.spinner.hide();
				this.toast.presentToast('Fehlgeschlagen', 'error');
			});
	}
	
	addImageOnWebBrowser(event) {
		event.preventDefault();
		this.spinner.show();
		// Cloude Firestore allowed pro document size 1048576 Bytes
		let allowedSizeFirestore = 1048576;
		let currentSize = event.currentTarget.files[0].size;
		let image = event.target.files[0];
		
		if (!image) {
			this.toast.presentToast('Bitte konnte nicht hochgeladen werden', 'error');
			return false;
		} else if (!image.type.includes('image/')) {
			this.toast.presentToast('Bitte Bild im Format JPG/PNG', 'error');
			return false;
		}
		
		let imageTyp = image.type;
		image = new File([image], image.name, {type: imageTyp});
		
		if (image.size < allowedSizeFirestore) {
			this.imageService.getPreviewUrl(image)
				.then((base64: string) => {
					this.imgUrl = base64;
					this.spinner.hide();
					this.storeProfilBild();
				});
		} else {
			this.toast.presentToast('Bild ist zu groß', 'error');
			this.spinner.hide();
		}
		//
		// this.ng2ImgMax.resizeImage(image, 10000, 500)
		// 	.subscribe((result) => {
		// 			let image = new File([result], result.name, {type: imageTyp});
		//
		// 			this.ng2ImgMax.compressImage(image, 1)
		// 				.subscribe((result) => {
		// 						if (result.size < allowedSizeFirestore) {
		// 							this.imageService.getPreviewUrl(result)
		// 								.then((base64: string) => {
		// 									this.imgUrl = base64;
		// 									this.spinner.hide();
		// 									this.storeProfilBild();
		// 								});
		// 						} else {
		// 							this.toast.presentToast('Bild ist zu groß', 'error');
		// 							this.spinner.hide();
		// 						}
		// 					},
		// 					(error) => {
		// 						this.toast.presentToast('Bild ist zu groß', 'error');
		// 						this.spinner.hide();
		// 					}
		// 				);
		// 		},
		// 		(error) => {
		// 			this.toast.presentToast('Bild ist zu groß', 'error');
		// 			this.spinner.hide();
		// 		});
	}
	
	goToPage(val) {
		// Trigger Click on Tab
		const $el = $.escapeSelector(val);
		$('[id *=' + $el + ']').trigger('click');
	}
}
