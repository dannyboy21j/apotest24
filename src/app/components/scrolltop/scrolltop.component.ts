import {Component, Input, SimpleChange} from '@angular/core';

@Component({
	selector: 'comp-scrolltop',
	templateUrl: './scrolltop.component.html',
	styleUrls: ['./scrolltop.component.scss'],
})
export class ScrolltopComponent {
	@Input() content;
	@Input() scrollHeight;
	showScrollToTop = false;
	
	ngOnChanges(changes: SimpleChange) {
		this.showScrollToTop = changes['scrollHeight'].currentValue > 500;
	}
	scrollToTop(): void {
		this.content.scrollToTop(1000);
	}
}
