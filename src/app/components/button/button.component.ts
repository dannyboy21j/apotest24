import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'comp-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  @Input() text: string;
  @Input() disabled: boolean;
}
