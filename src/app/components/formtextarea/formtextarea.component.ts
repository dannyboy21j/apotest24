import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from "@angular/forms";

@Component({
	selector: 'comp-formtextarea',
	templateUrl: './formtextarea.component.html',
	styleUrls: ['./formtextarea.component.scss'],
})
export class FormtextareaComponent implements OnInit{
	@Input() myForm: FormGroup;
	@Input() controlname: string;
	@Input() placeholder: string;
	@Input() minLength: string;
	@Input() maxLength: string;
	@Input() readonly: boolean;
	@Input() required: boolean;
	@Input() rows: boolean;
	@Input() title: string;
	@Output() myEventEmitted: EventEmitter<any> = new EventEmitter<any>();
	
	charCounter = 0;
	maximum = 0;
	
	ngOnInit() {
		this.maximum = Number(this.maxLength);
		this.charCounter = this.maximum;
	}
	
	countChar(value) {
		this.charCounter = this.maximum - value.length;
	}
	
	outputMyForm(myForm) {
		this.myEventEmitted.emit(myForm);
	}
}
