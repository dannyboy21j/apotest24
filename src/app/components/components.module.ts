import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from "./header/header.component";
import {IonicModule} from "@ionic/angular";
import {FormfieldComponent} from "./formfield/formfield.component";
import {MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {FormbuttonComponent} from "./formbutton/formbutton.component";
import {FormpasswordComponent} from "./formpassword/formpassword.component";
import {FormradioComponent} from "./formradio/formradio.component";
import {ForminputComponent} from "./forminput/forminput.component";
import {FormtextareaComponent} from "./formtextarea/formtextarea.component";
import {FormradioskalaComponent} from "./formradioskala/formradioskala.component";
import {CardComponent} from "./card/card.component";
import {RouterModule} from "@angular/router";
import {StarComponent} from "./star/star.component";
import {FormcheckboxComponent} from "./formcheckbox/formcheckbox.component";
import {IconBigComponent} from "./icon-big/icon-big.component";
import {ImageuploadindexComponent} from "./imageuploadindex/imageuploadindex.component";
import {PhotobuttonComponent} from "./photobutton/photobutton.component";
import {ImageViewerComponent} from "./image-viewer/image-viewer.component";
import {ImageZoomComponent} from "./image-zoom/image-zoom.component";
import {HeadlineComponent} from "./headline/headline.component";
import {ButtonComponent} from "./button/button.component";
import {SpinnerComponent} from "./spinner/spinner.component";
import {HrComponent} from "./hr/hr.component";
import {ModalpopupComponent} from "./modalpopup/modalpopup.component";

const PAGES_COMPONENTS = [
    HeaderComponent,
    FormfieldComponent,
    FormbuttonComponent,
    FormpasswordComponent,
    FormradioComponent,
    ForminputComponent,
    FormtextareaComponent,
    FormradioskalaComponent,
    CardComponent,
    StarComponent,
    FormcheckboxComponent,
    IconBigComponent,
    ImageuploadindexComponent,
    PhotobuttonComponent,
    ImageViewerComponent,
    ImageZoomComponent,
    HeadlineComponent,
    ButtonComponent,
    SpinnerComponent,
    HrComponent,
    ModalpopupComponent
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        RouterModule
    ],
    exports: [PAGES_COMPONENTS],
    declarations: [PAGES_COMPONENTS],

})
export class ComponentsModule {
}
