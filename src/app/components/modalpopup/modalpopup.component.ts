import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from "@ionic/angular";
import {FirebaseService} from "../../services/firebase.service";
import {GLOBAL} from "../../global-data/global";

@Component({
	selector: 'app-modalpopup',
	templateUrl: './modalpopup.component.html',
	styleUrls: ['./modalpopup.component.scss'],
})
export class ModalpopupComponent implements OnInit {
	@Input() text: string;
	@Input() isAnleitungsText: boolean;
	showContentText = '';
	GLOBAL = GLOBAL;

	constructor(
		private fire: FirebaseService,
		private modalCtrl: ModalController,
	) {
	}
	
	ngOnInit() {
		this.showContentText = (this.isAnleitungsText) ? this.GLOBAL.Anleitungstexte[this.text] : this.text;
	}
	
	dismiss() {
		this.modalCtrl.dismiss({
			dismissed: true
		});
	}
}
