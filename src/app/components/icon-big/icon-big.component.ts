import {Component, Input} from '@angular/core';

@Component({
  selector: 'comp-icon-big',
  templateUrl: './icon-big.component.html',
  styleUrls: ['./icon-big.component.scss'],
})
export class IconBigComponent {
  @Input() showText: string;
  @Input() iconName: string;
  @Input() color: string
}
