import {Component, Input} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ImageViewerComponent} from '../image-viewer/image-viewer.component';

@Component({
	selector: 'image-zoom-component',
	templateUrl: './image-zoom.component.html',
	styleUrls: ['./image-zoom.component.scss']
})
export class ImageZoomComponent {
	@Input() imgUrl: string;
    @Input() firma: string;
    @Input() email: string;
    @Input() cssClasses: string;

	constructor(
		private modalCtrl: ModalController
	) {}

	async viewImage(src: string, title: string = '', description: string = '') {
		const modal = await this.modalCtrl.create({
			component: ImageViewerComponent,
			componentProps: {
				imgSource: src,
				imgTitle: title,
				imgDescription: description
			},
			cssClass: 'modal-fullscreen',
			keyboardClose: true,
			showBackdrop: true
		});

		return await modal.present();
	}
}
