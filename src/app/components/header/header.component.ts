import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NavController} from "@ionic/angular";
import {GLOBAL, USER} from "../../global-data/global";
import {DatabaseService} from "../../services/database.service";
import {AuthService} from "../../modules/authentification/auth.service";
import {LOCALSTORAGE} from "../../global-data/localstorage-names";
import {Storage} from "@ionic/storage";
import {ModalService} from "../../services/modal.service";
import {ModalpopupComponent} from "../modalpopup/modalpopup.component";
import {Location} from "@angular/common";
import {AlertService} from "../../services/alert.service";
import * as $ from 'jquery';

@Component({
    selector: 'comp-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
    @Output() myEventEmitted: EventEmitter<any> = new EventEmitter<any>();
    
    @Input() title: {klein: string, gross: string};
    @Input() showHomeIcon: boolean;
    @Input() showMenuBurger: boolean;
    @Input() showBackButton: boolean;
    @Input() showForwardButton: boolean;
    @Input() anleitungsText = '';
    @Input() isAnleitungsText: boolean;
    @Input() showConfirmation: boolean;
    @Input() showPrintButton: boolean;
    @Input() themeColor: string;
    
    USER = USER;
    GLOBAL = GLOBAL;

    constructor(
        private alertService: AlertService,
        private auth: AuthService,
        private database: DatabaseService,
        private location: Location,
        private modal: ModalService,
        private navCtrl: NavController,
        private storage: Storage
    ) {
    }

    back() {
        if(this.showConfirmation === true) {
            this.openModelAttentionAllDateWillLost();
        } else {
            this.navCtrl.back();
        }
    }
    
    forward() {
        this.location.forward();
    }
    
    async goToLandingpage() {
        this.navCtrl.navigateForward([await this.database.getLandingPageRoute()]);
    }
    
    signOut() {
        this.auth.SignOut();
        this.database.setEmail('');
        this.storage.remove(LOCALSTORAGE.userTyp);
        // this.navCtrl.navigateForward([ROUTES.login]);
    }
    
    openModal() {
        this.modal.showModal(ModalpopupComponent, {
            text: this.anleitungsText,
            isAnleitungsText: this.isAnleitungsText
        });
    }
    
    
    openModelAttentionAllDateWillLost() {
        this.alertService.presentAlertConfirm('Ihre Formulareingaben gehen verloren', 'Möchten Sie die Seite wirklich verlassen?')
            .then((res: { data: object, role: string }) => {
                if (res && res.role === 'ja') {
                    this.navCtrl.back();
                }
            });
    }
    
    print() {
        this.myEventEmitted.emit();
    }
    
    goToPage() {
        $("ion-tab-button").removeClass('tab-selected-primary tab-selected-orange tab-selected-success');
        this.navCtrl.navigateForward(['/']);
    }
}
