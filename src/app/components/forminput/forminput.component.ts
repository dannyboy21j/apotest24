import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup} from "@angular/forms";
import * as _ from 'lodash';
import {AlertService} from "../../services/alert.service";
import {UtilService} from "../../services/util.service";

@Component({
	selector: 'comp-forminput',
	templateUrl: './forminput.component.html',
	styleUrls: ['./forminput.component.scss'],
})
export class ForminputComponent {
	@Input() myForm: FormGroup;
	@Input() controlname: string;
	@Input() required: boolean;
	@Input() title: string;
	@Input() title2: string;
	@Input() subtyp: string;
	@Input() placeholder: string;
	@Input() minLength: string;
	@Input() maxLength: string;
	@Input() readonly: boolean;
	@Input() kostenOrAlter: string;
	@Output() myEventEmitted: EventEmitter<any> = new EventEmitter<any>();
	
	showConfirmationLayerForPriceCheck = true;
	
	constructor(
		private alertService: AlertService,
		private util: UtilService
	) {}
	
	cutValue(val, myForm) {
		if(this.title2 === 'input_kosten') {
			if(val !== '' && val.length <= this.maxLength && this.util.isPriceValid(val)) {
				val = val.replace(',','.').replace(/\s/g,'');
				
				if(_.includes(val,'.')) {
					let splitted = val.split('.');
					let teil2 = splitted[1];
					
					if(teil2.length === 2) {
						this.kostenOrAlter = val;
					} else if(teil2.length > 2) {
						let sliced = teil2.slice(0,2);
						splitted[1] = sliced;
						this.kostenOrAlter = splitted.join('.');
					} else if(teil2.length === 1){
						splitted[1] = teil2 + '0';
						this.kostenOrAlter = splitted.join('.');
					}
				} else {
					this.kostenOrAlter = Number(val).toFixed(2);
				}
				
				if(this.showConfirmationLayerForPriceCheck) {
					this.alertService.presentAlertConfirm('Bitte den Betrag kontrollieren', 'Ist der Betrag von ' + this.kostenOrAlter + ' € richtig?')
						.then((res: { data: object, role: string }) => {
							if (res && res.role === 'nein') {
								this.myForm.get('input_kosten').setValue(null);
								this.myEventEmitted.emit(myForm);
							}
						});
				}
			} else {
				this.alertService.presentAlert("Info", "", "Bitte geben Sie einen validen Preis an, z.B. 4.99", "OK")
					.then(() => {
						this.myForm.get('input_kosten').setValue(null);
						this.myEventEmitted.emit(myForm);
					});
			}
		}
	}
	
	outputMyForm(myForm) {
		this.myEventEmitted.emit(myForm);
	}
}
