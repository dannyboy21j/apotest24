import {Component, Input} from '@angular/core';

@Component({
  selector: 'comp-imageuploadindex',
  templateUrl: './imageuploadindex.component.html',
  styleUrls: ['./imageuploadindex.component.scss'],
})
export class ImageuploadindexComponent{
  @Input() bilder = [];
  @Input() maximum: number;
}
