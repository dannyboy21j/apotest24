import {Component, Input} from '@angular/core';
import {FormGroup} from "@angular/forms";

@Component({
    selector: 'comp-formfield',
    templateUrl: './formfield.component.html',
    styleUrls: ['./formfield.component.scss'],
})
export class FormfieldComponent {
    @Input() myForm: FormGroup;
    @Input() label: string;
    @Input() controlname: string;
    @Input() icon: string;
}
