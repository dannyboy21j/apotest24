import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup} from "@angular/forms";

@Component({
	selector: 'comp-formradioskala',
	templateUrl: './formradioskala.component.html',
	styleUrls: ['./formradioskala.component.scss'],
})
export class FormradioskalaComponent {
	@Input() myForm: FormGroup;
	@Input() controlname: string;
	@Input() required: string;
	@Input() title: string;
	@Input() radioItems: Array<number>;
	@Input() disabled: boolean;
	@Output() myEventEmitted: EventEmitter<any> = new EventEmitter<any>();
	
	outputMyForm(myForm) {
		this.myEventEmitted.emit(myForm);
	}
}
