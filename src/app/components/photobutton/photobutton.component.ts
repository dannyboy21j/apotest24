import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ImageService} from "../../services/image.service";
import * as _ from 'lodash';
import {ToastService} from "../../services/toast.service";
import {SpinnerService} from "../../services/spinner.service";
import {Platform} from "@ionic/angular";
import {Camera, CameraOptions} from "@ionic-native/camera/ngx";

type TCompressedResult = {
	lastModified: number,   // lastModified: 1578669539210
	name: string,           //name: 'Fronalpstock_big.jpg'
	size: number,           //size: 1007135
	type: string            //type: 'image/jpeg'
};

@Component({
	selector: 'comp-photobutton',
	templateUrl: './photobutton.component.html',
	styleUrls: ['./photobutton.component.scss'],
})
export class PhotobuttonComponent implements OnInit{
	@Output() myEventEmitted: EventEmitter<any> = new EventEmitter<any>();
	@Input() maximumImages: number;
	@Input() showImageAmountHint: boolean = false;
	@Input() subtyp: string = '';
	@Input() produktBilder: Array<{ base64: string }> = [];
	@Input() showSubmitButton: boolean;

	isBrowser: boolean;
	options: CameraOptions = {
		quality: 60,
		targetWidth: 400,
		targetHeight: 400,
		destinationType: this.camera.DestinationType.DATA_URL,
		encodingType: this.camera.EncodingType.JPEG,
		mediaType: this.camera.MediaType.PICTURE,
	};
	
	slideOpts = {
		slidesPerView: 3,
		spaceBetween: 15,
		pager: false
	};
	
	// produktBilder: Array<{ base64: string }> = [];
	
	constructor(
		private camera: Camera,
		private imageService: ImageService,
        private platform: Platform,
        private spinner: SpinnerService,
        private toast: ToastService
	) {
	}
	
	ngOnInit() {
		this.isBrowser = !this.platform.is('cordova');
	}
	
	addImageInNativePlatform(imageSourceTyp) {
		this.spinner.show();
		if(imageSourceTyp === 'PHOTOLIBRARY') {
			this.options['sourceType'] = this.camera.PictureSourceType.PHOTOLIBRARY;
		}
		this.camera.getPicture(this.options)
			.then((imageData) => {
				let base64 = 'data:image/jpeg;base64,' + imageData;
				if (this.produktBilder.length <= this.maximumImages) {
					this.produktBilder.push({base64});
					this.myEventEmitted.emit(this.produktBilder);
					this.spinner.hide();
				}
				this.spinner.hide();
			})
			.catch((err) => {
				this.spinner.hide();
				this.toast.presentToast('Fehlgeschlagen', 'error');
			});
	}
	
	addImageOnWebBrowser(event) {
		event.preventDefault();
		if (!this.isImageLimitAchieved()) {
			this.spinner.show();
			// Cloude Firestore allowed pro document size 1048576 Bytes
			let allowedSizeFirestore = 1048576;
			let currentSize = event.currentTarget.files[0].size;
			let image = event.target.files[0];
			
			if (!image) {
				this.toast.presentToast('Bitte konnte nicht hochgeladen werden', 'error');
				return false;
			} else if (!image.type.includes('image/')) {
				this.toast.presentToast('Bitte Bild im Format JPG/PNG', 'error');
				return false;
			}
			
			let imageTyp = image.type;
			image = new File([image], image.name, {type: imageTyp});
			if (image.size < allowedSizeFirestore) {
				this.imageService.getPreviewUrl(image)
					.then((base64: string) => {
						if (this.produktBilder.length <= this.maximumImages) {
							this.produktBilder.push({base64});
							this.myEventEmitted.emit(this.produktBilder);
							this.spinner.hide();
						}
					});
			} else {
				this.toast.presentToast('Bild ist zu groß', 'error');
				this.spinner.hide();
			}
			
			
			// this.ng2ImgMax.resizeImage(image, 10000, 500)
			// 	.subscribe((result) => {
			// 		let image = new File([result], result.name, {type: imageTyp});
			//
			// 		this.ng2ImgMax.compressImage(image, 1)
			// 			.subscribe((result: TCompressedResult) => {
			// 					if (result.size < allowedSizeFirestore) {
			// 						this.imageService.getPreviewUrl(result)
			// 							.then((base64: string) => {
			// 								if (this.produktBilder.length <= this.maximumImages) {
			// 									this.produktBilder.push({base64});
			// 									this.myEventEmitted.emit(this.produktBilder);
			// 									this.spinner.hide();
			// 								}
			// 							});
			// 					} else {
			// 						this.toast.presentToast('Bild ist zu groß', 'error');
			// 						this.spinner.hide();
			// 					}
			// 				},
			// 				(error) => {
			// 					this.toast.presentToast('Bild ist zu groß', 'error');
			// 					this.spinner.hide();
			// 				}
			// 			);
			// 	},
			// 	(error) => {
			// 		this.toast.presentToast('Bild ist zu groß', 'error');
			// 		this.spinner.hide();
			// 	});
		}
	}
	
	isImageLimitAchieved(): boolean {
		return this.produktBilder.length === this.maximumImages;
	}
	
	deleteImage(baseUrl: string) {
		_.remove(this.produktBilder, {base64: baseUrl});
		this.myEventEmitted.emit(this.produktBilder);
	}
}
