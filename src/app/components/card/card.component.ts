import {Component, Input} from '@angular/core';
import {Collection, GLOBAL} from "../../global-data/global";
import {NavController} from "@ionic/angular";
import {ROUTES} from "../../global-data/routes";
import {FirebaseService} from "../../services/firebase.service";
import * as _ from 'lodash';
import {AuthService} from "../../modules/authentification/auth.service";
import {DatabaseService} from "../../services/database.service";

@Component({
	selector: 'comp-card',
	templateUrl: './card.component.html',
	styleUrls: ['./card.component.scss'],
})
export class CardComponent {
	GLOBAL = GLOBAL;
	ROUTES = ROUTES;
	email: string;

	@Input() route: string;
	@Input() formTyp?: string;
	@Input() itemValue?: string;
	@Input() active?: boolean;
	@Input() btnAuswertung?: boolean;
	@Input() title: string;
	@Input() subTitle: string;
	@Input() amount: number | string;
	@Input() prozent: number;
	@Input() icon: string;
	@Input() linkText: string;
	@Input() isDeactive: boolean;
	@Input() bereitsErledigterAuftrag: boolean;
	@Input() kontingentErschoepft: boolean;
	@Input() umsatz: boolean;
	@Input() prozentZeichen: boolean;
	@Input() genehmigungsStatus: string;
	@Input() korrekturbeschreibung: string;
	@Input() themeColor: string;
	
	constructor(
		private auth: AuthService,
		private database: DatabaseService,
		private fire: FirebaseService
	) {
	}
	
	async toggleActivation(event, itemValue: string, active: boolean) {
		event.preventDefault();
		event.stopPropagation();
		// Wenn active == true ist, d.h. dass Apotheke zuvor AKTIV ist und jetzt deaktiviert werden muss u. das passiert wie folgt
		// Das Array deaktApotheken muss um das itemValue erweitert werden
		let apotheker: any = await this.database.getCurrentApotheker();
		if(apotheker.length) {
			let deaktiveItems,
				updateProperty;
			if(this.formTyp === 'Apotheke') {
				deaktiveItems = apotheker[0].deaktApotheken
				updateProperty = 'deaktApotheken';
			} else if(this.formTyp === 'Auftrag') {
				deaktiveItems = apotheker[0].deaktAuftraege
				updateProperty = 'deaktAuftraege';
			}
			if(active) {
				deaktiveItems.push(itemValue);
			} else {
				deaktiveItems = _.pull(deaktiveItems, itemValue);
			}
			this.fire.updateCollectionProperty(
				Collection.Apotheker, apotheker[0].docId, updateProperty , _.uniq(deaktiveItems));
		}
	}
	
	getCssClass(status) {
		if(status === 'inpruefung') return 'inpruefung';
		if(status === 'beantragt') return 'beantragt';
		if(status === 'abgelehnt') return 'abgelehnt';
		if(status === 'offen') return 'offen';
		if(status === 'erledigt') return 'erledigt';
	}
	
	// toggleActivation(event, krankheitsId: string, active: boolean) {
	// 	event.preventDefault();
	// 	event.stopPropagation();
	//
	// 	let entitaet: IAuftragsSchablone[] = _.filter(this.allExistsForms, obj => obj.data.id === krankheitsId);
	// 	let docId = entitaet[0].id;
	//
	// 	this.fire.updateCollectionProperty(Collection.Auftragsschablonen, docId, 'active', !active)
	// 		.then(() => {
	// 		});
	// }

	goToPage(event, route, krankheitsId: string) {
		// event.preventDefault();
		// event.stopPropagation();
		//
		// let entitaet: IAuftragsSchablone[] = _.filter(this.allExistsForms, obj => obj.data.id === krankheitsId);
		// let docId = entitaet[0].id;
		// let title = entitaet[0].data.title;
		//
		// this.navCtrl.navigateForward([route, {krankheitsId, docId, title, comeFromPage: 'APOTHEKER'}]);
	}
}
