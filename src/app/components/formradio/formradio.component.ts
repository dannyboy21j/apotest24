import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup} from "@angular/forms";

@Component({
	selector: 'comp-formradio',
	templateUrl: './formradio.component.html',
	styleUrls: ['./formradio.component.scss'],
})
export class FormradioComponent {
	@Input() myForm: FormGroup;
	@Input() controlname: string;
	@Input() required: boolean = false;
	@Input() title: string;
	@Input() radioItems: Array<{ label: string, value: string }>;
	@Input() disabled: boolean;
	@Input() isKaufentscheidung: boolean = false;
	@Output() myEventEmitted: EventEmitter<any> = new EventEmitter<any>();
	@Output() myEventEmitted2: EventEmitter<any> = new EventEmitter<any>();
	
	isValid = false;
	
	kaufentscheidung(e) {
		if(this.isKaufentscheidung) {
			this.myEventEmitted.emit(e.detail.value);
		}
	}
	
	outputMyForm(myForm) {
		this.myEventEmitted2.emit(myForm);
	}
}
