import {Component, Input} from '@angular/core';

@Component({
  selector: 'comp-fab',
  templateUrl: './fab.component.html',
  styleUrls: ['./fab.component.scss'],
})
export class FabComponent {
  @Input() vertical;   // "bottom" | "center" | "top" | undefined.
  @Input() horizontal; // "center" | "end" | "start" | undefine
  @Input() color;
  @Input() bgColor;
  @Input() icon;
  @Input() showOpacity;
}
