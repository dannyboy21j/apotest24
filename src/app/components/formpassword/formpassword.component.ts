import {Component, Input} from '@angular/core';
import {FormGroup} from "@angular/forms";

@Component({
    selector: 'comp-formpassword',
    templateUrl: './formpassword.component.html',
    styleUrls: ['./formpassword.component.scss'],
})
export class FormpasswordComponent {
    @Input() myForm: FormGroup;
    @Input() label: string;
    @Input() controlname: string;
    hide = true;
}
