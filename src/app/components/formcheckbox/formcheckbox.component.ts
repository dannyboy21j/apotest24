import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormArray, FormGroup} from "@angular/forms";

@Component({
	selector: 'comp-formcheckbox',
	templateUrl: './formcheckbox.component.html',
	styleUrls: ['./formcheckbox.component.scss'],
})
export class FormcheckboxComponent {
	@Input() myForm: FormGroup;
	@Input() controlname: string;
	@Input() formArray;
	@Input() title: string;
	@Input() checkboxen: Array<string>;
	@Input() disabled: boolean;
	@Output() myEventEmitted: EventEmitter<any> = new EventEmitter<any>();
	
	getControls() {
		return (<FormArray>this.myForm.get(this.formArray)).controls;
	}
	
	outputMyForm(myForm) {
		this.myEventEmitted.emit(myForm);
	}
}
