import {Component, Input} from '@angular/core';

@Component({
  selector: 'comp-hr',
  templateUrl: './hr.component.html',
  styleUrls: ['./hr.component.scss'],
})
export class HrComponent {
    @Input() themeColor: string;
}
