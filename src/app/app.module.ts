import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';
import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FirebaseModule} from "./modules/firebase.module";
import {MaterialModule} from "./modules/material.module";
import {AuthentificationModule} from "./modules/authentification/authentification.module";
import {ComponentsModule} from "./components/components.module";
import {IonicStorageModule} from "@ionic/storage";
import { Ng2ImgMaxModule } from 'ng2-img-max';
import {AgmCoreModule} from "@agm/core";
import {ImageViewerComponent} from "./components/image-viewer/image-viewer.component";
import {ModalfeedbackComponent} from "./pages/apotheker/auswertung/fragen/modalfeedback/modalfeedback.component";
import {Camera} from "@ionic-native/camera/ngx";

@NgModule({
    declarations: [
        AppComponent,
        ModalfeedbackComponent
    ],
    entryComponents: [
        ImageViewerComponent,
        ModalfeedbackComponent
    ],
    imports: [
        IonicModule.forRoot(),
        IonicStorageModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyA_xxEVGFxPgWY5dCj3ZBgQd37KYn-7Kds',
            libraries: ['places']
        }),
        BrowserModule,
        ComponentsModule,
        FirebaseModule,
        MaterialModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        AuthentificationModule,
        Ng2ImgMaxModule,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Camera,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
