// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	firebaseConfig: {
		apiKey: "AIzaSyA_xxEVGFxPgWY5dCj3ZBgQd37KYn-7Kds",
		authDomain: "apo24-7d691.firebaseapp.com",
		projectId: "apo24-7d691",
		storageBucket: "apo24-7d691.appspot.com",
		messagingSenderId: "211846948272",
		appId: "1:211846948272:web:8b83a0d3dbf960aa666275",
		measurementId: "G-3D088RSWN8"
	}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
